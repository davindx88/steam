@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>My Gifts</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Gift</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div class="float-right">
        <div class="dropdown">
            <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Gift {{ ['Received','Sent'][$type] }}
            </button>
            <div class="dropdown-menu" style="background:#0009" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item text-warning" href="?type=received">Gift Received</a>
                <a class="dropdown-item text-warning" href="?type=sent">Gift Sent</a>
            </div>
        </div>
    </div>
    <h1 id="gift_header">Gift {{ ['Received','Sent'][$type] }}</h1>
    @forelse ($list_gift as $key => $gift)
    <div class="card bg-dark position-relative p-3 mb-3">
        <div class="position-absolute" style="right: 10px; top: 10px">
            <a class="btn btn-dark collapse-toggler text-warning"
                data-toggle="collapse"
                href="#collapse{{ $key }}"
                role="button">
                <i class="fa fa-chevron-down" ></i>
            </a>
        </div>
        <h4>
            @if ($type==0)
            From: {{ $gift->sender->first_name }} {{ $gift->sender->last_name }}
            <a href="/users/{{ $gift->sender->username }}">
                {{ '@'.$gift->sender->username }}
            </a>
            @else
            To: {{ $gift->receiver->first_name }} {{ $gift->receiver->last_name }}
            <a href="/users/{{ $gift->receiver->username }}">
                {{ '@'.$gift->receiver->username }}
            </a>
            @endif
        </h4>
        <div style="margin-top: -4px">
            Status: <span class="badge {{ ['badge-danger', 'badge-warning', 'badge-success'][$gift->status+1] }}">
                {{ ['Rejected', 'Pending', 'Accepted'][$gift->status+1] }}
            </span><br>
        </div>
        <div class="mb-2" style="margin-top: -10px">
            <sub>{{ $gift->created_at->diffForHumans() }}</sub>
        </div>
        <div class="collapse" id="collapse{{ $key }}">
            <div class="card card-body" style="background: #0009">
                {{ $gift->message }}
            </div>
            <div class="mt-3 d-flex justify-content-center flex-wrap">
            @foreach ($gift->transaction->games as $key_game => $game)
                <div class="mr-2 ml-2 mb-3">
                    <a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}">
                        <img src="/storage/{{ $game->thumbnail_filename }}" style="width:100px; height:100px;"
                            data-toggle="tooltip"
                            title="{{ $game->name }}">
                    </a>
                </div>
            @endforeach
            </div>

            @if ($type == 0)
            <div class="float-right">
                <form action="/gifts/response" method="post">
                    @csrf
                    <input type="hidden" name="gift_id" value="{{ $gift->id }}">
                    <input type="hidden" name="response" value="1">
                    <div style="background: #0003">
                        <button class="btn btn-outline-danger">Reject</button>
                    </div>
                </form>
            </div>
            <div class="float-right mr-2">
                <form action="/gifts/response" method="post">
                    @csrf
                    <input type="hidden" name="gift_id" value="{{ $gift->id }}">
                    <input type="hidden" name="response" value="0">
                    <div>
                        <button class="btn btn-success">Accept</button>
                    </div>
                </form>
            </div>
            @endif
        </div>
    </div>
    @empty
    <h3 class="text-center mt-5">There's no {{ ['Received Gift', 'Sent Gift'][$type] }} :(</h3>
    @endforelse
    @error('msg')
        {{ $message }}
    @enderror
</div>
@endsection

@section('script')
<script>
    $(()=>{
        $('.collapse-toggler').click(function(){
            const child = $(this).children()[0]
            if(child.className.includes('down')){
                child.className = 'fa fa-chevron-up'
            }else{
                child.className = 'fa fa-chevron-down'
            }
        })
    })
</script>
@endsection
