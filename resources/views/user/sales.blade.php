@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area"
    style="background:url('/storage/photos/{{ $sale->filename }}');
    background-repeat: no-repeat;background-size:100% 100%;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- Game Page Start -->
<section class="fag-product-page section_100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-center flex-wrap">
                    <div class="site-heading mt-3">
                        <h2 class="heading_animation"><span>{{ $sale->title }}</span></h2>
                        <p>{{ $sale->description }}</p>
                     </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Game Page End -->

<!-- Games Area Strat -->
<section class="fag-games-area section_140">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="site-heading">
                <h2 class="heading_animation">Sale <span>games</span></h2>
             </div>
          </div>
       </div>
       <div class="row">
          <div class="col-12">
             <div class="games-masonary">
                <div class="clearfix gamesContainer">
                    @foreach ($sale->games as $key => $game)
                    <div class="games-item">
                        <div class="games-thumb">
                            <div class="games-thumb-image">
                                <a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}">
                                    <img src="/storage/{{ $game->thumbnail_filename}}" alt="product" />
                                </a>
                            </div>
                        </div>
                        <div class="games-desc">
                            <h3 class="text-left"><a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}">{{ $game->name }}</a></h3>
                            <h6><a href="/developers/{{ $game->developer->developer->slug_url }}">{{ $game->developer->developer->slug_url }}</a></h6>
                            <h6 class="mt-3">{{ $game->genres->implode('name', ' | ') }}</h6>
                            @if ($game->rating() != 0)
                            <div class="game-rating">
                                <h4>{{ $game->rating() }}</h4>
                                <ul>
                                    @for ($i = 0; $i < 5; $i++)
                                    @if ($i+1 <= $game->rating())
                                    <li><span class="fa fa-star"></span></li>
                                    @else
                                    <li><span class="fa fa-star-o"></span></li>
                                    @endif
                                    @endfor
                                </ul>
                            </div>
                            @endif
                            <div class="game-action">
                                <div class="game-price">
                                    @if ($game->price == 0)
                                    <h4 class="text-success">Free</h4>
                                    @elseif($game->price == $game->discprice())
                                    <h4>@idr( $game->price )</h4>
                                    @else
                                    <h4><del>@idr( $game->price )</del></h4>
                                    @if ($game->discprice() == 0)
                                    <h4 class="text-success">Free</h4>
                                    @else
                                    <h4>@idr( $game->discprice() ) </h4>
                                    @endif
                                    @endif
                                </div>
                                <div class="game-buy ml-3">
                                    @if ($user !== null && $user->games()->where('id', $game->id)->first() !== null)
                                    <h5 class="text-success">Owned</h5>
                                    @else
                                    <form action="/cart/add" method="post">
                                        @csrf
                                        <input type="hidden" name="game_slug" value="{{ $game->slug_url }}">
                                        <button class="fag-btn">Buy Now!</button>
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
             </div>
          </div>
       </div>
    </div>
 </section>
 <!-- Games Area End -->
@endsection
