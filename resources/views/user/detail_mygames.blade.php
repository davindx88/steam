@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <div class="profile-image">
                        @if ($user->filename == '')
                                <img style="object-fit: cover;width: 250px;height: 250px;"  src="/iconprofile.png" alt="asd" class="edit-profile-img" id="img">
                            @else
                                <img style="object-fit: cover;width: 250px;height: 250px;"  src="/storage/photos/{{$user->filename}}" alt="asd" class="edit-profile-img" id="img">
                            @endif
                    </div>
                    <h3>Profile</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->
<div class="container p-5 mt-5 position-relative">

    <h1>{{ $user->first_name }} {{ $user->last_name }}</h1>
    <h4>{{ '@'.$user->username }}</h4>
    <hr>
    <div class="row">
        <div class="col-md-12 col-lg-8">
            <h2>Games ({{ $user->games->count() }})</h2>
            @foreach ($user->games as $key => $game)
            <div class="summury-inn clear profile-game">
                <img class="left-img" src="{{ url('Photos/game1.jpg') }}" alt="Game">
                <div class="left-div">
                    <h2><a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}" class="text-glowing">{{ $game->name }}</a></h2>
                    <h6><a href="/developers/{{ $game->developer->developer->slug_url }}">{{ $game->developer->developer->name }}</a></h6>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-md-12 col-lg-4">
            <h2>Friends ({{ $user->friends()->count() }})</h2>
            @foreach ($user->friends() as $key => $friend)
            <div class="summury-inn clear profile-game">
                <div class="left-div">
                    <div class="left-div">
                        <h4><a href="/users/{{ $friend->username }}">{{ $friend->username }}</a></h4>
                        <h6>{{ $friend->first_name }} {{ $friend->last_name }}</h6>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
