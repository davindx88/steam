@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Users</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Users</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->
<div class="container p-5 position-relative">
    <div class="contact-form-inn">
        <h3>Users</h3>
        <form action="#" method="get">
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field">
                        <input class="ns-com-name" name="u" placeholder="username" type="text"
                            value="{{ old('u') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field form-action">
                        <button class="fag-btn">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @forelse ($list_user as $key => $acc)
    <div class="summury-inn clear profile-game">
        <img style="object-fit: cover;width: 100px;height: 100px;" src="/storage/{{$acc->filename}}" alt="asd" class="edit-profile-img float-left" id="img">
        <div class="left-div">
            <h2><a href="/users/{{ $acc->username }}">{{ $acc->username }}</a></h2>
            <h6>{{ $acc->first_name }} {{ $acc->last_name }}</h6>
            @if ($user !== null)
                @if ($user !== null && $user->chatroomWithFriend($acc->id) === null)
                <a href="/friends/add?u={{ $acc->username }}" class="fag-btn-outline">Add Friend</a>
                @else
                <h5 class="text-success">Friend</h5>
                @endif
            @endif
        </div>
    </div>
    @empty

    @endforelse
</div>
@endsection
