@extends('layout.user')
@section('content')
<script type="text/javascript">
    $(document).ready(function () {
        $('.category').select2();
    });
</script>
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Explore</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Explore</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->



<!-- Game Page Start -->
<section class="fag-product-page section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="sidebar-widget">
                    <div class="filter">
                        <h4 class="filter_title">Filters
                            {{-- <button type="button">Clear all</button> --}}
                        </h4>
                        <form action="/filtersearch" method="post">
                            @csrf
                            @if ($olddata!=[])
                                <div class="filter_group">
                                    <label class="filter_label">Keyword:</label>
                                    <input type="text" name="key" class="filter_input bg-light form-control" placeholder="Keyword" value="{{ $olddata['key']}}" >
                                </div>


                                <div class="form-group">
                                    <label class="filter_label">Sort By:</label>
                                    <select class="category related-post form-control" name="allfilter">
                                        {{-- @foreach ($platform as $item) --}}
                                        <option value="none"   {{ $olddata['allfilter'] == "none" ?   "selected":'' }} >-- No Sort --</option>
                                        <option value="asc"    {{ $olddata['allfilter'] == "asc" ?    "selected":'' }}>Price Ascending</option>
                                        <option value="desc"   {{ $olddata['allfilter'] == "desc" ?   "selected":'' }}>Price Descending</option>
                                        <option value="rating" {{ $olddata['allfilter'] == "rating" ? "selected":'' }}>Rating</option>
                                        <option value="views"  {{ $olddata['allfilter'] == "views" ?  "selected":'' }}>Views</option>
                                        <option value="release"{{ $olddata['allfilter'] == "release" ?"selected":'' }}>Release Date</option>
                                        <option value="recent" {{ $olddata['allfilter'] == "recent" ? "selected":'' }}>Recent</option>
                                        <option value="most"   {{ $olddata['allfilter'] == "most" ?   "selected":'' }}>Most Download</option>
                                        {{-- @endforeach --}}
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Genre :</label>
                                    <select class="category related-post form-control" name="genre[]" multiple>
                                        @foreach ($genre ?? [] as $item)
                                            @if ($olddata['genre'] !=null)
                                                    @for ($i = 0; $i < sizeof($olddata['genre']); $i++)
                                                        <option value="{{$item->id}}" {{ $olddata['genre'][$i] == $item->id ?   "selected":'' }}  >{{$item->name}}</option>
                                                    @endfor
                                             @else
                                                <option value="{{$item->id}}"  >{{$item->name}}</option>
                                             @endif

                                            {{-- <option value="{{$item->id}}">{{$item->name}}</option> --}}
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Platform :</label>
                                    <select class="category related-post form-control" name="platform[]" multiple >

                                        @foreach ($platform as $item)


                                            @if ($olddata['platform'] !=null)
                                                @for ($i = 0; $i < sizeof($olddata['platform']); $i++)
                                                    <option value="{{$item->id}}" {{ $olddata['platform'][$i] == $item->id ?   "selected":'' }}  >{{$item->name}}</option>
                                                @endfor
                                            @else
                                                <option value="{{$item->id}}"  >{{$item->name}}</option>
                                            @endif



                                        {{-- <option value="{{$item->id}}" selected>{{$item->name}}</option> --}}
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Price :</label>
                                    <select class="category related-post form-control" name="filterprice">
                                        {{-- @foreach ($platform as $item) --}}
                                        <option value="none" {{ $olddata['filterprice'] == "none" ?   "selected":'' }}>-- All -- </option>
                                        <option value="free" {{ $olddata['filterprice'] == "free" ?   "selected":'' }}>Free</option>
                                        <option value="0" {{ $olddata['filterprice'] == "0" ?   "selected":'' }}>0 - 50.000</option>
                                        <option value="50" {{ $olddata['filterprice'] == "50" ?   "selected":'' }}>50.000 - 100.000</option>
                                        <option value="100"{{ $olddata['filterprice'] == "100" ?   "selected":'' }}>100.000 - 200.000</option>
                                        <option value="200"{{ $olddata['filterprice'] == "200" ?   "selected":'' }}>200.000 - 500.000</option>
                                        <option value="500"{{ $olddata['filterprice'] == "500" ?   "selected":'' }}>500.000++</option>
                                        {{-- @endforeach --}}
                                    </select>
                                </div>
                            @else
                                <div class="filter_group">
                                    <label class="filter_label">Keyword:</label>
                                    <input type="text" name="key" class="filter_input bg-light form-control" placeholder="Keyword"  >
                                </div>


                                <div class="form-group">
                                    <label class="filter_label">Sort By:</label>
                                    <select class="category related-post form-control" name="allfilter">
                                        {{-- @foreach ($platform as $item) --}}
                                        <option value="none">-- No Sort --</option>
                                        <option value="asc">Price Ascending</option>
                                        <option value="desc">Price Descending</option>
                                        <option value="rating">Rating</option>
                                        <option value="views">Views</option>
                                        <option value="release">Release Date</option>
                                        <option value="recent">Recent</option>
                                        <option value="most">Most Download</option>
                                        {{-- @endforeach --}}
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Genre :</label>
                                    <select class="category related-post form-control" name="genre[]" multiple>
                                        @foreach ($genre ?? [] as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Platform :</label>
                                    <select class="category related-post form-control" name="platform[]" multiple >
                                        @foreach ($platform as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        {{-- <option value="{{$item->id}}" selected>{{$item->name}}</option> --}}
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Price :</label>
                                    <select class="category related-post form-control" name="filterprice">
                                        {{-- @foreach ($platform as $item) --}}
                                        <option value="none">-- All -- </option>
                                        <option value="free">Free</option>
                                        <option value="0">0 - 50.000</option>
                                        <option value="50">50.000 - 100.000</option>
                                        <option value="100">100.000 - 200.000</option>
                                        <option value="200">200.000 - 500.000</option>
                                        <option value="500">500.000++</option>
                                        {{-- @endforeach --}}
                                    </select>
                                </div>
                            @endif

                        <div class="filter_group">
                            <button class="fag-btn" type="submit">APPLY FILTER</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="products-grid">
                    <div class="row">
                        <div class="col-12">
                            <div class="games-masonary">
                                <div class="clearfix gamesContainer">
                                    @foreach ($list_game as $key => $game)
                                    <div class="games-item">
                                        <div class="games-thumb">
                                            <div class="games-thumb-image">
                                                <a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}">
                                                    <img src="/storage/{{ $game->thumbnail_filename}}" alt="product" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="games-desc">
                                            <h3 class="text-left"><a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}">{{ $game->name }}</a></h3>
                                            <h6><a href="/developers/{{ $game->developer->developer->slug_url }}">{{ $game->developer->developer->slug_url }}</a></h6>
                                            <h6 class="mt-3">{{ $game->genres->implode('name', ' | ') }}</h6>
                                            @if ($game->rating() != 0)
                                            <div class="game-rating">
                                                <h4>{{ $game->rating() }}</h4>
                                                <ul>
                                                    @for ($i = 0; $i < 5; $i++)
                                                    @if ($i+1 <= $game->rating())
                                                    <li><span class="fa fa-star"></span></li>
                                                    @else
                                                    <li><span class="fa fa-star-o"></span></li>
                                                    @endif
                                                    @endfor
                                                </ul>
                                            </div>
                                            @endif
                                            <div class="game-action">
                                                <div class="game-price">
                                                    @if ($game->price == 0)
                                                    <h4 class="text-success">Free</h4>
                                                    @elseif($game->price == $game->discprice())
                                                    <h4>@idr( $game->price )</h4>
                                                    @else
                                                    <h4><del>@idr( $game->price )</del></h4>
                                                    @if ($game->discprice() == 0)
                                                    <h4 class="text-success">Free</h4>
                                                    @else
                                                    <h4>@idr( $game->discprice() ) </h4>
                                                    @endif
                                                    @endif
                                                </div>
                                                <div class="game-buy ml-3">
                                                    @if ($user !== null && $user->games()->where('id', $game->id)->first() !== null)
                                                    <h5 class="text-success">Owned</h5>
                                                    @else
                                                    <form action="/cart/add" method="post">
                                                        @csrf
                                                        <input type="hidden" name="game_slug" value="{{ $game->slug_url }}">
                                                        <button class="fag-btn">Buy Now!</button>
                                                    </form>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Game Page End -->
@endsection


