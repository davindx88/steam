@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Cart</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Cart</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- Cart Page Start -->
<section class="fag-cart-page-area section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="cart-table-left">
                    <h3>Shopping Cart</h3>
                    <div class="table-responsive cart_box">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Preview</th>
                                    <th>Game</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($cart as $key => $item)
                                <tr class="shop-cart-item">
                                    <td class="fag-cart-preview">
                                        <a href="/developers/{{ $item['game']->developer->developer->slug_url }}/{{ $item['game']->slug_url }}">
                                            <img src="/storage/{{  $item['game']->thumbnail_filename}}"  alt="cart-1">
                                            {{-- <img src="{{ url('faf/assets/img/product-1.png') }}" alt="cart-1"> --}}
                                        </a>
                                    </td>
                                    <td class="fag-cart-product">
                                        <a href="/developers/{{ $item['game']->developer->developer->slug_url }}/{{ $item['game']->slug_url }}">
                                            <p>{{ $item['game']->name }}</p>
                                        </a>
                                    </td>
                                    <td class="fag-cart-price">
                                        @if ($item['game']->price == 0)
                                        <p class="text-success">Free</p>
                                        @elseif($item['game']->price == $item['game']->discprice())
                                        <p>@idr( $item['game']->price )</p>
                                        @else
                                        <p><del>@idr( $item['game']->price )</del></p>
                                        @if ($item['game']->discprice() == 0)
                                        <p class="text-success">Free</p>
                                        @else
                                        <p>@idr( $item['game']->discprice() ) </p>
                                        @endif
                                        @endif
                                    </td>
                                    <td class="fag-cart-close">
                                        <form action="/cart/delete" method="post">
                                            @csrf
                                            <input type="hidden" name="game_slug" value="{{ $item['game']->slug_url }}">
                                            <button class="btn btn-outline-warning"><i class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Cart is empty.
                                        <a href="/search">Explore store now</a>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="cart-clear">
                        <form action="/cart/clear" method="post">
                            @csrf
                            <button class="btn btn-outline-warning">Clear Cart</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="order-summury-box">
                    <h3>Order Summary</h3>
                    <div class="summury-inn">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Cart Total</td>
                                    @if ($cart_total > 0)
                                    <td>@idr( $cart_total )</td>
                                    @elseif($cart_total === -1)
                                    <td class="text-warning">-</td>
                                    @else
                                    <td class="text-success">Free</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    @if ($discount > 0)
                                    <td class="text-success">@idr($discount)</td>
                                    @else
                                    <td class="text-warning">-</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Final Total</td>
                                    @if ($final_total > 0)
                                    <td>@idr($final_total)</td>
                                    @elseif($cart_total === -1)
                                    <td class="text-warning">-</td>
                                    @else
                                    <td class="text-success">Free</td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="checkout-action">
                    <a href="/cart/checkout" class="fag-btn">Proceed to checkout</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Cart Page End -->
@endsection
