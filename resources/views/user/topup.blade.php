@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Topup</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Topup</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- Match Page Start -->
<section class="fag-match-page section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="match-list-heading">
                    <h3>List <span>Topup</span></h3>
                    <p>Funds in your Wallet may be used for the purchase of any game in this website.</p>
                </div>
                <div class="fag-matchs-list">
                    @foreach ($list_topup as $key => $topup)
                    <div class="single-match-list">
                        <div class="match-box-middle">
                            <div class="matchcountdown">
                                <h3>Add @idr( $topup )</h3>
                            </div>
                        </div>
                        <div class="match-box-action">
                            <form action="/accounts/topup" method="post">
                                @csrf
                                <input type="hidden" name="nominal" value="{{ $topup }}">
                                <button class="fag-btn">Add Funds</button>
                            </form>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="col-lg-4">
                <div class="match-page-right">
                    <div class="single-match-widget">
                        <h3>Your <span>Balance</span></h3>
                        <div class="match-widget-inn">
                            <img src="/storage/{{$user->filename}}" alt="gamer" />
                            <div class="match-score">
                                <h4><span>@idr( $user->balance)</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Match Page End -->
@endsection
