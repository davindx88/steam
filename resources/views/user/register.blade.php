<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="Faf | Gaming HTML Template from Themescare">
      <meta name="keyword" content="game, gaming, videogame, developer, steam, studio, team">
      <meta name="author" content="Themescare">
      <!-- Title -->
      <title>Faf | Gaming HTML Template</title>
      <!-- Favicon -->
      <link rel="icon" type="image/png" sizes="32x32" href="{{ url('faf/assets/img/favicon/favicon-32x32.png') }}">
      <!--Bootstrap css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/bootstrap.css') }}">
      <!--Font Awesome css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/font-awesome.min.css') }}">
      <!--Magnific css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/magnific-popup.css') }}">
      <!--Owl-Carousel css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ url('faf/assets/css/owl.theme.default.min.css') }}">
      <!--NoUiSlider css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/nouislider.min.css') }}">
      <!--Animate css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/animate.min.css') }}">
      <!--Site Main Style css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/style.css') }}">
      <!--Responsive css-->
      <link rel="stylesheet" href="{{ url('faf/assets/css/responsive.css') }}">
   </head>
   <body>


      <!-- Custom Cursor Start -->
      <div id="cursor-large"></div>
      <div id="cursor-small"></div>
      <!-- Custom Cursor End -->


      <!-- page 404 -->
      <div class="page-404 section--full-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="page-404__wrap">
                     <div class="login-wrapper">
                        <h3>Create Account</h3>
                        <form action="/accounts/register" method="POST">
                            @csrf
                            <div class="form-row">
                                <input type="text" name="first_name" placeholder='First Name'
                                    value="{{ old('first_name') }}">
                            </div>
                            @error('first_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <div class="form-row">
                                <input type="text" name="last_name" placeholder='Last Name'
                                    value="{{ old('last_name') }}">
                            </div>
                            @error('last_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <div class="form-row">
                                <input type="text" name="username" placeholder="Username"
                                    value="{{ old('username') }}"/>
                            </div>
                            @error('username')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <div class="form-row">
                                <input type="email" name="email" placeholder="Email Address"
                                    value="{{ old('email') }}"/>
                            </div>
                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <div class="form-row">
                                <input type="password" name="password" placeholder="Password"
                                    value="{{ old('password') }}"/>
                            </div>
                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <div class="form-row">
                                <input type="password" name="password_confirmation" placeholder="Confirm Password"
                                    value="{{ old('password_confirmation') }}"/>
                            </div>
                            @error('password_confirmation')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                            {{-- <div class="form-row">
                                <div class="custom-checkbox">
                                    <input id="terms" type="checkbox" name="terms" checked="checked">
                                    <label for="terms">I agree to the <a href="#">Privacy Policy</a></label>
                                    <span class="checkbox"></span>
                                </div>
                            </div> --}}
                            <div class="col-12 form-row">
                                <button class="fag-btn" type="submit">Create your Account!</button>
                            </div>
                            <span>Already have an account? <a href="/login">Sign in</a></span>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end page 404 -->


      <!--Jquery js-->
      <script src="{{ url('faf/assets/js/jquery.min.js') }}"></script>
      <!-- Popper JS -->
      <script src="{{ url('faf/assets/js/popper.min.js') }}"></script>
      <!--Bootstrap js-->
      <script src="{{ url('faf/assets/js/bootstrap.min.js') }}"></script>
      <!--Owl-Carousel js-->
      <script src="{{ url('faf/assets/js/owl.carousel.min.js') }}"></script>
      <!--Magnific js-->
      <script src="{{ url('faf/assets/js/jquery.magnific-popup.min.js') }}"></script>
      <!--wNumb js-->
      <script src="{{ url('faf/assets/js/wNumb.js') }}"></script>
      <!--NoUiSlider js-->
      <script src="{{ url('faf/assets/js/nouislider.min.js') }}"></script>
      <!-- Isotop Js -->
      <script src="{{ url('faf/assets/js/isotope.pkgd.min.js') }}"></script>
      <script src="{{ url('faf/assets/js/custom-isotop.js') }}"></script>
      <!-- Counter JS -->
      <script src="{{ url('faf/assets/js/jquery.counterup.min.js') }}"></script>
      <!-- Way Points JS -->
      <script src="{{ url('faf/assets/js/waypoints-min.js') }}"></script>
      <!--Main js-->
      <script src="{{ url('faf/assets/js/main.js') }}"></script>
   </body>
</html>

