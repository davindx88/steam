@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Checkout</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><a href="/cart">Cart</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Checkout</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- Cart Page Start -->
<section class="fag-cart-page-area section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="cart-table-left">
                    <h3>Shopping Cart</h3>
                    <div class="table-responsive cart_box">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Preview</th>
                                    <th>Game</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($cart as $key => $item)
                                <tr class="shop-cart-item">
                                    <td class="fag-cart-preview">
                                        <a href="/developers/{{ $item['game']->developer->developer->slug_url }}/{{ $item['game']->slug_url }}">
                                            <img src="/storage/{{  $item['game']->thumbnail_filename }}"  alt="cart-1">
                                        </a>
                                    </td>
                                    <td class="fag-cart-product">
                                        <a href="/developers/{{ $item['game']->developer->developer->slug_url }}/{{ $item['game']->slug_url }}">
                                            <p>{{ $item['game']->name }}</p>
                                        </a>
                                    </td>
                                    <td class="fag-cart-price">
                                        @if ($item['game']->price == 0)
                                        <p class="text-success">Free</p>
                                        @elseif($item['game']->price == $item['game']->discprice())
                                        <p>@idr( $item['game']->price )</p>
                                        @else
                                        <p><del>@idr( $item['game']->price )</del></p>
                                        @if ($item['game']->discprice() == 0)
                                        <p class="text-success">Free</p>
                                        @else
                                        <p>@idr( $item['game']->discprice() ) </p>
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Cart is empty.
                                        <a href="/search">Explore store now</a>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-lg-8">
                <div class="order-summury-box">
                    <h3>Order Summary</h3>
                    <div class="summury-inn">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Cart Total</td>
                                    @if ($cart_total > 0)
                                    <td>@idr( $cart_total)</td>
                                    @elseif($cart_total === -1)
                                    <td class="text-warning">-</td>
                                    @else
                                    <td class="text-success">Free</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    @if ($discount > 0)
                                    <td class="text-success">@idr( $discount)</td>
                                    @else
                                    <td class="text-warning">-</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Final Total</td>
                                    @if ($final_total > 0)
                                    <td>@idr($final_total )</td>
                                    @elseif($cart_total === -1)
                                    <td class="text-warning">-</td>
                                    @else
                                    <td class="text-success">Free</td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h3 class="mt-5">Billing Details</h3>
                <form action="/cart/submit" method="POST">
                    @csrf
                    <div class="filter_group mt-3">
                        <label for="purchase_type" class="filter_label">Purchase Type</label>
                        <div class="filter_select-wrap">
                            <select name="purchase_type" id="purchase_type" class="filter_select bg-dark text-light">
                                <option value="0">Purchase for myself</option>
                                <option value="1">Purchase as a gift</option>
                            </select>
                        </div>
                    </div>
                    <div class="filter_group" id="friend_div">
                        <label for="friend" class="filter_label">Friend</label>
                        <div class="filter_select-wrap">
                            <select name="friend" id="friend" class="filter_select bg-dark text-light">
                                <option disabled selected value> -- select a friend -- </option>
                            @foreach ($list_friend as $key => $friend)
                                <option value="{{ $friend->username }}">
                                    {{ $friend->first_name }}
                                    {{ $friend->last_name }}
                                </option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="filter_group" id="message_div">
                        <label for="message" class="filter_label">Message</label>
                        <textarea name="message" id="message" class="form-control bg-dark text-light"
                            placeholder="Message to friend"></textarea>
                    </div>
                    <div class="checkout-action">
                        <button class="fag-btn">Proceed to checkout</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Cart Page End -->
@endsection

@section('script')
<script>
    $(()=>{
        $('#purchase_type').change(function(){
            const type = $(this).val()
            if(type == 0){
                $('#friend_div').slideUp()
                $('#message_div').slideUp()
                $('#friend').attr('required',false)
                $('#message').attr('required',false)
            }else{
                $('#friend_div').slideDown()
                $('#message_div').slideDown()
                $('#friend').attr('required',true)
                $('#message').attr('required',true)
            }
        })
        // UI
        $('#friend_div').hide()
        $('#message_div').hide()
    })
</script>
@endsection
