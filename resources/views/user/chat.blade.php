@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Chat with {{ $friend->name() }}</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><a href="/friends">Friends</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>{{ $friend->username }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div>
        <!-- DIRECT CHAT PRIMARY -->
        <div class="card card-primary bg-light card-outline direct-chat direct-chat-primary">
            <div class="card-header">
                <h3 class="card-title">Direct Chat</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                    @foreach ($room->chats as $key => $chat)
                    @if ($chat->sender_id == $user->id)
                    <div class="direct-chat-msg d-flex">
                        <div class="msg-user mb-3">
                            <div class="bg-primary text-light rounded p-3 position-relative">
                                <p>{{ $chat->message }}</p>
                            </div>
                            <div class="text-right text-dark">
                                {{ $chat->created_at->isoFormat('HH:mm') }}
                            </div>
                        </div>
                        <img src="/storage/{{ $user->filename }}"
                                alt="author" class="img-user ml-3 rounded-circle">
                    </div>
                    @else
                    <div class="direct-chat-msg d-flex">
                        <img src="/storage/{{ $room->friend($user->id)->filename }}"
                                alt="author" class="img-user mr-3 rounded-circle">
                        <div class="msg-user mb-3">
                            <div class="bg-secondary text-light rounded p-3 position-relative">
                                <p>{{ $chat->message }}</p>
                            </div>
                            <span class="text-dark">
                                {{ $chat->created_at->isoFormat('HH:mm') }}
                            </span>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <!--/.direct-chat-messages-->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="input-group">
                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                    <span class="input-group-append">
                        <button id="btn_send" type="submit" class="btn btn-primary">Send</button>
                    </span>
                </div>
            </div>
            <!-- /.card-footer-->
        </div>
        <!--/.direct-chat -->
    </div>
</div>
@endsection

@section('script')
<script>
$(()=>{
    function addComp({id, message, created_at, is_sender}){
        if(is_sender){
            $('.direct-chat-messages').append(`
                <div class="direct-chat-msg d-flex">
                    <div class="msg-user mb-3">
                        <div class="bg-primary text-light rounded p-3 position-relative">
                            <p>${message}</p>
                        </div>
                        <div class="text-right text-dark">
                            ${new Date(created_at).getHours()}:${new Date(created_at).getMinutes()}
                        </div>
                    </div>
                    <img src="/storage/{{ $user->filename }}"
                            alt="author" class="img-user ml-3 rounded-circle">
                </div>
            `)
        }else{
            $('.direct-chat-messages').append(`
                <div class="direct-chat-msg d-flex">
                    <img src="/storage/{{ $room->friend($user->id)->filename }}"
                            alt="author" class="img-user mr-3 rounded-circle">
                    <div class="msg-user mb-3">
                        <div class="bg-secondary text-light rounded p-3 position-relative">
                            <p>${message}</p>
                        </div>
                        <span class="text-dark">
                            ${new Date(created_at).getHours()}:${new Date(created_at).getMinutes()}
                        </span>
                    </div>
                </div>
            `)
        }
    }

    let last_chat = {{ $room->chats->count() > 0 ? $room->chats[$room->chats->count()-1]->id : -1 }}
    let ngirim = false

    $('#btn_send').click(()=>{
        const message = $('input[name=message]').val()
        ngirim = true
        $('input[name=message]').val('')
        $.ajax({
            type:'POST',
            url:'/chats/insert',
            data: {
                '_token' : "{{ csrf_token() }}",
                'room' : "{{ $room->id }}",
                'message' : message,
            },
            success: function(data) {
                last_chat = data.id;
                ngirim = false
                addComp(data)
            }
        });
    })
    // Load image 1 detik sekali
    setInterval(() => {
        if(ngirim) return;
        $.ajax({
            type:'POST',
            url:'/chats/get',
            data: {
                '_token' : "{{ csrf_token() }}",
                'room' : "{{ $room->id }}",
                'last_id' : last_chat,
            },
            success: function(arr_chat) {
                arr_chat.forEach((data) => {
                    last_chat = data.id
                    addComp(data)
                });
            }
        });
    }, 1000);
})
</script>
@endsection
