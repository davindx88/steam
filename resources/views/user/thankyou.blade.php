@extends('layout.user')
@section('content')
<div class="container text-center mt-5 section_140">
    @if (session('thankyoupage') == 1)
    <h1 class="display-1">
        <b>Thank you!</b>
    </h1>
    <span>your order was successfully completed.</span>
    @else
    <h1 class="display-1">
        <b>Hi, are you lost?</b>
    </h1>
    <span>consider yourself to explore and buy some game in our website</span>
    @endif
    <div class="mt-5">
        <a href="/search" class="fag-btn">Explore Shop</a>
    </div>
</div>
@endsection
