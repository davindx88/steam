@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Friend Requests</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><a href="/friends">Friends</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Requests</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div class="tv-panel-list min-h50">
        <div class="tv-tab">
            <ul class="nav nav-pills tv-tab-switch" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pills-pending-tab" data-toggle="pill" href="#pills-pending" role="tab"
                        aria-controls="pills-pending" aria-selected="true">Pending</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-requested-tab" data-toggle="pill" href="#pills-requested" role="tab"
                        aria-controls="pills-requested" aria-selected="false">Requested</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade active show" id="pills-pending" role="tabpanel" aria-labelledby="pills-pending-tab">
                <div class="tab-gamess-details">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-body">
                                @forelse ($list_pending as $key => $freq)
                                <div class="summury-inn clear profile-game">
                                    <img class="left-img" src="/storage/{{ $freq->sender->filename }}" alt="Game">
                                    <div class="left-div">
                                        <h2><a href="/users/{{ $freq->sender->username }}">{{ $freq->sender->username }}</a></h2>
                                        <h6>{{ $freq->sender->first_name }} {{ $freq->sender->last_name }}</h6>
                                        <h6 class="alert bg-dark mt-2"><p>{{ $freq->message }}</p></h6>
                                        <h6><sub>{{ Carbon\Carbon::parse($freq->created_at)->diffForHumans() }}</sub></h6>
                                        <form action="/friends/response" method="post" class="mt-3">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $freq->id }}">
                                            <button name='resp' value='1' class="btn btn-warning">Accept</button>
                                            <button name='resp' value='-1' class="btn btn-outline-warning">Decline</button>
                                        </form>
                                    </div>
                                </div>
                                @empty

                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-requested" role="tabpanel" aria-labelledby="pills-requested-tab">
                <div class="tab-gamess-details">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-body">
                                @forelse ($list_requested as $key => $freq)
                                <div class="summury-inn clear profile-game">
                                    <img class="left-img" src="/storage/{{ $freq->receiver->filename }}" alt="Game">
                                    <div class="left-div">
                                        <h2><a href="/users/{{ $freq->receiver->username }}">{{ $freq->receiver->username }}</a></h2>
                                        <h4>{{ $freq->receiver->first_name }} {{ $freq->receiver->last_name }}</h4>
                                        <h6>Status: <span class="badge {{ ['badge-danger','badge-warning','badge-success'][$freq->status+1] }}">
                                            {{ ['Rejected','Waiting','Accepted'][$freq->status+1] }}
                                        </span></h6>
                                        <h6><sub>{{ Carbon\Carbon::parse($freq->created_at)->diffForHumans() }}</sub></h6>
                                    </div>
                                </div>
                                @empty

                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@error('msg')
<script>
    // TODO pakai Sweet Alert
    alert('{{ $message }}')
</script>
@enderror
@endsection

@section('script')
<script>
    $(()=>{
        $('#myform').submit(()=>{

        })
    })
</script>
@endsection
