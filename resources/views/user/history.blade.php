@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>History Transaction</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>History</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->
<div class="container p-5 mt-5 position-relative">
    <h1>History</h1>
    <div class="table-responsive cart_box">
        <table class="table text-light">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Time</th>
                    <th>Description</th>
                    <th>Balance</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $ctr = 1
                @endphp
                @forelse ($history_transaction as $key => $history)
                <tr>
                    <td>{{ $ctr++ }}</td>
                    <td>{{ $history['time']->isoFormat('HH:mm DD/MM/YYYY') }}</td>
                    <td>
                        @if ($history['type'] == 0)
                        Top up <strong>@idr($history['nominal'])</strong>
                        @endif
                        @if ($history['type'] == 1)
                        Buy Game(s)
                        <ul>
                            @foreach ($history['object']->games as $k => $game)
                            <li>- {{$game->name}} <strong>@idr($game->pivot->price)</strong></li>
                            @endforeach
                        </ul>
                        @endif
                        @if ($history['type'] == 2)
                        Gift rejected
                        <ul>
                            @foreach ($history['object']->transaction->games as $k => $game)
                            <li>- {{$game->name}} <strong>@idr($game->pivot->price)</strong></li>
                            @endforeach
                        </ul>
                        @endif
                    </td>
                    <td>
                        @if ($history['type'] == 1)
                        <span class="text-danger">@idr($history['nominal'])</span>
                        @else
                        <span class="text-success">@idr($history['nominal'])</span>
                        @endif
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="10" class="text-center">
                        There's no transaction
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
