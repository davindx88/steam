@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Edit Profile</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Edit Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div class="checkout-left-box">
        <h3>Profile</h3>
        <form  action="/accounts/editprofile" method="POST"  enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row checkout-form">
                        <label for="picture">Upload Profile Picture</label>
                    </div>
                    <div class="row">
                            <input type="hidden" name="iduser" value="{{$user->id}}" >
                            <input type="hidden" name="type" value="{{$user->type}}" >
                            <input type="hidden" name="filename" value="{{$user->filename}}" >
                            {{-- {{$user->filename}} --}}

                            @if ($user->filename == '')
                                <img style="object-fit: cover;width: 150px;height: 150px;"  src="/iconprofile.png" alt="asd" class="edit-profile-img" id="img">
                            @else
                                <img style="object-fit: cover;width: 150px;height: 150px;"  src="/storage/{{$user->filename}}" alt="asd" class="edit-profile-img" id="img">
                            @endif

                            <input type="file" name="picture" id="picture"  onchange="readURL(this);" >

                        {{-- <div class="col-md-6">
                            <input type="file" name="picture" id="picture" onchange="readURL(this);">
                        </div> --}}


                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row checkout-form">
                        <div class="col-md-6">
                            <label for="picture">Firt Name</label>
                            <input placeholder="Your First Name" type="text" name="firstname" id="firstname"
                            value="{{ old('first_name',$user->first_name) }}">
                        </div>
                        <div class="col-md-6">
                            <label for="picture">Last Name</label>
                            <input placeholder="Your Last Name" type="text" name="lastname" id="lastname"
                            value="{{ old('first_name',$user->last_name) }}">
                        </div>
                    </div>
                    <div class="row checkout-form">
                        <div class="col-md-6">
                            <label for="picture">Email</label>
                            <input disabled placeholder="Your First Name" type="text" name="firstname" id="firstname"
                            value="{{ old('first_name',$user->email) }}">
                        </div>
                        <div class="col-md-6">
                            <label for="picture">Username</label>
                            <input disabled placeholder="Your Username" type="text" name="username" id="firstname"
                            value="{{ old('first_name',$user->username) }}">
                        </div>
                        {{-- <div class="col-md-6">
                            <label for="picture">Last Name</label>
                            <input placeholder="Your Last Name" type="text" name="lastname" id="lastname"
                            value="{{ old('first_name',$user->last_name) }}">
                        </div> --}}
                    </div>
                    <div class="row checkout-form">
                        <div class="col-md-12">
                            <button class="fag-btn" type="submit">Save</button>
                        </div>
                        {{-- <div class="col-md-6">
                            <label for="picture">Last Name</label>
                            <input placeholder="Your Last Name" type="text" name="lastname" id="lastname"
                            value="{{ old('first_name',$user->last_name) }}">
                        </div> --}}
                    </div>

                </div>
            </div>

            {{-- <div class="row checkout-form">
                <div class="col-md-12">
                    <label for="firstname">Username</label>
                    <input type="text" name="firstname" id="name23" disabled
                        value="{{ $user->username }}">
                </div>
            </div>
            <div class="row checkout-form">
                <div class="col-md-6">
                    <label for="firstname">First Name</label>
                    <input placeholder="Your First Name" type="text" name="firstname" id="firstname"
                        value="{{ old('first_name',$user->first_name) }}">
                </div>
                <div class="col-md-6">
                    <label for="lastname">Last Name</label>
                    <input placeholder="Your Last Name" type="text" name="lastname" id="lastname"
                        value="{{ old('first_name',$user->last_name) }}">
                </div>
            </div>
            <div class="row checkout-form">
                <div class="col-md-4 text-center">
                    <img src="" alt="asd" class="edit-profile-img" id="img">
                </div>
                <div class="col-md-8">
                    <label for="picture">Profile Picture</label>
                    <input type="file" name="picture" id="picture" onchange="readURL(this);">
                </div>
            </div> --}}
        </form>
    </div>
</div>
@endsection
@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img').attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
