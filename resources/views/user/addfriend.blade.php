@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Add Friend</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><a href="/friends">Friends</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Add</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div class="contact-form-inn">
        <h3>Add Friend</h3>
        <form action="/friends/addfriend" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field">
                        @error('username')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input class="ns-com-name" name="username" placeholder="username" type="text"
                            value="{{ $username=='' ? old('username') : $username }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field">
                        @error('message')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <textarea class="message" placeholder="Write your message here..." name="message" >{{ old('message') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field form-action">
                        <button class="fag-btn">Add Friend</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
    $(()=>{
        $('#myform').submit(()=>{

        })
    })
</script>
@endsection
