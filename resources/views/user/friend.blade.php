@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Friends</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Friends</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div class="float-right">
        <a href="/friends/add" class="btn btn-warning mr-2"><i class="fa fa-plus-square"></i>Add Friend</a>
        <a href="/friends/request" class="btn btn-warning"><i class="fa fa-user-plus"></i>Friend Request</a>
    </div>
    <h1>Friend</h1>
    @foreach ($chatroom as $key => $room)
    <div class="summury-inn clear profile-game">
        <img src="/storage/{{ $room->friend($user->id)->filename }}" style="width: 100px; height: 100px;"
                            alt="author" class="img-user mr-3 rounded-circle float-left">
        <div class="left-div">
            <h2><a href="/users/{{ $room->friend($user->id)->username }}">{{ $room->friend($user->id)->username }}</a></h2>
            <h6>{{ $room->friend($user->id)->first_name }} {{ $room->friend($user->id)->last_name }}</h6>
            <form action="/friends/unfriend" method="post">
                @csrf
                <input type="hidden" name="chatroom_id" value="{{ $room->id }}">
                <div class="game-buy">
                    <a class="btn btn-primary" href="/friends/{{ $room->friend($user->id)->username }}">Chat</a>
                    <button class="btn btn-outline-danger">Unfriend</button>
                </div>
            </form>
        </div>
    </div>
    @endforeach
</div>
@endsection

@section('script')
<script>
</script>
@endsection
