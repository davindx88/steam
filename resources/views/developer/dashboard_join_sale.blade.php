@extends('layout.developer')
@section('content')

<section class="fag-game-page section_100">
    <div class="container">
        <h1>{{$data[0]->title}}</h1>
        <h3>{{$data[0]->description}}</h3> <br> <br>
       <div class="row">
          <div class="col-lg-9">
             <div class="games-category">
                <div class="row">
                   @foreach ($data[1] as $item)

                       <div class="col-lg-4 col-sm-6">
                       <div class="games-single-item img-contain-isotope">
                          <div class="games-thumb">
                             <div class="games-thumb-image">
                                <a href="#">
                                <img src="/storage/{{$item->thumbnail_filename}}" alt="games" />
                                </a>
                             </div>
                          </div>
                          <div class="games-desc">
                             <h3><a href="#">{{$item->name}}</a></h3>
                             <p class="game-meta">@idr($item->price)</p>
                          <p class="game-meta">Release date:<span> {{$item->created_at}}</span></p>
                          <span class="single_rating"><i class="fa fa-star"></i>{{ $item->rating() }}</span>
                            <div class="game-rating">
                            </div>
                            <div class="game-action">
                                 <form action="/dashboard/addsales" method="post">
                                    @csrf
                                    <div class="game-price">
                                        Discount
                                       <input type="number" name="discount" id="" min="0" max="100">%
                                    </div> <br>
                                    <input type="hidden" name="gamesid" value="{{$item->id}}">
                                    <input type="hidden" name="salesid" value="{{$data[0]->id}}">
                                    <input type="submit" value="Apply this game" class="fag-btn-outline">

                                </form>
                                </div>
                          </div>
                       </div>
                    </div>
                   @endforeach

                </div>
             </div>
          </div>
       </div>
    </div>
 </section>

@endsection


