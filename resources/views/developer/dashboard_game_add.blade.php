@extends('layout.developer')
@section('content')

<div class="page-404 section--full-bg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-404__wrap">
                    <div class="checkout-left-box">
                        <div class="login-wrapper">
                            <h3>Add Your Game Here</h3>
                            <form method="POST" action="/dashboard/addgame" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <input type="text" placeholder="Game Name" name="name" required />
                                    </div>
                                    <div class="form-row">
                                        <input type="text" placeholder="Description" name="description" required />
                                    </div>
                                    <div class="form-row">
                                        <input type="number" placeholder="Price" name="price" required />
                                    </div>
                                    <div class="form-row">
                                    </div>

                                    <div class="form-group">
                                        <label>Platform :</label>
                                    </div>
                                    <select class="category related-post form-control" name="platform[]" multiple>
                                        @foreach ($data[0] as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                    <div class="form-group">
                                        <label>Genre :</label>
                                    </div>
                                    <select class="category related-post form-control" name="genre[]" multiple>
                                        @foreach ($data[1] as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-group">
                                        <label>Features :</label>
                                    </div>
                                    <fieldset>
                                        <div id="extender"></div>
                                        <a id="add" href="#">Add</a>
                                    </fieldset>
                                    <div class="form-row">
                                        <label for="imageGame">Thumbnail Image</label>
                                        <input type="file" name="imageGame" id="imageGame" required>
                                    </div>
                                    <div class="form-row">
                                        <label for="backgroundImage">Background Image</label>
                                        <input type="file" name="backgroundImage" id="backgroundImage" required>
                                    </div>
                                    <div class="form-row">
                                        <label for="fileData">File Data</label>
                                        <input type="file" name="fileData" id="fileData" required>
                                    </div>
                                    <div class="form-row"></div>
                                    <div class="form-row">
                                        <button class="fag-btn" type="submit">Add your Game!</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('.category').select2();
    });
</script>
<pre lang="javascript">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">

   $(function() {

        //set a counter
        var i =  1;

        //add input
        $('a#add').click(function() {
            $('<div class="form-row"> <input type="text" placeholder="Feature Name " name="featuresname[]" id="' + i + '" /><br> <input type="text" placeholder="Descriptions" name="featuredescription[]" id="' + i + '" /><br>  <br> <input type="file" name="image[]" id="' + i + '" value="' + i + '" /> </div>').fadeIn("slow").appendTo('#extender');
            i++;
            return false;
        });
    })

 </script>
@endsection
