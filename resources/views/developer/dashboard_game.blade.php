
@extends('layout.developer')
@section('content')

<section class="fag-breadcrumb-area">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="breadcromb-box">
                <h3>Our <span>Games</span></h3>
                <ul>
                   <li><i class="fa fa-home"></i></li>
                   <li><a href="index.html">Home</a></li>
                   <li><i class="fa fa-angle-right"></i></li>
                   <li>Our Games</li>
                </ul>
             </div>
          </div>
       </div>
    </div>
 </section>
      <!-- Game Page Start -->
      <section class="fag-games-area section_140">
        <div class="container">
           <div class="row">
              <div class="col-12">
                 <div class="games-masonary">
                    <div class="projectFilter project-btn">
                       <ul>
                          <li><a href="#" data-filter="*" class="current">show all</a></li>
                          <li><a href="#" data-filter=".verified">Verified</a></li>
                          <li><a href="#" data-filter=".pending">Pending</a></li>
                          <li><a href="#" data-filter=".rejected">Rejected</a></li>
                       </ul>
                    </div>
                    <div class="clearfix gamesContainer">
                        @foreach ($games as $item)
                            @if ($item->status==0)
                                <div class="games-item pending">
                                    <div class="games-single-item img-contain-isotope">
                                    <div class="games-thumb">
                                        <div class="games-thumb-image">
                                            <a href="/dashboard/games/{{$item->slug_url}}">
                                                <img src={{url('/storage/'.$item->thumbnail_filename)}} alt="games" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="games-desc">
                                        <h3><a href="/dashboard/games/{{$item->slug_url}}">{{$item->name}}</a></h3>
                                        <p class="game-meta">Created at:<span> {{$item->created_at}}</span></p>
                                        <div class="game-action">
                                            <div class="game-price">
                                                <h4>@idr($item->price)</h4>
                                            </div>
                                            <div class="game-buy">
                                                <a href="/dashboard/games/{{$item->slug_url}}" class="fag-btn-outline">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            @elseif($item->status==1)
                                <div class="games-item verified">
                                    <div class="games-single-item img-contain-isotope">
                                    <div class="games-thumb">
                                        <div class="games-thumb-image">
                                            <a href="/dashboard/games/{{$item->slug_url}}">
                                                <img src={{url('/storage/'.$item->thumbnail_filename)}} alt="games" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="games-desc">
                                        <h3><a href="/dashboard/games/{{$item->slug_url}}">{{$item->name}}</a></h3>
                                        <p class="game-meta">This Game is Verified</p>
                                        <div class="game-action">
                                            <div class="game-price">
                                                <h4>@idr($item->price)</h4>
                                            </div>
                                            <div class="game-buy">
                                                <a href="/dahsboard/games/{{$item->slug_url}}" class="fag-btn-outline">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            @else
                                <div class="games-item rejected">
                                    <div class="games-single-item img-contain-isotope">
                                    <div class="games-thumb">
                                        <div class="games-thumb-image">
                                            <a href="/dashboard/games/{{$item->slug_url}}">
                                                <img src="{{ url('/storage/'.$item->thumbnail_filename) }}" alt="games" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="games-desc">
                                        <h3><a href="/dashboard/games/{{$item->slug_url}}">{{$item->name}}</a></h3>
                                        <p class="game-meta">Message:<span> {{$item->message_admin}}</span></p>
                                        <div class="game-action">
                                            <div class="game-price">
                                                <h4>@idr($item->price)</h4>
                                            </div>
                                            <div class="game-buy">
                                                <a href="/dashboard/games/{{$item->slug_url}}" class="fag-btn-outline">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </section>
      <!-- Game Page End -->

      @endsection
