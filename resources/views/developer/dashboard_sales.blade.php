@extends('layout.developer')
@section('content')

<section class="fag-game-page section_100">
    <div class="container">
        <h1>List of Sales</h1>
       <div class="row">
          <div class="col-lg-9">
             <div class="games-category">
                <div class="row">
                   @foreach ($data as $item)
                       <div class="col-lg-4 col-sm-6">
                       <div class="games-single-item img-contain-isotope">
                          <div class="games-thumb">
                             <div class="games-thumb-image">
                                <a href="#">
                                <img src="/storage/photos/{{$item->filename}}" width="10px" height="70px" alt="games" />
                                </a>
                             </div>
                          </div>
                          <div class="games-desc">
                             <h3><a href="#">{{$item->name}}</a></h3>
                             <p class="game-meta">{{$item->price}}</p>
                             <p class="game-meta">Release date:<span> {{$item->created_at}}</span></p>
                             <div class="game-rating">
                          </div>
                                <div class="game-action">

                                    <button><a href="/dashboard/sales/{{$item->id}}/join">Join Now</a></button>

                                </div>
                            </div>
                       </div>
                    </div>
                   @endforeach

                </div>
             </div>
          </div>
       </div>
    </div>
 </section>

@endsection


