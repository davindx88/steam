@extends('layout.developer')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Edit Profile</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/dashboard">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Edit Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container p-5">
    <div class="checkout-left-box">
        <h3>Profile</h3>
        <form  action="/dashboard/isEditProfile" method="POST"  enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="row checkout-form">
                    </div>
                    <div class="row">
                            <input type="hidden" name="iduser" value="{{$data[0]->id}}" >
                            <input type="hidden" name="type" value="{{$data[0]->type}}" >
                            <input type="hidden" name="filename" value="{{$data[0]->filename}}" >

                            @if ($data[0]->filename == '')
                                <img style="object-fit: cover;width: 150px;height: 150px;"  src="/iconprofile.png" alt="asd" class="edit-profile-img" id="img">
                            @else
                                <img style="object-fit: cover;width: 150px;height: 150px;"  src="{{url('/storage/'.$data[0]->filename)}}" alt="asd" class="edit-profile-img" id="img">
                            @endif
                            <input type="file" name="image" id="" onchange="readURL(this);" >

                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row checkout-form">
                        <div class="col-md-6">
                            <label for="picture">Name</label>
                            <input placeholder="Your Name" type="text" name="firstname" id="firstname"
                            value="{{ old('first_name',$data[1]->name) }}">
                        </div>
                    </div>
                    <div class="row checkout-form">
                        <div class="col-md-6">
                            <label for="picture">Email</label>
                            <input disabled placeholder="Your First Name" type="text" name="firstname" id="firstname"
                            value="{{ old('first_name',$data[0]->email) }}">
                        </div>
                        <div class="col-md-6">
                            <label for="picture">Username</label>
                            <input disabled placeholder="Your Username" type="text" name="username" id="firstname"
                            value="{{ old('first_name',$data[0]->username) }}">
                        </div>
                    </div>
                    <div class="row checkout-form">
                        <div class="col-md-12">
                            <button class="fag-btn" type="submit">Save</button>
                        </div>
                    </div>

                </div>
            </div>

        </form>
    </div>
</div>
@endsection
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img').attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
