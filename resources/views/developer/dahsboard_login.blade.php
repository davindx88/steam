@extends('layout.developer')
@section('content')

<div class="page-404 section--full-bg">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="page-404__wrap">
                <div class="login-wrapper">
                    <h3>Developer Login</h3>
                    <form action="/accounts/login" method="POST">
                        @csrf
                        @error('msg')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                        <div class="form-row">
                        <input type="text" name="username" placeholder="Username or Email" required
                            value="{{ old('username') }}"/>
                        </div>
                        <div class="form-row">
                            <input type="password" name="password" placeholder="Password" required/>
                        </div>
                        <div class="form-row"></div>
                        <div class="form-row">
                            <button class="fag-btn" type="submit">Login to your Account!</button>
                        </div>
                    </form>
                    <span>Dont have an account? <a href="/registerdeveloper">Register now</a></span>
                   </form>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection
