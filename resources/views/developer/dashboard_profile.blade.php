@extends('layout.developer')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area bg-profile-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <div class="profile-image">
                        @if ($data[0]->filename == '')
                                <img style="object-fit: cover;width: 250px;height: 250px;"  src="/iconprofile.png" alt="asd" class="edit-profile-img" id="img">
                            @else
                                <img style="object-fit: cover;width: 250px;height: 250px;border-radius:50%"  src="/storage/{{$data[0]->filename}}" alt="asd" class="edit-profile-img" id="img">
                            @endif
                    </div>
                    <h3>Profile</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/dashboard">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->
<div class="container p-5 mt-5 position-relative">


    <h1>{{ $data[1]->name }}</h1>
    <h4>{{ '@'.$data[0]->username }}</h4>
    <div class="edit-profile-div clear">
        <div class="game-buy">
            <a class="fag-btn-outline" href="/dashboard/editprofile">Edit Profile</a>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-8">
            <h2>Games Created : ({{ $data[2]->count() }})</h2>
            @foreach ($data[2] as $key => $game)
            <div class="summury-inn clear profile-game">
                <img class="left-img" src="/storage/{{$game->thumbnail_filename}}" alt="Game">
                <div class="left-div">
                    <h2><a href="/developers/{{ $game->developer->developer->slug_url }}/{{ $game->slug_url }}" class="text-glowing">{{ $game->name }}</a></h2>
                    <h6>@idr($game->price)</h6>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
