@extends('layout.developer')
@section('content')

<div class="page-404 section--full-bg">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="page-404__wrap">
                <div class="login-wrapper">
                   <h3>Create Account</h3>
                   <form action="/developers/register" method="POST" enctype="multipart/form-data">
                       @csrf
                        <div style="color: white">
                            <label for="picture">Upload Profile Picture</label> <br>
                            <img style="object-fit: cover;width: 150px;height: 150px;border-radius:50%"  src="/iconprofile.png" alt="asd" class="edit-profile-img" id="img">
                            <input type="file" name="image" id="" onchange="readURL(this);" required>
                        </div>
                       <div class="form-row">
                        <input type="text" name="name" placeholder="name"
                            value="{{ old('name') }}" required/>
                        </div>
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                       <div class="form-row">
                           <input type="text" name="username" placeholder="Username"
                               value="{{ old('username') }}" required/>
                       </div>
                       @error('username')
                       <span class="text-danger">{{ $message }}</span>
                       @enderror
                       <div class="form-row">
                           <input type="email" name="email" placeholder="Email Address"
                               value="{{ old('email') }}" required/>
                       </div>
                       @error('email')
                       <span class="text-danger">{{ $message }}</span>
                       @enderror
                       <div class="form-row">
                           <input type="password" name="password" placeholder="Password"
                               value="{{ old('password') }}" required/>
                       </div>
                       @error('password')
                       <span class="text-danger">{{ $message }}</span>
                       @enderror
                       <div class="form-row">
                           <input type="password" name="password_confirmation" placeholder="Confirm Password"
                               value="{{ old('password_confirmation') }}" required/>
                       </div>
                       @error('password_confirmation')
                       <span class="text-danger">{{ $message }}</span>
                       @enderror
                       <div class="form-row">
                           <h3>Description</h3>
                         <textarea rows="4", cols="54" id="description" name="description" style="resize:none, " required></textarea>
                        </div>
                       <div class="col-12 form-row">
                           <button class="fag-btn" type="submit">Create your Account!</button>
                       </div>
                       <span>Already have an account? <a href="/logindeveloper">Sign in</a></span>
                   </form>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
@endsection

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img').attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
