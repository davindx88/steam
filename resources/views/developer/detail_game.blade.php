@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area"
    style="background:url('/storage/{{ $game->background_filename}}');background-repeat: no-repeat;background-size:100% 100%;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="games-details-banner">
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <div class="details-banner-thumb">
                                <img src="/storage/{{ $game->thumbnail_filename}}" alt="games">
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-8">
                            <div class="details-banner-info float-left p-3" style="background: #000a">
                                <h3>{{ $game->name }} <span class="single_rating"><i
                                            class="fa fa-star"></i>{{ $game->rating() }}</span>
                                </h3>
                                <div class="single_game_meta">
                                    <p class="details-genre">{{ $game->genres->implode('name', ' | ') }}</p>
                                    <p class="details-time-left">
                                        <i class="fa fa-calendar"></i>Release date:
                                        <span>{{ $game->released_at->isoFormat('DD.MM.YYYY') }}</span>
                                        <br>
                                        <i class="fa fa-eye mr-2"></i>{{ $game->views }} views
                                        <br>
                                        <i class="fa fa-download mr-2"></i>{{ $game->gamers->count() }} installs
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="game-price single_game_price">
                                @if ($game->price == 0)
                                <h4 class="text-success">Free</h4>
                                @elseif($game->price == $game->discprice())
                                <h4>@idr( $game->price )</h4>
                                @else
                                <h4><del>@idr( $game->price )</del></h4>
                                @if ($game->discprice() == 0)
                                <h4 class="text-success">Free</h4>
                                @else
                                <h4>@idr( $game->discprice() ) </h4>
                                @endif
                                @endif
                            </div>
                            <div class="details-banner-action">
                                {{-- Check apakah user punya gamenya --}}
                                @if ($user !== null && $user->games()->where('id', $game->id)->first() !== null)
                                <div class="game-buy">
                                    <a class="fag-btn-outline bg-success text-light">Owned</a>
                                </div>
                                @else
                                <form action="/cart/add" method="post">
                                    @csrf
                                    <input type="hidden" name="game_slug" value="{{ $game->slug_url }}">
                                    <button class="fag-btn">Buy Now</button>
                                </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- games Details Page Start -->
<section class="fag-games-details-page section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 offset-lg-3">
                <div class="games-details-page-box">
                    <h2>Developer :
                        <a href="/developers/{{ $game->developer->developer->slug_url }}">
                            {{ $game->developer->developer->name }}
                        </a>
                    </h2>
                    @if ($hasGame)
                    <form action="/downloadgame" class="mt-3" method="POST">
                        @csrf
                        <h3 class="text-center mb-2">Download your game now!</h3>
                        <input type="hidden" name="game_slug" value="{{ $game->slug_url }}">
                        <button class="fag-btn">Install</button>
                    </form>
                    @endif
                    <div class="tv-panel-list">
                        <div class="tv-tab">
                            <ul class="nav nav-pills tv-tab-switch" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="pills-brief-tab" data-toggle="pill"
                                        href="#pills-brief" role="tab" aria-controls="pills-brief"
                                        aria-selected="true">Game Brief</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-cast-tab" data-toggle="pill" href="#pills-cast"
                                        role="tab" aria-controls="pills-cast" aria-selected="false">Features</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-platform-tab" data-toggle="pill"
                                        href="#pills-platform" role="tab" aria-controls="pills-platform"
                                        aria-selected="false">Platforms
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews"
                                        role="tab" aria-controls="pills-reviews" aria-selected="false">reviews
                                        ({{ $game->reviews->count() }})
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade active show" id="pills-brief" role="tabpanel"
                                aria-labelledby="pills-brief-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <p>{{ $game->description }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-cast" role="tabpanel" aria-labelledby="pills-cast-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="row">
                                                    @forelse ($game->features as $feature)
                                                    <div class="col-lg-6 col-sm-6">
                                                        <div class="features-game">
                                                            <div class="feature-image">
                                                                <img src="{{ $feature->image_filename == "" ? url('faf/assets/img/feature-1.png') : url('/storage/photos/'.$feature->image_filename) }}"
                                                                    alt="feature" class='bg-warning rounded-circle' />
                                                            </div>
                                                            <div class="feature-text">
                                                                <h3>{{ $feature->name }}</h3>
                                                                <p>{{ $feature->description }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @empty
                                                    <div class="col-12">
                                                        <div class="features-game">
                                                            <div class="feature-text">
                                                                <h3>No features</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-platform" role="tabpanel" aria-labelledby="pills-platform-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="row">
                                                    @forelse ($game->platforms as $ket => $platform)
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="features-game">
                                                            <div class="feature-image">
                                                                <img src="/storage/photos/{{ $platform->filename }}"
                                                                    alt="feature" class='bg-warning rounded-circle' />
                                                            </div>
                                                            <div class="feature-text">
                                                                <h3>{{ $platform->name }}</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @empty
                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-reviews" role="tabpanel"
                                aria-labelledby="pills-reviews-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="fag-comment-list">
                                                    <div class="single-comment-item">
                                                        @forelse ($game->reviews as $key => $reviewer)
                                                        <div class="single-comment-box">
                                                            <div class="main-comment">
                                                                <div class="author-image">
                                                                    <img src="/storage/{{ $reviewer->filename }}"
                                                                        alt="author">
                                                                </div>
                                                                <div class="comment-text">
                                                                    <div class="comment-info">
                                                                        <h4>{{ $reviewer->first_name }}
                                                                            {{ $reviewer->last_name }}</h4>
                                                                        <ul>
                                                                            @for ($i = 0; $i < 5; $i++) <li><i
                                                                                    class="fa fa-star{{ $i+1 <= $reviewer->pivot->rating ? '' : '-o'}}"></i>
                                                                                </li>
                                                                                @endfor
                                                                        </ul>
                                                                        <p>{{ $reviewer->pivot->created_at->diffForHumans() }}
                                                                        </p>
                                                                    </div>
                                                                    <div class="comment-text-inner">
                                                                        <p>{{ $reviewer->pivot->message }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @empty
                                                        <h3 class="text-center">There's no review yet</h3>
                                                        @endforelse
                                                    </div>
                                                </div>
                                                <!-- /end comment list -->
                                                @if ($hasGame && !$isGameReviewed)
                                                <div class="fag-leave-comment">
                                                    <h1>Review Game</h1>
                                                    <form action="/reviewgames" method="POST">
                                                        @csrf
                                                        <div class="row">
                                                            <input type="hidden" name="game_slug"
                                                                value="{{ $game->slug_url }}">
                                                            <input type="hidden" name="rating" id="rating" value="0">
                                                            <div class="col-lg-12">
                                                                <h3 class="text-center mt-5">
                                                                    <strong>Rating</strong>
                                                                    <span id="display-rating">0</span>
                                                                    <i class="fa fa-star active-star"></i>
                                                                </h3>
                                                                <div id="star-box" class="text-center">
                                                                    <i index=0
                                                                        class="star fa fa-star fa-5x mr-3 inactive-star"></i>
                                                                    <i index=1
                                                                        class="star fa fa-star fa-5x mr-3 inactive-star"></i>
                                                                    <i index=2
                                                                        class="star fa fa-star fa-5x mr-3 inactive-star"></i>
                                                                    <i index=3
                                                                        class="star fa fa-star fa-5x mr-3 inactive-star"></i>
                                                                    <i index=4
                                                                        class="star fa fa-star fa-5x inactive-star"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="comment-field">
                                                                    <textarea class="comment" placeholder="Review..."
                                                                        name="message"
                                                                        value="{{ old('message') }}"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="comment-field">
                                                                    <button type="submit" class="fag-btn">Submit Review
                                                                        <span></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                @endif
                                                <!-- /end comment form -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- games Details Page End -->


<!-- Related Games Start -->
<section class="fag-games-area related_games section_100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="site-heading">
                    <h2 class="heading_animation">Related <span>games</span></h2>
                    <p>More games by {{ $game->developer->developer->name }}</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($game->developer->developedGames()
            ->where('status',1)
            ->where('id', '<>', $game->id)
                ->get() as $key => $othergame)
                <div class="col-lg-3 col-sm-6">
                    <div class="games-single-item">
                        <div class="games-thumb">
                            <div class="games-thumb-image">
                                <a
                                    href="/developers/{{ $othergame->developer->developer->slug_url }}/{{ $othergame->slug_url }}">
                                    <img src="/storage/{{ $othergame->thumbnail_filename}}" alt="product" />
                                </a>
                            </div>
                        </div>
                        <div class="games-desc">
                            <h3 class="text-left"><a
                                    href="/developers/{{ $othergame->developer->developer->slug_url }}/{{ $othergame->slug_url }}">{{ $othergame->name }}</a>
                            </h3>
                            <h6><a
                                    href="/developers/{{ $othergame->developer->developer->slug_url }}">{{ $othergame->developer->developer->slug_url }}</a>
                            </h6>
                            @if ($othergame->rating() != 0)
                            <div class="game-rating">
                                <h4>{{ $othergame->rating() }}</h4>
                                <ul>
                                    @for ($i = 0; $i < 5; $i++) @if ($i+1 <=$othergame->rating())
                                        <li><span class="fa fa-star"></span></li>
                                        @else
                                        <li><span class="fa fa-star-o"></span></li>
                                        @endif
                                        @endfor
                                </ul>
                            </div>
                            @endif
                            <div class="game-action">
                                <div class="game-price">
                                    @if ($othergame->price == 0)
                                    <h4 class="text-success">Free</h4>
                                    @elseif($othergame->price == $othergame->discprice())
                                    <h4>@idr( $othergame->price )</h4>
                                    @else
                                    <h4><del>@idr( $othergame->price )</del></h4>
                                    @if ($othergame->discprice() == 0)
                                    <h4 class="text-success">Free</h4>
                                    @else
                                    <h4>@idr( $othergame->discprice() ) </h4>
                                    @endif
                                    @endif
                                </div>
                                <div class="game-buy">
                                    @if ($user !== null && $user->games()->where('id', $othergame->id)->first() !==
                                    null)
                                    <h5 class="text-success">Owned</h5>
                                    @else
                                    <form action="/cart/add" method="post">
                                        @csrf
                                        <input type="hidden" name="game_slug" value="{{ $othergame->slug_url }}">
                                        <button class="fag-btn">Buy Now!</button>
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</section>
<!-- Related Games End -->
@endsection

@section('script')
<script>
    $(()=>{
        // Update Input Hidden
        let rating = {{ old('rating', '0') }};
        function updateInputHidden(){
            $('#rating').attr('value', rating)
            $('#display-rating').text(rating)
            $('.star').each(function(idx, value){
                $(this)
                .removeClass('active-star')
                .removeClass('inactive-star')
                if(idx+1 <= rating){
                    $(this).addClass('active-star')
                }else{
                    $(this).addClass('inactive-star')
                }
            })
        }
        // OnHoverBintang ke Highlight
        $('.star').mouseover(function(){
            const index = $(this).attr('index')
            $('.star').each(function(idx, value){
                $(this)
                .removeClass('active-star')
                .removeClass('inactive-star')
                if(idx <= index){
                    $(this).addClass('active-star')
                }else{
                    $(this).addClass('inactive-star')
                }
            })
        })
        .click(function(){
            const index = $(this).attr('index')
            rating = parseInt(index) + 1
            updateInputHidden()
        })
        $('#star-box').mouseleave(updateInputHidden)

        // Update UI
        updateInputHidden()
    })
</script>
@endsection
