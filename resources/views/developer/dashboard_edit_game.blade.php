@extends('layout.developer')
@section('content')

<div class="page-404 section--full-bg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-404__wrap">
                    <div class="checkout-left-box">
                        <div class="login-wrapper">
                            <h3>Edit Game Here</h3>
                            <form method="POST" action="/dashboard/editgame" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <input type="text" placeholder="Game Name" name="name" required value="{{$data[2]->name}}"/>
                                    </div>
                                    <div class="form-row">
                                        <input type="text" placeholder="Description" name="description" required value="{{$data[2]->description}}"/>
                                    </div>
                                    <div class="form-row">
                                        <input type="number" placeholder="Price" name="price" required value="{{$data[2]->price}}"/>
                                    </div>
                                    <br>
                                    <input type="hidden" name="idgame" value="{{$data[2]->id}}">
                                    <div class="form-row"></div>
                                    <div class="form-row">
                                        <button class="fag-btn" type="submit">Save</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('.category').select2();
    });
</script>
<pre lang="javascript">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">

   $(function() {

        //set a counter
        var i =  1;

        //add input
        $('a#add').click(function() {
            $('<div class="form-row"> <input type="text" placeholder="Feature Name " name="featuresname[]" id="' + i + '" /><br> <input type="text" placeholder="Descriptions" name="featuredescription[]" id="' + i + '" /><br>  <br> <input type="file" name="image[]" id="' + i + '" value="' + i + '" /> </div>').fadeIn("slow").appendTo('#extender');
            i++;
            return false;
        });
    })

 </script>
@endsection
