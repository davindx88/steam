
@extends('layout.developer')
@section('content')

<section class="fag-breadcrumb-area">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="breadcromb-box">
                <h3>Welcome </h3>
                <ul>
                   <li><i class="fa fa-home"></i></li>
                   <li><a href="/dashboard">Home</a></li>
                </ul>
             </div>
          </div>
       </div>
    </div>
 </section>
      <!-- Game Page Start -->
      <section class="fag-game-page section_100">
         <div class="container">
            <div class="row">
               <div class="col-lg-9">
                  <div class="games-category">
                     <div class="row">
                        <div class="container">
                            <h1>Users Graphs</h1>
                            <div class="row">
                                {!! $usersChart->container() !!}
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Game Page End -->

      @endsection
      @section('js')
      <script src="{{ $usersChart->cdn() }}"></script>

      {{ $usersChart->script() }}
      @endsection
