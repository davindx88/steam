@extends('layout.developer')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area" style="background: url('/storage/{{$data[0]->background_filename}}');background-repeat: no-repeat;background-size: 100% 100%">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="games-details-banner">
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <div class="details-banner-thumb">
                                <img src={{url('/storage/'.$data[0]->thumbnail_filename)}} alt="games" />

                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-8">
                            <div class="details-banner-info">
                                <h3>{{ $data[0]->name }} <span class="single_rating"><i class="fa fa-star"></i>4.5</span>
                                </h3>
                                <div class="single_game_meta">
                                    <p class="details-genre">{{ $data[0]->genres->implode('name', ' | ') }}</p>
                                    <p class="details-time-left"><i class="fa fa-calendar"></i>Release date: <span>
                                            {{$data[0]->released_at}}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="game-price single_game_price">
                                @if ($data[0]->price > 0)
                                <h4>@idr($data[0]->price)</h4>
                                @else
                                <h4 class="text-success">Free</h4>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- games Details Page Start -->
<section class="fag-games-details-page section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 offset-lg-3">
                <div class="games-details-page-box">
                    <h2>Developer :
                        <a href="/dashboard/games/">
                            {{ $data[1]->name }}
                        </a>
                    </h2>
                    <h2>
                        <a href="/dashboard/games/edit/{{$data[0]->id}}">
                            <button class="btn btn-warning">Edit</button>
                        </a>
                    </h2>
                    <div class="tv-panel-list">
                        <div class="tv-tab">
                            <ul class="nav nav-pills tv-tab-switch" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="pills-brief-tab" data-toggle="pill"
                                        href="#pills-brief" role="tab" aria-controls="pills-brief"
                                        aria-selected="true">Game Brief</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-cast-tab" data-toggle="pill" href="#pills-cast"
                                        role="tab" aria-controls="pills-cast" aria-selected="false">Features</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews"
                                        role="tab" aria-controls="pills-reviews" aria-selected="false">reviews ({{ $data[0]->reviews->count() }})</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade active show" id="pills-brief" role="tabpanel"
                                aria-labelledby="pills-brief-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <p>{{ $data[0]->description }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-cast" role="tabpanel" aria-labelledby="pills-cast-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="row">
                                                    @foreach ($data[2] as $item)
                                                    <div class="col-lg-6 col-sm-6">
                                                        <div class="features-game">
                                                            <div class="feature-image">
                                                                <img src="/storage/photos/{{$item->image_filename}}"
                                                                    alt="feature" />
                                                            </div>
                                                            <div class="feature-text">
                                                                <h3>{{$item->name}}</h3>
                                                                <p>{{$item->description}}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-reviews" role="tabpanel"
                                aria-labelledby="pills-reviews-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="fag-comment-list">
                                                    <div class="single-comment-item">
                                                        @forelse ($data[0]->reviews as $key => $reviewer)
                                                        <div class="single-comment-box">
                                                            <div class="main-comment">
                                                                <div class="author-image">
                                                                    <img src="{{ url('faf/assets/img/4.jpg') }}"
                                                                        alt="author">
                                                                </div>

                                                               <div class="col-lg-6 col-sm-8">
                                                                   <div class="details-banner-info">
                                                                       <h3>{{ $game->name }} <span class="single_rating"><i
                                                                                   class="fa fa-star"></i>{{ number_format($game->reviews->avg('pivot.rating'), 1, '.', '') }}</span>
                                                                       </h3>
                                                                       <div class="single_game_meta">
                                                                           <p class="details-genre">{{ $game->genres->implode('name', ' | ') }}</p>
                                                                           <p class="details-time-left"><i class="fa fa-calendar"></i>Release date:
                                                                               <span>{{ $game->released_at??"Not released yet"}}</span></p>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                                <div class="comment-text">
                                                                    <div class="comment-info">
                                                                        <h4>{{ $reviewer->first_name }} {{ $reviewer->last_name }}</h4>
                                                                        <ul>
                                                                            @for ($i = 0; $i < 5; $i++)
                                                                            <li><i class="fa fa-star{{ $i+1 <= $reviewer->pivot->rating ? '' : '-o'}}"></i></li>
                                                                            @endfor
                                                                        </ul>
                                                                        <p>{{ $reviewer->pivot->created_at->diffForHumans() }}</p>
                                                                    </div>
                                                                    <div class="comment-text-inner">
                                                                        <p>{{ $reviewer->pivot->message }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @empty
                                                        <h3 class="text-center">There's no review yet</h3>
                                                        @endforelse
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- games Details Page End -->


<!-- Related Games Start -->
<section class="fag-games-area related_games section_100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="site-heading">
                    <h2 class="heading_animation">Related <span>games</span></h2>
                    <p>More games by {{ $data[1]->name }}</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($data[3] as $key => $othergame)
                <div class="col-lg-3 col-sm-6">
                    <div class="games-single-item img-contain-isotope">
                        <div class="games-thumb">
                            <div class="games-thumb-image">
                                <a href="#">
                                    <img src="/storage/{{$othergame->thumbnail_filename}}"/>

                                </a>
                            </div>
                            <div class="game-overlay">
                                <a class="popup-youtube" href="https://www.youtube.com/watch?v=3SAuuHCOkyI">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                        <div class="games-desc">
                            <h3><a
                                    href="/developers/{{ $othergame->slug_url }}/{{ $othergame->slug_url }}">{{ $othergame->name }}</a>
                            </h3>
                            <p class="game-meta">{{ $othergame->genres->implode('name', ' | ') }}</p>
                            <p class="game-meta">Release date:<span> 07.12.2015</span></p>
                            <div class="game-action">
                                <div class="game-price">
                                    @if ($othergame->price > 0)
                                    <h4>IDR {{ $othergame->price }}</h4>
                                    @else
                                    <h4 class="text-success">Free</h4>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</section>
<!-- Related Games End -->
@endsection

@section('script')
<script>
    $(()=>{
        // Update Input Hidden
        let rating = {{ old('rating', '0') }};
        function updateInputHidden(){
            $('#rating').attr('value', rating)
            $('#display-rating').text(rating)
            $('.star').each(function(idx, value){
                $(this)
                .removeClass('active-star')
                .removeClass('inactive-star')
                if(idx+1 <= rating){
                    $(this).addClass('active-star')
                }else{
                    $(this).addClass('inactive-star')
                }
            })
        }
        // OnHoverBintang ke Highlight
        $('.star').mouseover(function(){
            const index = $(this).attr('index')
            $('.star').each(function(idx, value){
                $(this)
                .removeClass('active-star')
                .removeClass('inactive-star')
                if(idx <= index){
                    $(this).addClass('active-star')
                }else{
                    $(this).addClass('inactive-star')
                }
            })
        })
        .click(function(){
            const index = $(this).attr('index')
            rating = parseInt(index) + 1
            updateInputHidden()
        })
        $('#star-box').mouseleave(updateInputHidden)

        // Update UI
        updateInputHidden()
    })
</script>
@endsection
