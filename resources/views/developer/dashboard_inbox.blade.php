
@extends('layout.developer')
@section('content')

<section class="fag-breadcrumb-area">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <div class="breadcromb-box">
                <h3>Welcome </h3>
                <ul>
                   <li><i class="fa fa-home"></i></li>
                   <li><a href="/dashboard">Home</a></li>
                   <li><i class="fa fa-angle-right"></i></li>
                   <li><a href="/dashboard">Inbox</a></li>
                </ul>
             </div>
          </div>
       </div>
    </div>
 </section>
      <!-- Game Page Start -->
      <section class="fag-game-page section_100">
         <div class="container">
            <div class="row">
               <div class="col-lg-9">
                  <div class="games-category">
                     <div class="row">
                        <div class="container">
                            <h1>Message</h1>
                            @foreach ($sales as $item)
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="summury-inn clear profile-game">
                                        <img class="left-img" src={{url('/storage/photos/'.$item->filename)}} alt="games"/>
                                        <div class="left-div">
                                            <h2>{{$item->title}}</h2>
                                            <h3>Join Our Sales now</h3>
                                            <button class="fag-btn"><a href="/dashboard/sales/{{$item->id}}/join">Join Our Sale Now</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Game Page End -->

      @endsection
      @section('js')

      @endsection
