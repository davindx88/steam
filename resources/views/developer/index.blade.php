@extends('layout.user')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box">
                    <h3>Developers</h3>
                    <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="/">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Developers</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->
<div class="container p-5 position-relative">
    <div class="contact-form-inn">
        <h3>Developers</h3>
        <form action="#" method="get">
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field">
                        <input class="ns-com-name" name="u" placeholder="name" type="text"
                            value="{{ old('u') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="comment-field form-action">
                        <button class="fag-btn">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        @forelse ($list_developer as $key => $developer)
        <div class="col-md-6 col-lg-4">
            <div class="summury-inn clear profile-game text-center">
                <img style="object-fit: cover;width: 100px;height: 100px;" src="/storage/{{$developer->account->filename}}" alt="asd" class="rounded">
                <div class="">
                    <h2><a href="/developers/{{ $developer->slug_url }}">{{ $developer->name }}</a></h2>
                    <h6>{{ $developer->account->first_name }} {{ $developer->account->last_name }}</h6>
                    <a href="/developers/{{ $developer->slug_url }}" class="fag-btn-outline mt-4">Explore</a>
                </div>
            </div>
        </div>
        @empty

        @endforelse
    </div>
</div>
@endsection
