@extends('layout.developer')
@section('content')
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="games-details-banner">
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <div class="details-banner-thumb">
                                <img src="{{$data[6]}}" alt="games" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-8">
                            <div class="details-banner-info">
                                <h3>{{ $data[0]->name }} <span class="single_rating"><i class="fa fa-star"></i>4.5</span>
                                </h3>
                                <div class="single_game_meta">
                                    <p class="details-genre">{{ $data[3]}}</p>
                                    <p class="details-time-left"><i class="fa fa-calendar"></i>Release date: <span>
                                            {{$data[0]->released_at}}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="game-price single_game_price">
                                @if ($data[1]> 0)
                                <h4>@idr($data[1])</h4>
                                @else
                                <h4 class="text-success">Free</h4>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->


<!-- games Details Page Start -->
<section class="fag-games-details-page section_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 offset-lg-3">
                <div class="games-details-page-box">
                    {{-- <h2>Developer :
                        <a href="/developers/{{ $data[0]->developer->developer->slug_url }}">
                            {{ $data[1]->name }}
                        </a>
                    </h2> --}}
                    <div class="tv-panel-list">
                        <div class="tv-tab">
                            <ul class="nav nav-pills tv-tab-switch" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="pills-brief-tab" data-toggle="pill"
                                        href="#pills-brief" role="tab" aria-controls="pills-brief"
                                        aria-selected="true">Game Brief</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-cast-tab" data-toggle="pill" href="#pills-cast"
                                        role="tab" aria-controls="pills-cast" aria-selected="false">Features</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews"
                                        role="tab" aria-controls="pills-reviews" aria-selected="false">reviews (0)</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade active show" id="pills-brief" role="tabpanel"
                                aria-labelledby="pills-brief-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <p>{{ $data[7] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-cast" role="tabpanel" aria-labelledby="pills-cast-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="row">
                                                    <div class="col-lg-6 col-sm-6">
                                                        <div class="features-game">
                                                            <div class="feature-image">
                                                                <img src="{{$data[6]}}"
                                                                    alt="feature" />
                                                            </div>
                                                            <div class="feature-text">
                                                                <h3>{{$data[2]}}</h3>
                                                                <p>{{$data[8]}}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-reviews" role="tabpanel"
                                aria-labelledby="pills-reviews-tab">
                                <div class="tab-gamess-details">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-body">
                                                <div class="fag-comment-list">
                                                    <div class="single-comment-item">
                                                        @forelse ($data[0]->reviews as $key => $reviewer)
                                                        <div class="single-comment-box">
                                                            <div class="main-comment">
                                                                <div class="author-image">
                                                                    <img src="{{ url('faf/assets/img/4.jpg') }}"
                                                                        alt="author">
                                                                </div>
                                                                <div class="comment-text">
                                                                    <div class="comment-info">
                                                                        <h4>{{ $reviewer->first_name }} {{ $reviewer->last_name }}</h4>
                                                                        <ul>
                                                                            @for ($i = 0; $i < 5; $i++)
                                                                            <li><i class="fa fa-star{{ $i+1 <= $reviewer->pivot->rating ? '' : '-o'}}"></i></li>
                                                                            @endfor
                                                                        </ul>
                                                                        <p>{{ $reviewer->pivot->created_at->diffForHumans() }}</p>
                                                                    </div>
                                                                    <div class="comment-text-inner">
                                                                        <p>{{ $reviewer->pivot->message }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @empty
                                                        <h3 class="text-center">There's no review yet</h3>
                                                        @endforelse
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- games Details Page End -->

@endsection

