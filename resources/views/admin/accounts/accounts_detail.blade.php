@extends('layout.admin')
@section('title','Admin | Accounts')
@section('style')
<style>
    body {
        color: #1a202c;
        text-align: left;
        background-color: #e2e8f0;
    }

    .main-body {
        padding: 5px;
    }

    .card {
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
    }

    .card-body {
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1rem;
    }

    .gutters-sm {
        margin-right: -8px;
        margin-left: -8px;
    }

    .gutters-sm>.col,
    .gutters-sm>[class*=col-] {
        padding-right: 8px;
        padding-left: 8px;
    }

    .mb-3,
    .my-3 {
        margin-bottom: 1rem !important;
    }

    .bg-gray-300 {
        background-color: #e2e8f0;
    }

    .h-100 {
        height: 100% !important;
    }

    .shadow-none {
        box-shadow: none !important;
    }
</style>
@endsection
@section('script')
<script>
    function show_all_games(){
        Swal.fire({
            title: "All Games Owned by {{ $account->first_name }}",
            html: `
            @foreach ($account->games as $game)
            <div class="row py-2 container">
                @if ($game->thumbnail_filename)
                <img src='{{ url('/storage/'.$game->thumbnail_filename) }}' height=70 width=50 />
                @else
                <img src='{{ url('/icongame.png') }}'
                    height=80 width=60 />
                @endif
                <div class="pt-2 pl-4">
                    <h5 class='text-left'><a href="{{ url('/admin/games/'.$game->slug_url) }}">{{ $game->name }}</a>
                    </h5>
                    <span>{{  'owned '. Carbon\Carbon::parse($game->pivot->owned_at)->diffForHumans() }}
                    </span>
                </div>
            </div>
            @endforeach
            `,
            showLoaderOnConfirm: true,
        }).then(function (e) {
            if (e.value === true) {
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        })
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="main-body">
        <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="{{ $account->filename == "" ? url('/iconprofile.png') : url('/storage/'.$account->filename) }}"
                                alt="Admin" class="rounded-circle" width="150">
                            <div class="mt-3">
                                <h4>{{ $account->username }}</h4>
                                {{-- <button class="btn btn-danger">Block Account</button> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><i class="fa fa-key ml-1 mr-2"></i> ID</h6>
                            <span class="text-secondary">{{ $account->id }}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><i class="fa fa-calendar ml-1 mr-2"></i> Account created</h6>
                            <span
                                class="text-secondary">{{ Carbon\Carbon::parse($account->created_at)->diffForHumans() }}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><i class="fa fa-edit ml-1 mr-2"></i> Last update</h6>
                            <span
                                class="text-secondary">{{ Carbon\Carbon::parse($account->updated_at)->diffForHumans() }}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><i class="fa fa-check ml-1 mr-2"></i> Verified at</h6>
                            <span class="text-secondary">
                                @if ($account->verified_at)
                                {{ Carbon\Carbon::parse($account->verified_at)->diffForHumans() }}
                                @else
                                Not verified yet
                                @endif

                            </span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0"><i class="fa fa-trash ml-1 mr-2"></i> Deleted at</h6>
                            <span class="text-secondary">
                                @if ($account->deleted_at)
                                {{ Carbon\Carbon::parse($account->deleted_at)->diffForHumans() }}
                                @else
                                -
                                @endif
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Full Name</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{ $account->first_name . " " . $account->last_name }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{ $account->email }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Balance</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{ $account->balance }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Type</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <h6 class="mb-0">{{ $account->type == 0 ? 'User' : 'Admin' }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row gutters-sm">
                    <div class="col-sm-12 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                                <h6 class="d-flex align-items-center mb-3"><i
                                        class="material-icons text-info mr-2">report</i>Game Owned</h6>
                                @forelse ($account->games->take(5) as $game)
                                <div class="row py-2 container">
                                    @if ($game->thumbnail_filename)
                                    <img src='{{ url('/storage/'.$game->thumbnail_filename) }}' height=70 width=50 />
                                    @else
                                    <img src='{{ $game->thumbfilename == "" ? url('/icongame.png') : url('/storage/'.$game->thumbfilename) }}'
                                        height=70 width=50 />
                                    @endif
                                    <div class="pt-2 pl-4">
                                        <h5><a href="{{ url('/admin/games/'.$game->slug_url) }}">{{ $game->name }}</a>
                                        </h5>
                                        <span>{{  'owned '. Carbon\Carbon::parse($game->pivot->owned_at)->diffForHumans() }}
                                        </span>
                                    </div>
                                </div>
                                @empty
                                <div class="row py-2 container">
                                    <h5>This guy doesn't play game</h5>
                                </div>
                                @endforelse
                            </div>
                            @if ($account->games->count() > 5)
                            <div class="card-body text-right"><button onclick=show_all_games()
                                    class="btn btn-flat btn-info">View all</button>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
