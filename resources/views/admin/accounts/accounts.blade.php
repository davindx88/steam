@extends('layout.admin')
@section('title','Admin | Accounts')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')

<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_accounts =  $('#table-accounts').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/accounts/get_accounts') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'name', name: 'name'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Accounts</h5>
    </div>
</div>
<div class="row py-4">
    <div class="col">
        <table id="table-accounts" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
