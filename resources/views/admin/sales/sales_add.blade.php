@extends('layout.admin')
@section('title','Admin | Sales')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }

    html,
    body {
        height: 100%;
    }

    img {
        max-width: 100%;
        height: auto
    }

    @media (min-width: 1200px) {
        .container {
            max-width: 1200px;
        }
    }

    .fag-breadcrumb-area {
        padding: 170px 0 100px;
        background: url(../img/breadcrumb_bg.jpg) no-repeat scroll center center/cover
    }

    .section_100 {
        padding: 100px 0;
    }

    .details-banner-info h3 {
        margin-bottom: 5px;
        color: #fff;
        font-size: 34px;
        text-transform: capitalize;
        font-weight: 600;
        display: inline-block;
        position: relative;
    }


    .details-banner-info h3:after {
        position: absolute;
        content: "";
        left: 0;
        width: 100%;
        height: 3px;
        border-radius: 2px;
        bottom: -8px;
        background: #e48632 none repeat scroll 0 0;
    }

    .details-genre {
        color: #eee;
        text-transform: capitalize;
        font-weight: 500;
        letter-spacing: 1px;
        margin-bottom: 5px;
        margin-top: 5px;
    }

    .details-time-left {
        color: #eee;
        letter-spacing: .5px;
    }

    .details-time-left i {
        margin-right: 5px;
        color: #ec7532;
    }


    .single_game_meta {
        margin-top: 15px
    }

    .games-details-page-box>ul>li {
        margin-bottom: 5px;
        position: relative;
        padding-left: 15px;
    }

    .games-details-page-box>ul>li:after {
        position: absolute;
        content: "";
        top: 50%;
        left: 0;
        width: 5px;
        height: 5px;
        background: #ff7a21;
        border-radius: 50%;
        -webkit-transform: translate(0, -50%);
        transform: translate(0, -50%);
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $('input[name="date"]').daterangepicker({
            autoUpdateInput: false,
            minDate: new Date(),
            timePicker: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });
        $('input[name="date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD hh:mm:ss A') + ' until ' + picker.endDate.format('YYYY-MM-DD hh:mm:ss A'));
            onupdate_field();
        });
        $('input[name="date"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });

    function onupdate_field(){
        $('#title_preview').html($('#title').val() == "" ? "Title" : $('#title').val());
        $('#date_preview').html($('#date').val());
        $('#description_preview').html($('#description').val());
    }
</script>
@endsection
@section('content')
<section id="filename_preview" class="fag-breadcrumb-area bg-dark" style="
    background-image: url('{{ url('/icongame.png') }}');background-repeat: no-repeat;background-size: 100% 100%; ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="games-details-banner">
                    <div class="row">
                        <div class="col-lg-6 col-sm-8">
                            <div class="details-banner-info p-3" style="background-color: rgba(0,0,0,0.5); ">
                                <h3 id="title_preview">Title
                                </h3>
                                <div class="single_game_meta">
                                    <p class="details-genre"></p>
                                    <p class="details-time-left"><i class="fa fa-calendar"></i>Date:
                                        <span id="date_preview"></span></p>
                                </div>
                                <div>
                                    <p id="description_preview"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container mt-4">
    <div class="row">
        <div class="col">
            <h3>Create new Sale</h3>
        </div>
    </div>
    <div class="row my-4">
        <div class="col">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ url('admin/sales/store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" class="form-control" id="title" onkeyup="onupdate_field()"
                            placeholder="title" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" class="form-control" id="description" onkeyup="onupdate_field()"
                            rows="5" required></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10">
                        <input type="text" id="date" name="date" value="" class="bg-dark form-control"
                            placeholder="Start date until end date" autocomplete="off" onchange="onupdate_field()"
                            required />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="filename" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control-file" name="filename" id="filename"
                            onchange="$('#filename_preview').attr('style','background-image: url('+window.URL.createObjectURL(event.target.files[0]))+')'"
                            required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-block">Create</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
</div>
<div class="row my-4"></div>
@endsection
