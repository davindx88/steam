@extends('layout.admin')
@section('title','Admin | Sales')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_sales =  $('#table-sales').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/sales/get_sales') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'description', name: 'description', width: '20%'},
                {data: 'started_at', name: 'started_at'},
                {data: 'ended_at', name: 'ended_at'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
    function delete_confirmation(id){
        Swal.fire({
            title: "Delete?",
            text: "Please ensure and then confirm!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value === true) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: `/admin/sales/${id}/destroy`,
                    dataType: 'JSON',
                    success: function (results) {
                        if (results.success === true) {
                            swal.fire("Done!", results.message, "success");
                            $('#table-sales').DataTable().ajax.reload();
                        } else {
                            swal.fire("Error!", results.message, "error");
                        }
                    }
                });
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        });
    }
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Sales</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <div class="form-group">
            <a class="btn btn-primary btn-sm form-control-sm w-25" href="{{ url('admin/sales/add') }}"><i
                    class="fa fa-plus"></i>&nbsp&nbsp Create new Sales</a>
        </div>
        <table id="table-sales" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">description</th>
                    <th scope="col">Start</th>
                    <th scope="col">End</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="row my-5"></div>
@endsection
