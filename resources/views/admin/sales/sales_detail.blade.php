@extends('layout.admin')
@section('title','Admin | Sales')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }

    #table-games thead {
        display: none !important;
    }

    @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i');
    @import url('https://fonts.googleapis.com/css?family=Rajdhani:400,500,600,700&display=swap');

    html,
    body {
        height: 100%;
    }

    img {
        max-width: 100%;
        height: auto
    }

    @media (min-width: 1200px) {
        .container {
            max-width: 1200px;
        }
    }

    .fag-breadcrumb-area {
        padding: 170px 0 100px;
        background: url(../img/breadcrumb_bg.jpg) no-repeat scroll center center/cover
    }

    .section_100 {
        padding: 100px 0;
    }

    .details-banner-info h3 {
        margin-bottom: 5px;
        color: #fff;
        font-size: 34px;
        text-transform: capitalize;
        font-weight: 600;
        display: inline-block;
        position: relative;
    }


    .details-banner-info h3:after {
        position: absolute;
        content: "";
        left: 0;
        width: 100%;
        height: 3px;
        border-radius: 2px;
        bottom: -8px;
        background: #e48632 none repeat scroll 0 0;
    }

    .details-genre {
        color: #eee;
        text-transform: capitalize;
        font-weight: 500;
        letter-spacing: 1px;
        margin-bottom: 5px;
        margin-top: 5px;
    }

    .details-time-left {
        color: #eee;
        letter-spacing: .5px;
    }

    .details-time-left i {
        margin-right: 5px;
        color: #ec7532;
    }


    .single_game_meta {
        margin-top: 15px
    }

    .games-details-page-box>ul>li {
        margin-bottom: 5px;
        position: relative;
        padding-left: 15px;
    }

    .games-details-page-box>ul>li:after {
        position: absolute;
        content: "";
        top: 50%;
        left: 0;
        width: 5px;
        height: 5px;
        background: #ff7a21;
        border-radius: 50%;
        -webkit-transform: translate(0, -50%);
        transform: translate(0, -50%);
    }
</style>
{{-- <link rel="stylesheet" href="{{ url('AdminLTE/style.css') }}"> --}}
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_games = $('#table-games').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/sales/'.$sale->id.'/get_games') }}",
            language: {
                "emptyTable": "No games currently in this sale",
                "zeroRecords": "No game found!",
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'image', name: 'image'},
                {data: 'name', name: 'name'},
                {data: 'price', name: 'price'},
                {data: 'discount', name: 'discount'},
                {data: 'developer', name: 'developer'},
                {data: 'released_at', name: 'released_at'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
</script>
@endsection
@section('content')
<section class="fag-breadcrumb-area bg-dark"
    style="
    background-image: {{ $sale->filename == '' ? 'url('.url('/icongame.png').')' : 'url('.url('/storage/photos/'.$sale->filename).')' }};  background-repeat: no-repeat; background-size: 100% 100%;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="games-details-banner">
                    <div class="row">
                        <div class="col-lg-6 col-sm-8">
                            <div class="details-banner-info p-3" style="background-color: rgba(0,0,0,0.5); ">
                                <h3>{{ $sale->title }}
                                </h3>
                                <div class="single_game_meta">
                                    <p class="details-genre"></p>
                                    <p class="details-time-left"><i class="fa fa-calendar"></i>Date:
                                        <span>{{ $sale->started_at .' until '. $sale->ended_at }}</span></p>
                                </div>
                                <div>
                                    <p>{{ $sale->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="row my-4">
    <div class="col container">
        <table id="table-games" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Discount</th>
                    <th scope="col">Developer</th>
                    <th scope="col">Released at</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>

</div>
<div class="row my-5"></div>
@endsection
