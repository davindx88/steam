@extends('layout.admin')
@section('title','Admin | Genres')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>Create new Genre</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <form action="{{ url('admin/gamegenres/store') }}" method="POST">
            @csrf
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Genre Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Genre Name">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row my-5"></div>
@endsection
