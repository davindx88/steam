@extends('layout.admin')
@section('title','Admin | Admins')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')

<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_accounts =  $('#table-accounts').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/admins/get_admins') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
            ],
        });
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Admins</h5>
    </div>
</div>
<div class="row pt-4">
    <div class="col">
        <div class="form-group">
            <a class="btn btn-primary btn-sm form-control-sm w-25" href="{{ url('admin/admins/add') }}"><i
                    class="fa fa-plus"></i>&nbsp&nbsp Register new admin</a>
        </div>
    </div>
</div>
<div class="row py-4">
    <div class="col">
        <table id="table-accounts" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
