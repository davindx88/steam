@extends('layout.admin')
@section('title','Admin | Features')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_genres =  $('#table-genres').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/gamefeatures/get_features') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'image_filename', name: 'image_filename'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Genres</h5>
    </div>
</div>
<div class="row">
    <div class="col">
        @if (session('status'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" style="color: black;">&times;</span>
            </button>
        </div>
        @endif
    </div>
</div>
<div class="row">

    <div class="col">
        <div class="form-group">
            <a class="btn btn-primary btn-sm form-control-sm w-25" href="{{ url('admin/gamefeatures/add') }}"><i
                    class="fa fa-plus"></i>&nbsp&nbsp Create new Feature</a>
        </div>
        <table id="table-genres" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Image</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="row my-5"></div>
@endsection
