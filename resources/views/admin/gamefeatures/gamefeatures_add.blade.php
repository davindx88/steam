@extends('layout.admin')
@section('title','Admin | Features')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>Create new Feature</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
        @endforeach
        @endif
        <form action="{{ url('admin/gamefeatures/store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" id="description"
                        placeholder="Description">
                </div>
            </div>
            <div class="form-group row">
                <label for="image_filename" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control-file" name="image_filename" id="image_filename">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row my-5"></div>
@endsection
