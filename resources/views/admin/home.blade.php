@extends('layout.admin')
@section('title','Admin | Home')
@section('style')
<style>
    body,
    html {
        height: 100%;
    }

    .details-banner-info h3 {
        margin-bottom: 5px;
        color: #fff;
        font-size: 34px;
        text-transform: capitalize;
        font-weight: 600;
        display: inline-block;
        position: relative;
    }


    .details-banner-info h3:after {
        position: absolute;
        content: "";
        left: 0;
        width: 100%;
        height: 3px;
        border-radius: 2px;
        bottom: -8px;
        background: #e48632 none repeat scroll 0 0;
    }

    .details-time-left {
        color: #eee;
        letter-spacing: .5px;
    }

    .details-time-left i {
        margin-right: 5px;
        color: #ec7532;
    }
</style>
@endsection
@section('script')
<script>
    // var a = JSON.parse(`@php echo json_encode($top_genre->pluck('name')->toArray()); @endphp`);
    $(()=>{
        // card
        anime({
            targets: '.info-box',
            translateX: 250,
            direction: 'reverse',
            easing: 'easeInOutSine',
            delay: anime.stagger(100, {from: 'center', easing: 'easeOutQuad'}),
        });
        // component
        anime({
            targets: '.component',
            translateX: 450,
            direction: 'reverse',
            easing: 'easeInOutSine',
            delay: anime.stagger(150, {from: 'center', easing: 'easeOutQuad'}),
        });
        // released games count
        anime({
            targets: '.released-games-count',
            innerHTML: [0, {{ $games->where('status','1')->count() }}],
            easing: 'linear',
            round: 1,
        })
        // unreleased games count
        anime({
            targets: '.unreleased-games-count',
            innerHTML: [0, {{ $games->where('status','0')->count() }}],
            easing: 'linear',
            round: 1,
        })
        // genre count
        anime({
            targets: '.genres-count',
            innerHTML: [0, {{ $genres->count() }}],
            easing: 'linear',
            round: 1,
        })
        // platforms count
        anime({
            targets: '.platforms-count',
            innerHTML: [0, {{ $platforms->count() }}],
            easing: 'linear',
            round: 1,
        })
        // Account count
        anime({
            targets: '.accounts-count',
            innerHTML: [0, {{ $accounts->count() }}],
            easing: 'linear',
            round: 1,
        })
        // developer count
        anime({
            targets: '.developers-count',
            innerHTML: [0, {{ $developers->count() }}],
            easing: 'linear',
            round: 1,
        })
        anime({
            targets: '.games-purchased-count',
            innerHTML: [0, {{ $monthly_purchases_made->pluck('count')->sum() }}],
            easing: 'linear',
            round: 1,
        })
        // Games - Genre Chart
        let genre_games_chart = new Chart("genre_games_chart", {
            type: 'pie',
            data: {
                datasets: [{
                    data: {{ json_encode($top_genre->pluck('games_count')->toArray()) }},
                }],
                labels: JSON.parse(`@php echo json_encode($top_genre->pluck('name')->toArray()); @endphp`),
            },
            options: {
                maintainAspectRatio: false,
                animation:{
                    onprogress: {
                        onProgress: function(animation) {
                            progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                        }
                    }
                },
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 255, 255)'
                    }
                },
                plugins: {
                    colorschemes: {
                        scheme: 'brewer.Spectral7'
                    }
                },

            }
        });
        //
        let platform_games_chart = new Chart("platform_games_chart", {
            type: 'pie',
            data: {
                datasets: [{
                    data: {{ json_encode($top_platform->pluck('games_count')->toArray()) }},
                }],
                labels: JSON.parse(`@php echo json_encode($top_platform->pluck('name')->toArray()); @endphp`),
            },
            options: {
                maintainAspectRatio: false,
                animation:{
                    onprogress: {
                        onProgress: function(animation) {
                            progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                        }
                    }
                },
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 255, 255)'
                    }
                },
                plugins: {
                    colorschemes: {
                        scheme: 'brewer.Spectral7'
                    }
                },

            }
        });

        // most popular game
        let games_account_chart = new Chart("games_account_chart", {
            type: 'horizontalBar',
            data: {
                datasets: [{
                    data: {{ json_encode($top_games->pluck('gamers_count')->toArray()) }},
                    labels: '# purchases made',
                }],
                labels: JSON.parse(`@php echo json_encode($top_games->pluck('name')->toArray()); @endphp`),
            },
            options: {
                maintainAspectRatio: false,
                animation:{
                    onprogress: {
                        onProgress: function(animation) {
                            progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                        }
                    }
                },
                legend: {
                    display: false,
                    labels: {
                        fontColor: 'rgb(255, 255, 255)'
                    }
                },
                plugins: {
                    colorschemes: {
                        scheme: 'brewer.Spectral7'
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontColor: '#fff',

                        }
                    }],
                    yAxes: [{
                        ticks:{
                            fontColor: '#fff',
                        }
                    }]
                }
            }
        });
        // purchased games 6 month ago
        let games_purchased_chart = new Chart("games_purchased_chart",{
            type: 'line',
            data: {
                datasets: [{
                    data: {{ json_encode($monthly_purchases_made->pluck('count')->toArray()) }},
                    label: 'games purchases'
                }],
                labels: JSON.parse(`@php echo json_encode($monthly_purchases_made->pluck('date')->toArray()); @endphp`),
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false,
                    labels: {
                        fontColor: 'rgb(255, 255, 255)'
                    },
                },
                plugins: {
                    colorschemes: {
                        scheme: 'brewer.Spectral7'
                    },
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            fontColor: '#fff',
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks:{
                            fontColor: '#fff',
                        }
                    }]
                }
            }
        });
    });
</script>
@endsection

@section('content')
<div class="row">
    <div class="col">
        <h1 class="m-0 component">Welcome, {{ Auth::user()->username }}</h1>
    </div>
</div>
<div class="row py-4">
    <div class="col-5">
        <div class="info-box">
            <div class="card-body">
                {{-- Account and developer --}}
                <div class="my-2">
                    <h3>Registered Account and Developer</h3>
                    <div class="border-top my-2"></div>
                    <p>Currently there are <span class="accounts-count"></span> account in total registered on FAF
                        Gaming. <br> Currently there are <span class="developers-count"></span> developers in total
                        registered
                        on FAF
                        Gaming</p>
                </div>
                {{-- Genre --}}
                <div class="my-2">
                    <h3>Top 3 Genres Registered In Games</h3>
                    <div class="border-top my-2"></div>
                    <p>Currently there are <span class="genres-count"></span> genre in total registered in FAF
                        Gaming and these are the most genre registered in all games
                    </p>
                    <div class="chart-container component">
                        <canvas id="genre_games_chart" width="300" height="300"></canvas>
                    </div>
                    <br>
                </div>
                {{-- Platform --}}
                <div class="my-2">
                    <h3>Top 3 Platform Used By Games</h3>
                    <div class="border-top my-2"></div>
                    <p>There are <span class="platforms-count"></span> platforms in total registered in FAF Gaming</p>
                    <div class="chart-container component">
                        <canvas id="platform_games_chart" width="300" height="300"></canvas>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="col-7">
        <div class="info-box">
            <div class="card-body">
                {{-- Top 3 Games most purchase --}}
                <div class="my-2">
                    <h3>Ongoing Sale</h3>
                    <div class="border-top my-2"></div>
                    @if ($ongoing_sale == null)
                    <p>There are currently no sale going on</p>
                    @else
                    <div class="bg-light" style="
                        background-size: contain;
                        background: {{ $ongoing_sale->filename == '' ? "url(".url('/icongame.png').")" : "url(".url('/storage/photos/'.$ongoing_sale->filename).")" }};
                        background-repeat: no-repeat;
                        ">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="games-details-banner">
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-8">
                                                <div class="details-banner-info p-3"
                                                    style="background-color: rgba(0,0,0,0.5); ">
                                                    <h3>{{ $ongoing_sale->title }}
                                                    </h3>
                                                    <div class="single_game_meta">
                                                        <p class="details-genre"></p>
                                                        <p class="details-time-left"><i class="fa fa-calendar"></i>Date:
                                                            <span>{{ $ongoing_sale->started_at .' until '. $ongoing_sale->ended_at }}</span>
                                                        </p>

                                                        <p><a class='btn btn-warning'
                                                                href="{{ url('/admin/sales/'.$ongoing_sale->id) }}">Check
                                                                sale</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <br>
                </div>
                {{-- Top 3 Games most purchase --}}
                <div class="my-2">
                    <h3>Top 3 Most Purchased Games</h3>
                    <div class="border-top my-2"></div>
                    <p>Currently there are <span class="released-games-count"></span> released games in total and <span
                            class="unreleased-games-count"></span> unreleased games in total registered on FAF
                        Gaming and these are the top 3 most purchased games in FAF Gaming</p>
                    <div class="chart-container component">
                        <canvas id="games_account_chart" width="300" height="300"></canvas>
                    </div>
                    <br>
                </div>
                {{-- Purchase --}}
                <div class="my-2">
                    <h3>Purchases Made</h3>
                    <div class="border-top my-2"></div>
                    <p>There are <span class="games-purchased-count"></span> purchase made since
                        {{ $monthly_purchases_made->count() }} month ago</p>
                    <div class="chart-container component">
                        <canvas id="games_purchased_chart" width="300" height="300"></canvas>
                    </div>
                    <br>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
