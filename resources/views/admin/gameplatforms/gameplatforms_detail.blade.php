@extends('layout.admin')
@section('title','Admin | Platform')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>Update Platform</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
        @endforeach
        @endif
        <form action="{{ url('admin/gameplatforms/'.$platform->id.'/update') }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name"
                        value="{{ $platform->name }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="filename" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-1 text-center">
                    @if ($platform->filename)
                    <img id="image" src="{{ url('/storage/photos/'.$platform->filename) }}" width="30" height="30" />
                    @else
                    <img id="image" src="{{ url('/icongame.png') }}" width="30" height="30" />
                    @endif
                </div>
                <div class="col-sm-9">
                    <input type="file" class="" name="filename" id="filename" onchange="readURL(this)">
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-block">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row my-5"></div>
@endsection
