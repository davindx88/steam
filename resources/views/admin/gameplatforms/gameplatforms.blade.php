@extends('layout.admin')
@section('title','Admin | Platforms')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')
<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_platforms =  $('#table-platforms').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/gameplatforms/get_platforms') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'filename', name: 'filename'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
    function delete_confirmation(id) {
        Swal.fire({
            title: "Delete?",
            text: "Please ensure and then confirm!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value === true) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: `/admin/gameplatforms/${id}/destroy`,
                    dataType: 'JSON',
                    success: function (results) {
                        if (results.success === true) {
                            swal.fire("Done!", results.message, "success");
                            $('#table-platforms').DataTable().ajax.reload();
                        } else {
                            swal.fire("Error!", results.message, "error");
                        }
                    }
                });
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        })
    }
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Platforms</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <div class="form-group">
            <a class="btn btn-primary btn-sm form-control-sm w-25" href="{{ url('admin/gameplatforms/add') }}"><i
                    class="fa fa-plus"></i>&nbsp&nbsp Create new Platforms</a>
        </div>
        <table id="table-platforms" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Image</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="py-3"></div>
@endsection
