@extends('layout.admin')
@section('title','Admin | Games')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')

<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_games =  $('#table-games').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/games/get_unreleased_games') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'image', name: 'image'},
                {data: 'name', name: 'name'},
                {data: 'price', name: 'price'},
                {data: 'developer', name: 'developer'},
                {data: 'released_at', name: 'released_at'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
    function accept_confirmation(id) {
        Swal.fire({
            title: "Accept?",
            text: "Accept and release the game ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Accept and release!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0,
            showLoaderOnConfirm: true,
        }).then(function (e) {
            if (e.value === true) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: `/admin/games/accept/${id}`,
                    dataType: 'JSON',
                    success: function (results) {
                        if (results.success === true) {
                            swal.fire("Done!", results.message, "success");
                            $('#table-games').DataTable().ajax.reload();
                        } else {
                            swal.fire("Error!", results.message, "error");
                        }
                    }
                });
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        })
    }
    function reject_confirmation(id) {
        Swal.fire({
            title: "Accept?",
            text: "Reject the game ?",
            input: 'textarea',
            inputPlaceholder: 'Type your message here...',
            showCancelButton: !0,
            confirmButtonText: "Reject",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0,
            showLoaderOnConfirm: true
        }).then(function (e) {
            console.log(e);
            if (e.isConfirmed === true) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: `/admin/games/reject/${id}`,
                    data: {
                        'message_admin' : e.value
                    },
                    dataType: 'JSON',
                    success: function (results) {
                        if (results.success === true) {
                            swal.fire("Done!", results.message, "success");
                            $('#table-games').DataTable().ajax.reload();
                        } else {
                            swal.fire("Error!", results.message, "error");
                        }
                    }
                });
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        })
    }
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Unreleased Games</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <table id="table-games" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Developer</th>
                    <th scope="col">Released at</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="py-3"></div>
@endsection
