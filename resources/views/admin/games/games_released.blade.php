@extends('layout.admin')
@section('title','Admin | Games')
@section('style')
<style>
    label,
    .dataTables_info,
    .dataTables_paginate,
    .paging_simple_numbers,
    .paginate_button {
        color: white !important;
    }
</style>
@endsection
@section('script')

<script>
    $(()=>{
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        let table_games = $('#table-games').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/games/get_released_games') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'image', name: 'image'},
                {data: 'name', name: 'name'},
                {data: 'price', name: 'price'},
                {data: 'developer', name: 'developer'},
                {data: 'released_at', name: 'released_at'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            // columnDefs: [
            //     {"width" : 30%, "targets":4}
            // ],
            order: [[ 6, "desc" ]],
        });
    });
</script>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h5>List Of Released Games</h5>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <table id="table-games" class="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Developer</th>
                    <th scope="col">Released at</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="py-3"></div>
@endsection
