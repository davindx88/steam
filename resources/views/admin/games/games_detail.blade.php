@extends('layout.admin')
@section('title','Admin | Developers')
@section('style')
<!--Bootstrap css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/bootstrap.css') }}">
<!--Font Awesome css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/font-awesome.min.css') }}">
<!--Magnific css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/magnific-popup.css') }}">
<!--Owl-Carousel css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ url('faf/assets/css/owl.theme.default.min.css') }}">
<!--NoUiSlider css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/nouislider.min.css') }}">
<!--Animate css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/animate.min.css') }}">
<!--Site Main Style css-->
<link rel="stylesheet" href="{{ url('AdminLTE/style.css') }}">
<!--Responsive css-->
<link rel="stylesheet" href="{{ url('faf/assets/css/responsive.css') }}">
<style>
    .layer {
        background-color: rgba(0, 0, 0, 0.7);
        padding: 40px;
    }
</style>
@endsection
@section('script')
<script></script>
@endsection
@section('content')
<div style="background-color: black;">
    <section class="fag-breadcrumb-area bg-dark"
        style="background-image: {{ $game->background_filename == '' ? 'url('.url('/icongame.png').')' : 'url('.url('/storage/'.$game->background_filename).')' }};">
        <div class=" container layer">
            <div class="row">
                <div class="col-12">
                    <div class="games-details-banner">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4">
                                <div class="details-banner-thumb">
                                    <img src='{{ $game->thumbnail_filename == '' ? url('/icongame.png') : url('/storage/'.$game->thumbnail_filename) }}'
                                        alt="games">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-8">
                                <div class="details-banner-info">
                                    <h3>{{ $game->name }} <span class="single_rating"><i
                                                class="fa fa-star"></i>{{ number_format($game->reviews->avg('pivot.rating'), 1, '.', '') }}</span>
                                    </h3>
                                    <div class="single_game_meta">
                                        <p class="details-genre">{{ $game->genres->implode('name', ' | ') }}</p>
                                        <p class="details-time-left"><i class="fa fa-calendar"></i>Release date:
                                            <span>{{ $game->released_at??"Not released yet"}}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="game-price single_game_price">
                                    @if ($game->price > 0)
                                    <h4>
                                        @php
                                        $isSale = false;
                                        foreach ($game->sales as $sale_games) {
                                        if ($sale_games->started_at < Carbon\Carbon::now() && $sale_games->ended_at >
                                            Carbon\Carbon::now()) {
                                            $price = $game->price - ($game->price * $sale_games->pivot->discount / 100);
                                            echo '<h2><del>' . "IDR " . number_format($game->price, 2, '.', ',')
                                                    .'</del></h2>' .'<h2>' . "IDR " . number_format($price, 2, '.', ',')
                                                . '</h2>';
                                            $isSale = true;
                                            }
                                            }
                                            if(!$isSale){
                                            echo "<h2>IDR" . number_format($game->price, 2, '.', ','). '</h2>';
                                            }
                                            // $sale = App\Sale::where('started_at', '<=',Carbon\Carbon::now()) ->
                                                // where('ended_at','>=',Carbon\Carbon::now())->first();
                                                // // dd(App\Sale::all());
                                                // if ($sale) {
                                                // $price = 0;
                                                // foreach ($game->sales as $sale_games) {
                                                // if ($sale_games->id == $sale->id) {
                                                // $price = $game->price - ($game->price * $sale_games->pivot->discount
                                                // /
                                                // 100);
                                                // }
                                                // }
                                                // if($price == 0){
                                                // $price = $game->price;
                                                // }
                                                // } else {
                                                // $price = $game->price;
                                                // }
                                                @endphp
                                                {{-- @idr($price) --}}
                                    </h4>
                                    @else
                                    <h4 class="text-success">Free</h4>
                                    @endif
                                </div>
                                <div class="details-banner-action">
                                    {{-- Check apakah user punya gamenya --}}
                                    {{-- @if ($user !== null && $user->games()->where('id', $game->id)->first() !== null) --}}
                                    {{-- <div class="game-buy">
                                        <a class="fag-btn-outline bg-success text-light">Owned</a>
                                    </div> --}}
                                    {{-- @else --}}
                                    <form action="{{ url('/admin/games/download') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="slug_url" value="{{ $game->slug_url }}">
                                        <button class="fag-btn">Download Game</button>
                                    </form>
                                    {{-- @endif --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->


    <!-- games Details Page Start -->
    <section class="fag-games-details-page section_100">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 offset-lg-3">
                    <div class="games-details-page-box">
                        <h2>Developer :
                            <a href="{{ url('/admin/developers/'.$game->developer->developer->slug_url) }}">
                                {{ $game->developer->developer->name }}
                            </a>
                        </h2>
                        <div class="tv-panel-list">
                            <div class="tv-tab">
                                <ul class="nav nav-pills tv-tab-switch" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" id="pills-brief-tab" data-toggle="pill"
                                            href="#pills-brief" role="tab" aria-controls="pills-brief"
                                            aria-selected="true">Game Description</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-cast-tab" data-toggle="pill" href="#pills-cast"
                                            role="tab" aria-controls="pills-cast" aria-selected="false">Features</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-reviews-tab" data-toggle="pill"
                                            href="#pills-reviews" role="tab" aria-controls="pills-reviews"
                                            aria-selected="false">reviews
                                            ({{ $game->reviews->count() }})</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade active show" id="pills-brief" role="tabpanel"
                                    aria-labelledby="pills-brief-tab">
                                    <div class="tab-gamess-details">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-body">
                                                    <p>{{ $game->description }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-cast" role="tabpanel"
                                    aria-labelledby="pills-cast-tab">
                                    <div class="tab-gamess-details">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-body">
                                                    <div class="row">
                                                        @forelse ($game->features as $feature)
                                                        <div class="col-lg-6 col-sm-6">
                                                            <div class="features-game">
                                                                <div class="feature-image">
                                                                    <img src="{{ $feature->image_filename == "" ? url('faf/assets/img/feature-1.png') : url('/storage/photos/'.$feature->image_filename) }}"
                                                                        alt="feature"
                                                                        class='bg-warning rounded-circle' />
                                                                </div>
                                                                <div class="feature-text">
                                                                    <h3>{{ $feature->name }}</h3>
                                                                    <p>{{ $feature->description }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @empty
                                                        <div class="col-12">
                                                            <div class="features-game">
                                                                <div class="feature-text">
                                                                    <h3>No features</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforelse
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-reviews" role="tabpanel"
                                    aria-labelledby="pills-reviews-tab">
                                    <div class="tab-gamess-details">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-body">
                                                    <div class="fag-comment-list">
                                                        @forelse ($game->reviews as $review)
                                                        <div class="single-comment-item">
                                                            <div class="single-comment-box">
                                                                <div class="main-comment">
                                                                    <div class="author-image">
                                                                        <img src="{{ $review->filename == "" ? url('faf/assets/img/4.jpg') : url('/storage/'.$review->filename) }}"
                                                                            alt="author">
                                                                    </div>
                                                                    <div class="comment-text">
                                                                        <div class="comment-info">
                                                                            <h4>{{ $review->first_name . ' ' . $review->last_name }}
                                                                            </h4>
                                                                            <ul>
                                                                                @for ($i = 0; $i < 5; $i++) <li><i
                                                                                        class="fa fa-star{{ $i+1 <= $review->pivot->rating ? '' : '-o'}}"></i>
                                                                                    </li>
                                                                                    @endfor
                                                                            </ul>
                                                                            <p>{{ $review->pivot->created_at->diffForHumans() }}
                                                                            </p>
                                                                        </div>
                                                                        <div class="comment-text-inner">
                                                                            <p>{{ $review->pivot->message }}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @empty
                                                        <div class="col-12">
                                                            <div class="features-game">
                                                                <div class="feature-text">
                                                                    <h3>There's no review yet</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforelse
                                                    </div>
                                                    <!-- /end comment list -->
                                                    {{-- <div class="fag-leave-comment">
                                                        <form>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="comment-field">
                                                                        <textarea class="comment"
                                                                            placeholder="Comment..."
                                                                            name="comment"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="comment-field">
                                                                        <button type="submit" class="fag-btn">Add
                                                                            Comment
                                                                            <span></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div> --}}
                                                    <!-- /end comment form -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Row -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
