@php
use App\Account;
$account = Auth::user();
$is_login = Auth::check();
@endphp
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Faf | Gaming HTML Template from Themescare">
    <meta name="keyword" content="game, gaming, videogame, developer, steam, studio, team">
    <meta name="author" content="Themescare">
    <!-- Title -->
    <title>Faf | Gaming HTML Template</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('faf/assets/img/favicon/favicon-32x32.png') }}">
    <!--Bootstrap css-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha256-aAr2Zpq8MZ+YA/D6JtRD3xtrwpEz2IqOS+pWD/7XKIw=" crossorigin="anonymous" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha256-OFRAJNoaD8L3Br5lglV7VyLRf0itmoBzWUoM+Sji4/8=" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js" defer></script>
    <link rel="stylesheet" href="{{ url('faf/assets/css/bootstrap.css') }}">

    <!--Font Awesome css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/font-awesome.min.css') }}">
    <!--Magnific css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/magnific-popup.css') }}">
    <!--Owl-Carousel css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('faf/assets/css/owl.theme.default.min.css') }}">
    <!--NoUiSlider css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/nouislider.min.css') }}">
    <!--Animate css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/animate.min.css') }}">
    <!--Site Main Style css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/style.css') }}">
    <!--Responsive css-->
    <link rel="stylesheet" href="{{ url('faf/assets/css/responsive.css') }}">
    {{-- Custom CSS --}}
    <link rel="stylesheet" href="{{ url('custom/css/style.css') }}">
    {{-- Sweet alert --}}
    <link rel="stylesheet" href="{{ url('/AdminLTE/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ url('/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <script src="{{ url('/AdminLTE/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
</head>

<body>
    @include('sweetalert::alert')


    <!-- Custom Cursor Start -->
    <div id="cursor-large"></div>
    <div id="cursor-small"></div>
    <!-- Custom Cursor End -->


    <!-- Header Area Start -->
    <nav class="fag-header navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{ url('faf/assets/img/logo.png') }}"
                    alt="site logo" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="menu-toggle"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="header_menu  mr-auto">
                    <li class="nav-item active">
                        <a href="/" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="/search" class="nav-link">Explore</a>
                    </li>
                    <li class="nav-item">
                        <a href="/users" class="nav-link">Users</a>
                    </li>
                    <li class="nav-item">
                        <a href="/developers" class="nav-link">Developers</a>
                    </li>
                    @if ($is_login)
                    <li class="nav-item">
                        <a href="/friends" class="nav-link">Friends</a>
                    </li>
                    @endif
                </ul>
                <div class="header-right  my-2 my-lg-0">
                    <div class="header-search">
                        <a href="#" id="search-trigger"><i class="fa fa-search"></i></a>
                    </div>
                    <!-- Search Block -->
                    <div class="search-block">
                        <a href="#" class="search-toggle">
                            <i class="fa fa-times"></i>
                        </a>
                        <form action="/search" method="GET">
                            <input type="text" name="n" placeholder="What're you looking for?">
                            <span class="fa fa-search"></span>
                        </form>
                    </div>
                    <!-- /Search Block -->
                    <div class="header-cart">
                        <a href="/cart">
                            <img src="{{ url('faf/assets/img/shopping-cart.png') }}" alt="shopping cart" />
                            @if ($is_login)
                            @idr($account->balance)
                            @endif
                        </a>
                    </div>
                    <div class="header-auth  nav-item dropdown">
                        @if ($is_login)
                        <a class="lang-btn nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            aria-expanded="false">
                            {{ $account->username }}
                        </a>
                        <ul class="user_menu dropdown-menu">
                            <li><a href="/users/{{ $account->username }}">Profile</a></li>
                            <li><a href="/topup">Topup</a></li>
                            <li><a href="/gifts">Gift</a></li>
                            <li><a href="/history">History</a></li>
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                        @else
                        <a class="lang-btn nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            aria-expanded="false">
                            <i class="fa fa-user mr-1"></i>
                            Account
                        </a>
                        <ul class="user_menu dropdown-menu">
                            <li><a href="/login">Login</a></li>
                            <li><a href="/register">Register</a></li>
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- Header Area End -->
    {{-- @section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.category').select2();
        });
    </script>
    @endsection --}}
    @yield('content')


    <!-- Footer Area Start -->
    <footer class="fag-footer">
        <div class="footer-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="single-footer">
                            <h3>About us</h3>
                            <p>Hello we are student of Sekolah Tinggi Teknik Surabaya (STTS). We made this project for our beloved courses "Web Programming Framework"</p>
                            <p>In this website we're trying to make clone of popular social gaming website called <u><a href="https://store.steampowered.com/">Steam</a></u>.  Where you can buy, play, and discuss games</p>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <div class="widget-content">
                            <div class="row clearfix">
                                <div class=" col-lg-6 col-md-6 col-sm-12">
                                    <div class="single-footer">
                                        <h3>Explore</h3>
                                        <ul>
                                            <li><a href="/"><span class="fa fa-caret-right"></span>Home</a></li>
                                            <li><a href="/search"><span class="fa fa-caret-right"></span>Explore</a></li>
                                            <li><a href="/users"><span class="fa fa-caret-right"></span>Users</a></li>
                                            <li><a href="/developers"><span class="fa fa-caret-right"></span>Developers</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-lg-3 col-md-6 col-sm-12">
                        <div class="single-footer">
                            <h3>Web Developer</h3>
                            <div class="footer-contact">
                                <h4 class="title"><i class="fa fa-gamepad"></i>Gamer </h4>
                                <p>
                                    <a href="https://www.linkedin.com/in/davin-djayadi-04a8a41ab/">Davin Djayadi</a><br>
                                    <a href="https://www.linkedin.com/in/yulius-setiawan-13708a1aa/">Yulius</a>
                                </p>
                            </div>
                            <div class="footer-contact">
                                <h4 class="title"><i class="fa fa-code"></i>Developer</h4>
                                <p>
                                    <a href="https://www.linkedin.com/in/ivan-budihardjo-61a8a21ab/">Ivan Budihardjo</a>
                                </p>
                            </div>
                            <div class="footer-contact">
                                <h4 class="title"><i class="fa fa-id-badge"></i>Admin</h4>
                                <p>
                                    <a href="https://www.linkedin.com/in/james-f-308b6111b/">James Jeremy F</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="footer-bottom-inn">
                            <div class="footer-logo mb-3">
                                <a href="/">
                                    <img src="{{ url('faf/assets/img/logo.png') }}" alt="site logo" />
                                </a>
                            </div>
                            <div class="copyright">
                                <p>&copy; Copyrights 2020 FAF - All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->


    <!--Jquery js-->
    <script src="{{ url('faf/assets/js/jquery.min.js') }}"></script>
    <!-- Popper JS -->
    <script src="{{ url('faf/assets/js/popper.min.js') }}"></script>
    <!--Bootstrap js-->
    <script src="{{ url('faf/assets/js/bootstrap.min.js') }}"></script>
    <!--Owl-Carousel js-->
    <script src="{{ url('faf/assets/js/owl.carousel.min.js') }}"></script>
    <!--Magnific js-->
    <script src="{{ url('faf/assets/js/jquery.magnific-popup.min.js') }}"></script>
    <!--wNumb js-->
    <script src="{{ url('faf/assets/js/wNumb.js') }}"></script>
    <!--NoUiSlider js-->
    <script src="{{ url('faf/assets/js/nouislider.min.js') }}"></script>
    <!-- Isotop Js -->
    <script src="{{ url('faf/assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ url('faf/assets/js/custom-isotop.js') }}"></script>
    <!-- Counter JS -->
    <script src="{{ url('faf/assets/js/jquery.counterup.min.js') }}"></script>
    <!-- Way Points JS -->
    <script src="{{ url('faf/assets/js/waypoints-min.js') }}"></script>
    <!-- Custom Clock JS -->
    <script src="{{ url('faf/assets/js/clock-custom.js') }}"></script>
    <!--Main js-->
    <script src="{{ url('faf/assets/js/main.js') }}"></script>
    @yield('script')
</body>

</html>
