@extends('layout.user')
@section('content')
<section class="slider-area">
    <div class="fag-slide owl-carousel">
        <div class="fag-main-slide slide-1"
            style="background:url('Photos/game/2.jpg');
            background-repeat: no-repeat;background-size:100% 100%;"
        >
           <div class="fag-main-caption">
              <div class="fag-caption-cell">
                 <div class="container">
                    <div class="row">
                       <div class="col-12">
                          <div class="slider-text">
                             <h3>Welcome Gamer!</h3>
                             <h2>Start Exploring Now</h2>
                             <a class="fag-btn-outline" href="/search"><span class="fa fa-play"></span>Explore</a>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
</section>
@if ($sale!==null)
<!-- Breadcrumb Area Start -->
<section class="fag-breadcrumb-area"
    style="background:url('/storage/photos/{{ $sale->filename }}');
    background-repeat: no-repeat;background-size:100% 100%;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcromb-box" >
                    <h3 class="text-left float-left p-3" style="background: #0006">{{ $sale->title }}</h3>
                    <div style="clear:both"></div>
                    <div class="text-left float-left" style="background: #0006">
                        <a href="/sales" class="fag-btn-outline">Join Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection
