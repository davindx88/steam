<div style="text-align:center">
	<h1 style="color:#ccca">FAF Gaming | Laravel</h1>
	<h2 style="font-weight: 100 ">Hello <strong>{{ $user->first_name }} {{ $user->last_name }}</strong></h2>
	<p style="margin-bottom: 10px">
		Thanks for registering an account with Faf! We hope you will get good experience with our website.<br>
        Before we get started, we’ll need to verify your account. To do so, click on the button below or
        <a href="http://localhost:8000/verif/{{ $user->token }}">click this link</a>.
    </p>
	<a href="http://localhost:8000/verif/{{ $user->token }}" style="
		display: inline-block;
		padding: 10px 20px;
		border-radius: 5px;
		color: #ded;
		font-weight: bold;
		background-color: royalblue;
	    text-decoration: none;
	    background-image: linear-gradient(37deg, rgb(32, 218, 233),rgb(40, 21, 236));
	    border: 1px solid black;
	    border-bottom: 3px solid black;
	">Verify</a>
</div>
