php artisan make:middleware IsLoggedInMiddleware
php artisan make:middleware IsNotLoggedInMiddleware
php artisan make:middleware IsAdminMiddleware
php artisan make:middleware LoggingMiddleware
php artisan make:middleware RateLimitMiddleware
php artisan make:middleware RoleMiddleware
