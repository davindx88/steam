<?php

use App\Account;
use App\ChatRoom;
use Illuminate\Database\Seeder;

class ChatRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // harus buat manual :(
        Account::where('type', 0)->get()
        ->each(function($account){
            Account::where('type',0)->where('id','<>',$account->id)->get()
            ->each(function($friend)use($account){
                $skip = rand(0,2) <= 1;
                $room = ChatRoom::where('friend_id', $account->id)
                    ->where('account_id', $friend->id)
                    ->where('status', 0)
                    ->first();
                if(!$skip && $room === null){
                    $room = new ChatRoom();
                    $room->account_id = $account->id;
                    $room->friend_id = $friend->id;
                    $room->status = rand(0,1);
                    $room->save();
                }
            });
        });
    }
}
