<?php

use App\FriendRequest;
use Illuminate\Database\Seeder;

class FriendRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(FriendRequest::class, 100)->create();
    }
}
