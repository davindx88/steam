<?php

use App\Htransaction;
use Illuminate\Database\Seeder;

class HTransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Buat HTrans lalu baru buat DTRANS di DTRANS seeder
        factory(Htransaction::class, 200)->create();
    }
}
