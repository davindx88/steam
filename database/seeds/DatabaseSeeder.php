<?php

use App\Feature;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AccountsTableSeeder::class,
            FriendRequestsTableSeeder::class, // TimeStamp
            ChatRoomsTableSeeder::class, // TimeStamp /Chat
            PlatformsTableSeeder::class,
            GenresTableSeeder::class,
            GamesTableSeeder::class, // TimeStamp
            TopupsTableSeeder::class, // TimeStamp
            ViewsTableSeeder::class, // TimeStamp
            HTransactionsTableSeeder::class, // TimeStamp
            DTransactionsTableSeeder::class,
            GiftsTableSeeder::class,
            FeaturesTableSeeder::class,
            ReviewsTableSeeder::class, // TimeStamp
            SalesTableSeeder::class,
            ChatsTableSeeder::class
        ]);
    }
}
