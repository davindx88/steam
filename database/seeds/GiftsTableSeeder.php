<?php

use App\Account;
use App\Gift;
use App\Htransaction;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        Htransaction::where('type', 1)->get()->each(function($htrans)use($faker){
            // Insert GIFT
            $gift = new Gift;
            $gift->account_id = $htrans->account_id;
            $gift->friend_id = Account::where('id','<>',$htrans->account_id)->where('type',0)->inRandomOrder()->first()->id;
            $gift->htransaction_id = $htrans->id;
            $gift->message = $faker->realtext(100);
            $gift->status = 0;
            $gift->save();
            // Random Response
            // chance statusnya 0 60%;
            $chance = $faker->numberBetween(0,10) >= 6;
            if($chance === false){
                // Kalau False jadi random status antara -1 / 1
                $newStatus = $faker->numberBetween(0,1)*2-1;
                if($newStatus == -1){
                    // Reject
                    // Update Gift Status
                    $gift->status = -1;
                    $gift->save();
                    // Iterate untuk tiap games pada transaksi
                    $htrans = $gift->transaction;
                    $buyer = $htrans->account;
                    $htrans->games->each(function($game)use($buyer){
                        // Kembalikan uang buyer
                        $buyer->balance += $game->pivot->price;
                    });
                    $buyer->save();
                }else{
                    // Accept
                    // Pengecekan apakah user memiliki Game
                    $receiver = $gift->receiver;
                    $invalidgame = $receiver->games()
                    ->whereIn('id', $gift->transaction->games->pluck('id'))
                    ->first();
                    $valid = $invalidgame === null;
                    if($valid){
                        // Update Gift Status
                        $gift->status = 1;
                        $gift->save();
                        // Iterate untuk tiap games pada transaksi
                        $htrans = $gift->transaction;
                        $htrans->games->each(function($game)use($receiver){
                            // Tambah ke AccountGames
                            $receiver->games()->attach($game->id,[
                                'owned_at' => Carbon::now()
                            ]);
                            // Tambah uang developer
                            $developer = $game->developer;
                            $developer->balance += $game->pivot->price;
                            $developer->save();
                        });
                    }
                }
            }
        });
    }
}
