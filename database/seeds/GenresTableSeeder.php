<?php

use App\Genre;
use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list_genre = [
            "Action",
            "Platform",
            "Shooter",
            "Fighting",
            "Beat 'em up",
            "Stealth game",
            "Survival",
            "Battle Royale",
            "Rhythm",
            "Survival horror",
            "Metroidvania",
            "Visual novels",
            "Interactive movie",
            "Real-time 3D adventures",
            "Action RPG",
            "MMORPG",
            "Roguelikes",
            "Tactical RPG",
            "Sandbox RPG",
            "First-person party-based RPG",
            "JRPG",
            "Monster Tamer",
            "Simulation",
            "Artillery game",
            "Auto battler",
            "Multiplayer online battle arena (MOBA)",
            "Real-time strategy (RTS)",
            "Real-time tactics (RTT)",
            "Tower defense",
            "Turn-based strategy (TBS)",
            "Turn-based tactics (TBT)",
            "Wargame",
            "Sports",
            "Racing",
            "Competitive",
            "Sports-based fighting",
            "MMO",
            "Boardgame",
            "Card game",
            "Horror",
            "Idle",
            "Logic",
            "Party",
            "Programming",
            "Trivia",
            "Typing",
            "Sandbox",
            "Creative",
            "Open world"
        ];
        collect($list_genre)->each(function($genre){
            $newGenre = new Genre();
            $newGenre->name = $genre;
            $newGenre->save();
        });
    }
}
