<?php

use App\Account;
use App\Game;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Game::all()->each(function ($game) {
            $accounts = $game->gamers->take(5);
            $accounts->each(function ($account) use ($game) {
                $rating_message = [
                    [
                        'rating' => 1,
                        'message' => 'I dont like this game',
                    ],
                    [
                        'rating' => 2,
                        'message' => 'I prefer the other games than this one',
                    ],
                    [
                        'rating' => 3,
                        'message' => 'Could be better if they added more interesting features',
                    ],
                    [
                        'rating' => 4,
                        'message' => 'I usually play this game everyday',
                    ],
                    [
                        'rating' => 5,
                        'message' => 'I love this game',
                    ],
                ];

                $rand = rand(0, count($rating_message) - 1);
                $game->reviews()->attach($account->id, [
                    'rating' => $rating_message[$rand]['rating'],
                    'message' => $rating_message[$rand]['message'],
                    'created_at' => Carbon::now()->subMinutes(rand(1, 10000))
                ]);
            });
        });
    }
}
