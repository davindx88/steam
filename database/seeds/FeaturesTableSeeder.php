<?php

use App\Feature;
use App\Game;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class FeaturesTableSeeder extends Seeder
{
    public function run()
    {
        Game::all()->each(function ($game) {
            $list_features = [
                [
                    "name" => "Singleplayer",
                    "description" => "Game for single player ",
                    "image_filename" => "1p"
                ],
                [
                    "name" => "Multiplayer",
                    "description" => "Multiplayer up to 4 player",
                    "image_filename" => "4p"
                ],
                [
                    "name" => "Open world",
                    "description" => "Open world for everyone playing this game",
                    "image_filename" => "open-world"
                ],
                [
                    "name" => "Achievement",
                    "description" => "Gain and unlock new achivement in this game",
                    "image_filename" => "achievement"
                ],
                [
                    "name" => "Leaderboard",
                    "description" => "Get on the top of the leaderboard",
                    "image_filename" => "leaderboard"
                ],
                [
                    "name" => "Singleplayer",
                    "description" => "Playing this alone",
                    "image_filename" => "1p"
                ],
                [
                    "name" => "Online",
                    "description" => "Online playing with your friends",
                    "image_filename" => "online"
                ],
                [
                    "name" => "Multiplayer",
                    "description" => "Multiplayer up to 3 player",
                    "image_filename" => "3p"
                ],
                [
                    "name" => "Offline",
                    "description" => "Play offline without wasting your internet",
                    "image_filename" => "offline"
                ],
                [
                    "name" => "Fun Family",
                    "description" => "Play this with your family for more fun",
                    "image_filename" => "family"
                ],
                [
                    "name" => "Open world",
                    "description" => "Open world to play with your friends in this game",
                    "image_filename" => "open-world"
                ],
                [
                    "name" => "Singleplayer",
                    "description" => "Game for single player ",
                    "image_filename" => "1p"
                ],
                [
                    "name" => "Multiplayer 4 Player",
                    "description" => "Multiplayer up to 4 player",
                    "image_filename" => "4p"
                ],
                [
                    "name" => "Open world",
                    "description" => "Playing open world",
                    "image_filename" => "open-world"
                ],
                [
                    "name" => "Achievement",
                    "description" => "Unlock new achivement in this game",
                    "image_filename" => "achievement"
                ],
                [
                    "name" => "Leaderboard",
                    "description" => "Be the best player in the world",
                    "image_filename" => "leaderboard"
                ],
            ];
            $rand_index = rand(0, count($list_features) - 6);
            $random_features = array_slice($list_features, $rand_index, $rand_index + 4);
            for ($i = 0; $i < 4; $i++) {
                $feature = new Feature();
                $feature->game_id = $game->id;
                $feature->name = $random_features[$i]["name"];
                $feature->description = $random_features[$i]["description"];

                $source = '/template/features/' . $random_features[$i]["image_filename"] . ".png";
                $destination = uniqid() . '.png';
                Storage::copy($source, '/public/photos/' . $destination);
                $feature->image_filename = $destination;

                $feature->created_at = Carbon::now();
                $feature->updated_at = Carbon::now();
                $feature->save();
            }
        });
    }
}
