<?php

use App\View;
use Illuminate\Database\Seeder;

class ViewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(View::class, 500)->create()->each(function ($view) {
            // Update View Game
            $game = $view->game()->first();
            $game->views += 1;
            $game->save();
        });
    }
}
