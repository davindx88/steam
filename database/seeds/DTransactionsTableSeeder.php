<?php

use App\Game;
use App\Htransaction;
use App\Topup;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DTransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ForEach HTRANS
        Htransaction::all()->each(function($htrans){
            // Buat DTRANS
            // Get Buyer
            $buyer = $htrans->account;
            // Random game + jumlah
            $num_of_game = rand(1,7);
            $list_game = Game::where('status',1); // Active Game (Sudah di Acc admin)
            // Jika beli buat diri sendiri maka pengecekan apakah sudah punya atau blm
            if($htrans->type == 0)
                $list_game = $list_game->whereNotIn('id', $buyer->games->pluck('id'));
            $list_game = $list_game->inRandomOrder()->take($num_of_game)->get();
            // Iterate utk itap game
            $list_game->each(function($game)use($htrans){
                // TODO Beli game yg lagi sales
                // Price
                $price = $game->price;
                $htrans->games()->attach($game->id, [
                    'price' => $price
                ]);
                // Update harga Htrans
                $htrans->total += $price;
            });
            // Update HTrans
            $htrans->save();
            // Kurangi saldo
            $buyer = $htrans->account;
            $buyer->balance -= $htrans->total;
            $buyer->save();
            // TOPUP selama saldo negatif
            while($buyer->balance < 0){
                $topup = factory(Topup::class)->create();
                $buyer->balance += $topup->nominal;
                $buyer->save();
            }
            // Action tergantung pruchase type
            if($htrans->type == 0){
                // Jika Beli utk diri sendiri
                $htrans->games->each(function($game)use($buyer){
                    // Update Saldo Developer
                    $developer = $game->developer;
                    $developer->balance += $game->pivot->price;
                    $developer->save();
                    // Update AccountGames
                    $buyer->games()->attach($game->id,[
                        'owned_at' => Carbon::now()
                    ]);
                });
            }else{
                // Jika Gift maka
            }

        });
    }
}
