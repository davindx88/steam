<?php

use App\Topup;
use Illuminate\Database\Seeder;

class TopupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Topup::class, 100)->create()->each(function($topup){
            // Update Account balance
            $account = $topup->account()->first();
            $account->balance += $topup->nominal;
            $account->save();
        });
    }
}
