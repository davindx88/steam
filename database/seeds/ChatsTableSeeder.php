<?php

use App\Chat;
use App\ChatRoom;
use Illuminate\Database\Seeder;

class ChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list_message = collect([
            'Hello',
            'Hi Wassup',
            'Hello World',
            'Goodbye i still have something to do',
            'What are you doing right now?',
            'Do you ever buy that game?',
            'No that game is kinda suck tho dont buy it',
            'But Why?',
            'Woww what??',
            'Ok but how?',
            'Be right back',
            'Hello nice to meet you :)',
            'Are you anony? Because youre kinda sus tho',
            'Nooooooooooooooooooooooooooo',
            'Heehehehehehehehehe',
            'Haha nice one',
        ]);
        ChatRoom::all()->each(function($room)use($list_message){
            $acc1 = $room->receiver;
            $acc2 = $room->sender;
            $num_of_chat = rand(2,20);
            for ($i=0; $i < $num_of_chat; $i++) {
                $sender = rand(0, 1) == 1 ? $acc1 : $acc2;
                $message = $list_message[rand(0, $list_message->count()-1)];
                // Insert Chat Baru
                $chat = new Chat();
                $chat->chatroom_id = $room->id;
                $chat->sender_id = $sender->id;
                $chat->message = $message;
                $chat->save();
            }
        });
    }
}
