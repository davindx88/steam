<?php

use App\Game;
use App\Genre;
use App\Platform;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        factory(Game::class, 200)->create()->each(function($game , $idx)use($faker){
            // Insert m2m GamePlatform
            Platform::all()->take(rand(1,5))->each(function($platform)use($game){
                $game->platforms()->attach($platform->id,[]);
            });
            // Insert m2m GameGenres
            Genre::all()->take(rand(1,3))->each(function($genre)use($game){
                $game->genres()->attach($genre->id,[]);
            });
            //  Jika game status 1 maka released_at diisi
            if($game->status == 1){
                $game->released_at = $faker->dateTimeBetween('-1 years' ,'now');
                $game->save();
            }
            $id= $idx % 41;
            $source1 = '/template/game/'.$id.".jpg";
            $destination1 = 'game/'.uniqid().'.jpg';
            $source2 = '/template/thumbnail/'.$id.".jpg";
            $destination2 = 'thumbnail/'.uniqid().'.jpg';
            $source3 = '/template/filename/'.$id.".exe";
            $destination3 = 'filename/'.uniqid().'.exe';
            Storage::copy($source1, '/public/'.$destination1);
            Storage::copy($source2, '/public/'.$destination2);
            Storage::copy($source3, '/public/'.$destination3);
            $game ->background_filename = $destination1;
            $game ->thumbnail_filename = $destination2;
            $game ->game_filename = $destination3;
            $game ->save();

        });
    }
}
