<?php

use App\Platform;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class PlatformsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list_platform = [
            [
                'name' => 'Xbox 360',
                'image' => ''
            ],
            [
                'name' => 'Playstation 1',
                'image' => ''
            ],
            [
                'name' => 'Playstation 2',
                'image' => ''
            ],
            [
                'name' => 'Playstation 3',
                'image' => ''
            ],
            [
                'name' => 'Playstation 4',
                'image' => ''
            ],
            [
                'name' => 'Playstation Portable',
                'image' => ''
            ],
            [
                'name' => 'Nintendo 3DS',
                'image' => ''
            ],
            [
                'name' => 'Game Boy',
                'image' => ''
            ],
            [
                'name' => 'Nintendo Wii',
                'image' => ''
            ],
            [
                'name' => 'Nintendo DS',
                'image' => ''
            ],
            [
                'name' => 'Wii U',
                'image' => ''
            ],
            [
                'name' => 'Nintendo Switch',
                'image' => ''
            ],
            [
                'name' => 'Virtual Boy',
                'image' => ''
            ],
            [
                'name' => 'Game Boy Color',
                'image' => ''
            ],
            [
                'name' => 'Game Boy Advance',
                'image' => ''
            ],
            [
                'name' => 'Mobile',
                'image' => ''
            ],
            [
                'name' => 'iOS Apple',
                'image' => ''
            ],
            [
                'name' => 'Android Playstore',
                'image' => ''
            ],
            [
                'name' => 'PC',
                'image' => ''
            ],
        ];
        collect($list_platform)->each(function ($platform, $idx) {
            $new_platform = new Platform();
            $new_platform->name = $platform['name'];

            $source = '/template/platform/' . $idx . ".png";
            $destination = uniqid() . '.png';
            Storage::copy($source, '/public/photos/' . $destination);
            $new_platform->filename = $destination;

            // $new_platform->filename = $platform['image'];
            $new_platform->save();
        });
    }
}
