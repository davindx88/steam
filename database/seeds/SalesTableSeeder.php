<?php

use App\Developer;
use App\Game;
use App\Sale;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_sales = [
            [
                "title" => "Christmas Sales",
                "description" => "Sales for you gamers to play and enjoy the cheap games for Christmas",
                "filename" =>  'sale2',
                "started_at" => '2020-12-20 00:00:00',
                "ended_at" => '2020-12-27 00:00:00',
            ],
            [
                "title" => "New Year Sales",
                "description" => "Sales for you gamers to play and enjoy the cheap games for New year celebration",
                "filename" =>  'sale1',
                "started_at" => '2021-01-01 00:00:00',
                "ended_at" => '2021-01-03 00:00:00',
            ],
            [
                "title" => "WFH Sales",
                "description" => "Sales for you gamers to play and enjoy the cheap games at home",
                "filename" =>  'sale3',
                "started_at" => '2021-01-05 00:00:00',
                "ended_at" => '2021-01-09 00:00:00',
            ],
        ];

        foreach ($list_sales as $list_sale) {
            // insert sale
            $sale = new Sale();
            $sale->title = $list_sale['title'];
            $sale->description = $list_sale['description'];

            $source = '/template/sales/' . $list_sale["filename"] . ".jpg";
            $destination = uniqid() . '.jpg';
            Storage::copy($source, '/public/photos/' . $destination);
            $sale->filename = $destination;

            $sale->started_at = $list_sale['started_at'];
            $sale->ended_at = $list_sale['ended_at'];

            $sale->save();

            // notification
            foreach (Developer::all() as $developer) {
                $sale->accounts()->attach($developer->account->id, ["message" => "$sale->title is now open for register, register your game now!", "status" => 0]);
            }

            // sale games
            Game::where('status', 1)->inRandomOrder()->take(rand(30,50))->get()
            ->each(function($game)use($sale){
                $sale->games()->attach($game->id, [
                    "discount" => rand(10, 30)
                ]);
            });
        }
    }
}
