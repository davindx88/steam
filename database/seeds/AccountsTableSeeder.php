<?php

use App\Account;
use App\Developer;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        factory(Account::class, 50)->
        create()->each(function ($account, $idx) use ($faker) {
            // Seeder Image Profile
            $id=$idx % 71;
            $source = '/template/orang/'.$id.".jpg";
            $destination = 'profile/'.uniqid().'.jpg';
            Storage::copy($source, '/public/'.$destination);
            $account->filename = $destination;
            $account->save();


            if ($account->type !== 1) // 1 itu developer
                return;
            // Insert Developer
            // TODO developer name faker (mugkin bisa dibagusin :)
            // TODO description & background faker
            $developer = new Developer();
            $developer->account_id = $account->id;
            $developer->name = $faker->unique()->company;
            $developer->slug_url = Str::slug($developer->name);
            $developer->description = $faker->paragraph();
            $developer->background_filename = '';
            $developer->save();
        });


        // admin (admin, admin)
        $account = new Account();
        $account->username = 'admin';
        $account->email = 'admin@gmail.com';
        $account->password = Hash::make('admin');
        $account->first_name = 'admin';
        $account->last_name = 'admin';
        $account->balance = 0;
        $account->filename = '';
        $account->type = 2;
        $account->token = Str::random(100);
        $account->verified_at = Carbon::now();
        $account->save();
    }
}
