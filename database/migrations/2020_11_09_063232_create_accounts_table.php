<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('username', 50);
            $table->string('email');
            $table->string('password');
            $table->string('first_name', 20)->default(''); //Hanya diisi untuk Gamer & Admin
            $table->string('last_name', 40)->default(''); //Hanya diisi untuk Gamer & Admin
            $table->integer('balance');
            $table->string('filename'); // Profile Picture User
            $table->integer('type'); // 0 Gamer 1 Developer 2 Admin
            $table->string('token', 256);
            $table->timestamp('verified_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
