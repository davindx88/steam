<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->id();
            $table->integer('account_id');
            $table->integer('friend_id');
            $table->integer('htransaction_id'); // Giftnya reference htransaction
            $table->string('message');
            $table->integer('status'); // 0 Pending 1 Diterima -1 Ditolak -2 Kenak pengecekan? mboh
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
