<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->integer('developer_id'); // bukan dari table developer, tapi dari account type = 1 (dev)
            $table->string('name', 50);
            $table->integer('price');
            $table->string('description',512);
            $table->string('thumbnail_filename', 30);
            $table->string('background_filename', 30);
            $table->string('slug_url');
            $table->string('game_filename');
            $table->integer('views');
            $table->string('message_admin')->nullable();
            $table->integer('status'); // -1 RejectAdmin 0 pending 1 AdminConfirmed
            $table->timestamp('released_at')->nullable(); // Diisi oleh admin ketika game diacc
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
