<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use App\Htransaction;
use Faker\Generator as Faker;

$factory->define(Htransaction::class, function (Faker $faker) {
    return [
        'account_id' => Account::where('type',0)->inRandomOrder()->first(),
        'type' => $faker->numberBetween(0,1),
        'total' => 0,
        'created_at' => $faker->dateTimeBetween('-1 years' ,'now')
    ];
});
