<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use App\Game;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Game::class, function (Faker $faker) {
    // TODO faker game name
    $game_name = $faker->unique()->domainName;
    // Alasan ditolak admin
    $list_alasan = [
        'Game mengandung virus!',
        'Game banyak bugnya!',
        'Game melanggar hak cipta!',
        'Game tidak dapat dimainkan!',
        'Game bukan game!',
        'Game mengandung terlalu banyak ads!',
        'Ada deh :)',
    ];
    // TODO faker name, description, thumbnail, background, game_filename
    return [
        //
        'developer_id' => Account::where('type',1)->inRandomOrder()->first()->developer->id,
        'name' => $game_name,
        'price' => $faker->numberBetween(0, 40) * 12500,
        'description' => $faker->paragraph(),
        'thumbnail_filename' => '',
        'background_filename' => '',
        'slug_url' => Str::slug($game_name),
        'game_filename' => '',
        'views' => 0,
        'message_admin' => $list_alasan[$faker->numberBetween(0, count($list_alasan)-1)],
        'status' =>$faker->numberBetween(0,10) < 3 ? -1 : ($faker->numberBetween(0,5) < 3 ? 0 : 1),
    ];
});
