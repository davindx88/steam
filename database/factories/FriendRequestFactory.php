<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use App\FriendRequest;
use Faker\Generator as Faker;

$factory->define(FriendRequest::class, function (Faker $faker) {
    $account1 = Account::inRandomOrder()->where('type',0)->first();
    $account2 = Account::inRandomOrder()->where('type',0)->where('id','<>',$account1->id)->first();
    return [
        'account_id' => $account1->id,
        'friend_id' => $account2->id,
        'message' => $faker->sentence,
        'status' => $faker->numberBetween(-1,1),
        'created_at' => $faker->dateTimeBetween('-1 years' ,'now')
    ];
});
