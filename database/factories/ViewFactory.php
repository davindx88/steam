<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use App\Game;
use App\View;
use Faker\Generator as Faker;

$factory->define(View::class, function (Faker $faker) {
    return [
        'game_id' => Game::where('status',1)->inRandomOrder()->first()->id,
        'account_id' => Account::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTimeBetween('-1 years' ,'now')
    ];
});
