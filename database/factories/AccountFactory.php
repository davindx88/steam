<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

$factory->define(Account::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('asd'),
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'balance' => 0,
        'filename' => '',
        'type' => $faker->numberBetween(0,2),
        'token' => Str::random(100),
        'verified_at' => now()
    ];
});
