<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use App\Topup;
use Faker\Generator as Faker;

$factory->define(Topup::class, function (Faker $faker) {
    // List topup yg ad di web
    $list_topup = collect([
        45000, 145000, 245000, 345000, 445000, 545000, 645000
    ]);
    return [
        'account_id' => Account::where('type',0)->inRandomOrder()->first()->id,
        'nominal' => $list_topup[$faker->numberBetween(0, $list_topup->count()-1)],
        'created_at' => $faker->dateTimeBetween('-1 years' ,'now')
    ];
});
