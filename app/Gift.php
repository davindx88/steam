<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gift extends Model
{
    protected $table = 'gifts';
    use SoftDeletes;
    public function sender()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }
    public function receiver()
    {
        return $this->belongsTo(Account::class, 'friend_id');
    }
    public function transaction()
    {
        return $this->belongsTo(Htransaction::class, 'htransaction_id');
    }
}
