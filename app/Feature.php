<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model
{
    //
    protected $table = 'features';
    use SoftDeletes;
    public function game(){
        return $this->belongsTo(Game::class, 'game_id');
    }
}
