<?php

namespace App\Rules;

use App\Account;
use App\Game;
use Illuminate\Contracts\Validation\Rule;

class CartGamesNotOwnedByFriend implements Rule
{
    private $game;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $friend = Account::where('username',$value)->first();
        $cart = session('cart');
        foreach ($cart as $game_slug => $qty) {
            $game = Game::where('slug_url', $game_slug)->first();
            if($friend->games()->where('id',$game->id)->first() !== null){
                $this->game = $game;
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Friend already has  '.$this->game->name.' game';
    }
}
