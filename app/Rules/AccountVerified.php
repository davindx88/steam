<?php

namespace App\Rules;

use App\Account;
use Illuminate\Contracts\Validation\Rule;

class AccountVerified implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $account = Account::where('username', $value)->first();
        return $account->verified_at !== null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        // Jika account belum verified maka diasumsikan tidak bisa dicari
        return 'Username not found!';
    }
}
