<?php

namespace App\Rules;

use App\Account;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class FriendIsUserFriend implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = Auth::user();
        $user = Account::find($user->id);
        $friend = Account::where('username',$value)->first();
        // Check if Friend is User's Friend
        $list_friend = $user->chatrooms()
        ->where('status',0)
        ->get()
        ->map(function($room)use($user){
            return $room->friend($user->id);
        });
        foreach ($list_friend as $key => $userFriend) {
            if($userFriend->id === $friend->id){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Friend is not active user\'s friend';
    }
}
