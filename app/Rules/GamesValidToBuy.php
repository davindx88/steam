<?php

namespace App\Rules;

use App\Account;
use App\Game;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class GamesValidToBuy implements Rule
{
    private $error = '';
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Get User
        $user = Auth::check() ? Account::find(Auth::user()->id) : null;
        // Error
        $this->error = '';
        $game = Game::where('slug_url', $value)->first();
        if($game == null || $game->status != 1){
            $this->error = 'Cart Doesn\'t valid';
        }else if($user !== null && $user->games()->where('id',$game->id)->first() !== null){
            $this->error = 'Game "'.$game->name.'" already owned!';
        }
        return $this->error === '';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        alert()->error('Error', $this->error);
        return $this->error;
    }
}
