<?php

namespace App\Rules;

use App\Account;
use Illuminate\Contracts\Validation\Rule;

class AccountIsGamer implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $account = Account::where('username', $value)->first();
        return $account->type === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        // Diasumsikan jika yang diadd developer maka not found
        return 'Username not found!';
    }
}
