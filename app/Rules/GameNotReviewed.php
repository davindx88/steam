<?php

namespace App\Rules;

use App\Account;
use App\Game;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class GameNotReviewed implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $game_slug)
    {
        //
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Get Game
        $game = Game::where('slug_url', $game_slug)->first();
        return $user->reviews()->where('id', $game->id)->first() === null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Game already reviewed!';
    }
}
