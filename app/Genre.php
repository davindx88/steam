<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    //
    protected $table = "genres";
    use SoftDeletes;

    public function games(){
        return $this->belongsToMany(Game::class, 'gamegenres', 'genre_id', 'game_id');
    }
}
