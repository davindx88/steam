<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Developer extends Model
{
    //
    protected $table = "developers";
    use SoftDeletes;
    // Relation
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }
}
