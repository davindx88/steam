<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Htransaction extends Model
{
    // account_id, total
    protected $table = 'htransactions';
    use SoftDeletes;
    public function games()
    {
        return $this->belongsToMany('App\Game', 'dtransactions', 'htransaction_id', 'game_id')->withPivot('price');
    }
    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id', 'id');
    }
}
