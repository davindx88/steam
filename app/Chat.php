<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    //
    protected $table = 'chats';
    use SoftDeletes;

    public function chatroom(){
        return $this->belongsTo(ChatRoom::class, 'chatroom_id');
    }

    public function sender(){
        return $this->belongsTo(Account::class, 'sender_id');
    }
}
