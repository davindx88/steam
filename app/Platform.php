<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Platform extends Model
{
    //
    protected $table = 'platforms';
    use SoftDeletes;
    public function games()
    {
        return $this->belongsToMany('App\Game', 'GamePlatforms', 'platform_id', 'game_id');
    }
}
