<?php

namespace App\Http\Middleware;

use App\Account;
use App\Game;
use Closure;
use Illuminate\Support\Facades\Auth;

class CartGamesValidMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $terminate_invalid=false)
    {
        $terminate_invalid = $terminate_invalid === 'true';
        // Get User
        $user = Auth::check() ? Account::find(Auth::user()->id) : null;
        // Get Cart
        $real_cart = session('cart');
        $cart = collect($real_cart);
        // Error
        $error = '';
        foreach ($cart as $game_slug => $item) {
            $game = Game::where('slug_url', $game_slug)->first();
            if($game == null){
                // Exist
                unset($real_cart[$game_slug]);
                $error = 'Cart Doesn\'t valid';
            }else if($game->status != 1){
                // Available
                unset($real_cart[$game_slug]);
                $error = 'Cart Doesn\'t valid';
            }else if($user !== null && $user->games()->where('id',$game->id)->first() !== null){
                // Owned
                unset($real_cart[$game_slug]);
                $error = 'Game "'.$game->name.'" already owned!';
            }
        }
        // Update Cart
        session()->put('cart', $real_cart);
        // Redir
        if($error === '' || $terminate_invalid === true){
            // Valid
            return $next($request);
        }else{
            // Tidak Valid
            alert()->error('Error', $error);
            return back();
        }
    }
}
