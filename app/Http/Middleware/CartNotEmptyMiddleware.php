<?php

namespace App\Http\Middleware;

use Closure;

class CartNotEmptyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cart = collect(session('cart',[]));
        if($cart->count() == 0){
            alert()->warning('Warning','Cart is empty!');
            return back();
        }else{
            return $next($request);
        }
    }
}
