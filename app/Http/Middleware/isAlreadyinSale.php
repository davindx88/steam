<?php

namespace App\Http\Middleware;

use App\Game;
use Closure;

class isAlreadyinSale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Route Parameter
        $game_id = $request->route()->parameters('game_id');
        // Get Active User
        // $game = Account::find(Auth::user()->id);
        // Get Game
        $game = Game::find($game_id);
        if($game == null){ // jika game tidak ditemukan
            return abort(404);
        }
        // Check apakah user punya gamenya atau tidak
        $sale = $game->sales()->where('id', $game->id)->first();
        if($sale == null){
            return abort(404);
        }else{ // Jika Valid
            return $next($request);
        }
    }
}
