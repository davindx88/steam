<?php

namespace App\Http\Middleware;

use App\Account;
use App\Game;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserBalanceEnoughMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get User
        $user = Account::find(Auth::user()->id);
        // Get Cart
        $real_cart = session('cart');
        $cart = collect($real_cart);
        $total = $cart->map(function($qty, $game_slug){
            return Game::where('slug_url', $game_slug)->first();
        })->reduce(function($carry, $game){
            return $carry + $game->discprice();
        }, 0);
        if($user->balance >= $total){
            return $next($request);
        }else{
            return back()->withErrors([
                'msg' => 'User\'s balance not enough'
            ]);
        }
    }
}
