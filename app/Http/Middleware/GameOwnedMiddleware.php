<?php

namespace App\Http\Middleware;

use App\Account;
use App\Game;
use Closure;
use Illuminate\Support\Facades\Auth;

class GameOwnedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Route Parameter
        $game_slug = $request->route()->parameters('game_slug');
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Get Game
        $game = Game::where('slug_url', $game_slug)->first();
        if($game == null){ // jika game tidak ditemukan
            return abort(404);
        }
        // Check apakah user punya gamenya atau tidak
        $userGame = $user->games()->where('id', $game->id)->first();
        if($userGame == null){
            return abort(404);
        }else{ // Jika Valid
            return $next($request);
        }
    }
}
