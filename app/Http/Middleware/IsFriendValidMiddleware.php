<?php

namespace App\Http\Middleware;

use App\Account;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsFriendValidMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Cek apakah friend sudah friend dengan active user
        // Ambil Parameter dn Get Friend
        $username = $request->route()->parameter('username');
        // Get Active User
        $user = Auth::user();
        $user = Account::find($user->id);
        // Get Friends
        foreach ($user->friends() as $friend)
            if($friend->username == $username) // Valid redir
                return $next($request);
        // Not found
        return abort(404);
    }
}
