<?php

namespace App\Http\Middleware;

use App\Account;
use Closure;

class IsUsernameValidMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Username
        $username = $request->route()->parameter('username');
        // Pengecekan apakah username exist dan verified
        $user = Account::where('username', $username)
        ->where('type', 0)
        ->where('verified_at','<>','')
        ->first();
        if($user === null){
            // Jika User tidak valid
            return abort(404);
        }else{
            // Jika User Valid
            return $next($request);
        }
    }
}
