<?php

namespace App\Http\Middleware;

use App\Account;
use App\Gift;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsGiftValidMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Parameter
        $gift_id = $request->gift_id;
        $response = $request->response;
        if($gift_id == null || $response == null)
            return back()->withErrors([
                'msg' => "Nice parameters:)"
            ]);
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Pengecekan Apakah Gift Exist
        $gift = Gift::find($gift_id);
        if($gift === null)
            return back()->withErrors([
                'msg' => "Gift doesn't exist!"
            ]);
        // Pengecekan Apakah Gift Milik User
        if($gift->friend_id != $user->id)
            return back()->withErrors([
                'msg' => 'Gift not owned by you!'
            ]);
        // Pengecekan Gift Status 0
        if($gift->status != 0)
            return back()->withErrors([
                'msg' => 'Gift already responded!'
            ]);
        // Pengecekan Game tidak dimiliki User
        if($response == 0) // jika accept
            foreach ($gift->transaction->games as $key => $game)
                if($user->games()->where('id', $game->id)->first() != null){
                    // Jika punya gamenya maka return false
                    return back()->withErrors([
                        'msg' => 'User already own "'.$game->name.'" game'
                    ]);
                }
        return $next($request);
    }
}
