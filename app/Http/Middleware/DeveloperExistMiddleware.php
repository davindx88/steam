<?php

namespace App\Http\Middleware;

use App\Developer;
use Closure;

class DeveloperExistMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Username
        $developer_slug = $request->route()->parameter('developer_slug');
        $developer = Developer::where('slug_url', $developer_slug)->first();
        if($developer !== null){
            return $next($request);
        }else{
            return redirect('/developers');
        }
    }
}
