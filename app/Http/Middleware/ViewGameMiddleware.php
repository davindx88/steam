<?php

namespace App\Http\Middleware;

use App\Account;
use App\Game;
use App\View;
use Closure;
use Illuminate\Support\Facades\Auth;

class ViewGameMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Route Params
        $game_slug = $request->route()->parameter('game_slug');
        $game = Game::where('slug_url', $game_slug)->first();
        // Get Active User
        if(Auth::check()){
            $user = Account::find(Auth::user()->id);
            // Insert table View
            $view = new View();
            $view->game_id = $game->id;
            $view->account_id = $user->id;
            $view->save();
            // Updaet
            $game->views += 1;
            $game->save();
        }
        return $next($request);
    }
}
