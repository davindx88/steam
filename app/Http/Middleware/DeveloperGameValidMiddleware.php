<?php

namespace App\Http\Middleware;

use App\Developer;
use Closure;

class DeveloperGameValidMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Route Param
        $developer_slug = $request->route()->parameter('developer_slug');
        $game_slug = $request->route()->parameter('game_slug');
        $developer = Developer::where('slug_url', $developer_slug)->first();
        $game = $developer->account->developedGames()
        ->where('slug_url', $game_slug)
        ->where('status', 1)
        ->first();
        if($game !== null)
            return $next($request);
        else
            return redirect('/developers/'.$developer->slug_url);
    }
}
