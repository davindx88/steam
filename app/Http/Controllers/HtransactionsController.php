<?php

namespace App\Http\Controllers;

use App\Account;
use App\Game;
use App\Gift;
use App\Htransaction;
use App\Rules\CartGamesNotOwnedByFriend;
use App\Rules\FriendIsUserFriend;
use App\Rules\UsernameExist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class HtransactionsController extends Controller
{
    public function makeTransaction(Request $request){
        $validate = $request->validate([
            'purchase_type' => [
                'required',
                Rule::in(['0', '1'])
            ]
        ]);
        // Get Param
        $type = $request->purchase_type;
        // Get Cart (list_of_game)
        $cart = session('cart');
        $cart = collect($cart)->map(function($item, $game_slug){
            return Game::where('slug_url', $game_slug)->first();
        });
        if($type == 1){
            // Gift ada validasinya lagi
            $validate = $request->validate([
                'friend' => 'required',
                'message' => 'required',
            ]);
            $validate = $request->validate([
                'friend' =>  new UsernameExist
            ]);
            $validate = $request->validate([
                'friend' =>  new FriendIsUserFriend
            ]);
            $validate = $request->validate([
                'friend' =>  new CartGamesNotOwnedByFriend
            ]);
            // Get Param
            $username = $request->friend;
            $friend = Account::where('username', $username)->first();
            $message = $request->message;
        }
        // Get Active User
        $user = Auth::user();
        $user = Account::find($user->id);
        // Htransaction Insert
        $htrans = new Htransaction();
        $htrans->account_id = $user->id;
        $htrans->type = $type; // 0 buat diri sendiri, 1 buat gift
        $htrans->total = 0;
        $htrans->save();
        // Dtransactions Insert
        // TODO insert harga sales
        $cart->each(function($game)use($htrans){
            // Price
            $price = $game->discprice();
            // Insert DTRANS
            $htrans->games()->attach($game->id, [
                'price' => $price,
            ]);
            // Update harga htrans
            $htrans->total += $price;
        });
        // Update total Htrans
        $htrans->save();
        // Update balance User
        $user->balance -= $htrans->total;
        $user->save();
        // Clear Session
        session()->forget('cart');
        // Action tambahan tergantung purchase type
        if($type == 0){
            $htrans->games->each(function($game)use($user){
                // Jika user maka insertkan ke table accountsgame
                $user->games()->attach($game->id, [
                    'owned_at' => Carbon::now()
                ]);
                // Developer dapet wang :D
                $developer = $game->developer;
                $developer->balance += $game->pivot->price;
                $developer->save();
            });
        }else{
            // jika gift maka insert table gift
            $gift = new Gift;
            $gift->account_id = $user->id;
            $gift->friend_id = $friend->id;
            $gift->htransaction_id = $htrans->id;
            $gift->message = $message;
            $gift->status = 0; // 0 itu pending
            // Redirect thankyou
            $gift->save();
        }
        // TODO kasih sweet alert :)
        alert()->success('Success','your order was successfully completed.');
        session()->flash('thankyoupage', 1);
        return redirect('/thankyou');
    }
}
