<?php

namespace App\Http\Controllers;

use App\Account;
use App\Chat;
use App\ChatRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatsController extends Controller
{
    public function insert(Request $request){
        $validate = $request->validate([
            'room' => 'required',
            'message' => 'required'
        ]);
        // Get Parameters
        $room_id = $request->room;
        $message = $request->message;
        // Get User
        $user = Account::find(Auth::user()->id);
        // Get Chatroom
        $room = ChatRoom::find($room_id);
        // Insert
        $chat = new Chat();
        $chat->chatroom_id = $room->id;
        $chat->sender_id = $user->id;
        $chat->message = $message;
        $chat->save();
        // Return
        $data = [
            'id' => $chat->id,
            'message' => $message,
            'created_at' => $chat->created_at,
            'is_sender' => true
        ];
        return response()->json($data, 200);
    }
    public function get(Request $request){
        $validate = $request->validate([
            'room' => 'required',
            'last_id' => 'required'
        ]);
        // Get Parameters
        $room_id = $request->room;
        $last_id = $request->last_id;
        // Get User
        $user = Account::find(Auth::user()->id);
        // Get Chat
        $new_chat = Chat::where('chatroom_id', $room_id)
        ->where('id','>', $last_id)
        ->orderby('id', 'ASC')
        ->get()
        ->map(function($chat)use($user){
            return [
                'id' => $chat->id,
                'message' => $chat->message,
                'created_at' => $chat->created_at,
                'is_sender' => $chat->sender_id == $user->id
            ];
        });
        return response()->json($new_chat, 200);
    }
}
