<?php

namespace App\Http\Controllers;

use App\Account;
use App\Developer;
use App\Feature;
use App\Game;
use App\Genre;
use App\Htransaction;
use App\Platform;
use App\Sale;
use App\Rules\UniqueEmail;
use App\Rules\UniqueUsername;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Charts\UserChart;
use App\Gift;
use ArielMejiaDev\LarapexCharts\Facades\LarapexChart;
use ArielMejiaDev\LarapexCharts\LarapexChart as LarapexChartsLarapexChart;
use Illuminate\Support\Facades\DB;

class DevelopersController extends Controller
{
    public function editprofile(Request $request)
    {
        $user = Account::find(Auth::user()->id);
        $developer= Developer::where('account_id', $user->id)->first();
        $data=[$user,$developer];
        return view('developer.dashboard_edit_profile')->with('data',$data);
    }
    public function isEditProfile(Request $request)
    {
        $name ='profile/'. uniqid().".".$request->file("image")->getClientOriginalExtension();
        $account = Account::where("id", "=", $request->iduser) ->update(
            array(
                "filename" => $name,
            )
        );
        if($request->filename != ''){
            Storage::delete($request->filename);
        }
        $request->file("image")->storeAs("public/", $name);
        //$request->image->move(public_path('Photos'), $name);
        return back();
    }
    public function profile()
    {
        $user = Account::find(Auth::user()->id);
        $developer= Developer::where('account_id',$user->id)
                              ->first();
        $games=Game::where('developer_id',$developer->id)->get();
        $data=[$user,$developer,$games];
        return view('developer.dashboard_profile')->with('data',$data);
    }
    // Page
    public function developerLogin()
    {
        return view('developer.dahsboard_login');
    }
    public function login(Request $request)
    {
        $validate = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        // Ambil parameter
        // Username bisa jadi Email
        $username = $request->username;
        $password = $request->password;
        // data login
        $data_1 = ['username' => $username, 'password' => $password];
        $data_2 = ['email' => $username, 'password' => $password];
        // Coba login pake username / email
        if(Auth::attempt($data_1) || Auth::attempt($data_2)){
            $account = Auth::user();
            // Jika ketemu user
            if($account->verified_at === null){
                // Jika belum di verifikasi
                $request->flash();
                Auth::logout();
                return back()->withErrors([
                    'msg' => 'Account not verified yet'
                ]);
            }else {
                // Session disimpan di Active
                if($account->type === 0){
                    // User
                    return redirect('/');
                }else if($account->type === 1){
                    // Developer
                    return redirect('/dashboard');
                }else if($account->type === 2){
                    // Admin
                    return redirect('/admin');
                }
            }
        }else{
            // Jika login gagal
            $request->flash();
            return back()->withErrors([
                'msg' => 'Invalid username/email or password'
            ]);
        }
    }
    public function developerRegister()
    {
        return view('developer.dashboard_register');
    }
    public function register(Request $request)
    {

        $validate = $request->validate([
            'name' => ['required', 'alpha_num', 'min:8'],
            'username' => ['required', 'alpha_num', 'min:8', new UniqueUsername],
            'email' => ['required',' email:rfc,dns', new UniqueEmail],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            // 'terms' => 'required'
        ]);
        if ($request->hasFile('image')) {
            $name = uniqid().".".$request->file("image")->getClientOriginalExtension();
            $account=new Account();
            $account->username=$request->username;
            $account->email=$request->email;
            $account->password=Hash::make($request->password);
            $account->type=1;
            $account->balance=0;
            $account->filename = $name;
            $account->token = Str::random(100);
            $account->save();
            $developer=new Developer();
            $developer->name=$request->name;
            $developer->account_id=$account->id;
            $developer->slug_url=Str::slug($request->name);
            $developer->description=$request->description;
            $developer->background_filename=$name;
            $developer->save();
            //$request->image->move(public_path('Photos/images'), $name);
            $request->file("image")->storeAs("/public/photos", $name);
        } else {
            // TODO Validation with Error
            return "tidak ada foto";
        }

        return redirect('/logindeveloper');
    }
    public function addGame(Request $request)
    {
        $user = Account::find(Auth::user()->id);
        $developer= Developer::where('account_id',$user->id)
                              ->first();
        $request->validate([
            "name" => "required",
            "price" => "required",
            "description" => "required"
        ]);

        if ($request->hasFile("imageGame")) {
            //game
            $game=new Game();
            $game->name=$request->name;
            $game->price=$request->price;
            $game->description=$request->description;
            $extension = $request->file('imageGame')->getClientOriginalExtension();
            $game->thumbnail_filename = 'thumbnail/'.uniqid() . "." . $extension;
            $extension = $request->file('backgroundImage')->getClientOriginalExtension();
            $game->background_filename = 'thumbnail/'.uniqid() . "." . $extension;
            $extension = $request->file('fileData')->getClientOriginalExtension();
            $game->game_filename = 'thumbnail/'.uniqid() . "." . $extension;
            $game->views=0;
            $game->status=0;
            $game->slug_url=Str::slug($game->name, '-');
            $game->developer_id=$developer->id;
            $game->save();
           //$request->imageGame->move(public_path('Photos'), $game->thumbnail_filename);
           //$request->backgroundImage->move(public_path('Photos'), $game->background_filename);
           //$request->fileData->move(public_path('Photos'), $game->game_filename);
           //$request->file("imagegame")->storeAs("/public/photos", $game->thumbnail_filename);
           $request->file("imageGame")->storeAs("/public/",  $game->thumbnail_filename);
           $request->file("backgroundImage")->storeAs("/public/", $game->background_filename);
           $request->file("fileData")->storeAs("/public/",$game->game_filename);
           //genre
            for ($i=0; $i < sizeof($request->genre); $i++) {
                $genre= Genre::find($request->genre[$i]);
                $genre->games()->attach($game->id);
            }
            //platform
            for ($i=0; $i < sizeof($request->platform); $i++) {
                $platform= Platform::find($request->platform[$i]);
                $platform->games()->attach($game->id);
            }
            //feature
            for ($i=0; $i < sizeof($request->image); $i++) {
                $feature= new Feature();
                $feature->game_id=$game->id;
                $feature->name=$request->featuresname[$i];
                $feature->description=$request->featuredescription[$i];
                $feature->image_filename = uniqid() . ".jpg";
                $request->image[$i]->storeAs('public/photos',$feature->image_filename);
                $feature->save();
            }
            return redirect('/dashboard/games');
        }
    }
    public function developerPage(Request $request){
        // Query String
        $u = $request->query('u','');
        // Get User URL
        $list_developer = Developer::where('name', 'like', '%'.$u.'%')->get();
        return view('developer.index',[
            'user' => Auth::user(),
            'list_developer' => $list_developer
        ]);
    }
    // /developers/developer_slug
    public function developerDetailPage($developer_slug)
    {
        $developer = Developer::where('slug_url', $developer_slug)->first();
        // dd($developer);
        return view('developer.detail_developer', [
            'developer' => $developer,
            'user' => Auth::user()
        ]);
    }
    // /developers/developer_slug/gameslug
    public function developerGamePage($developer_slug, $game_slug)
    {
        // Get Detail Game
        $game = Game::where('slug_url', $game_slug)->first();
        // Get Get Auth User
        $user = null;
        $hasGame = false;
        $isGameReviewed = false;
        if(Auth::check()){
            $user = Account::find(Auth::user()->id);
            // has Game
            $hasGame = $user->games()->where('id', $game->id)->first() !== null;
            // Game Reviewd
            $isGameReviewed = $user->reviews()->where('id', $game->id)->first() !== null;
        }
        return view('developer.detail_game', [
            'game' => $game,
            'user' => $user,
            'hasGame' => $hasGame,
            'isGameReviewed' => $isGameReviewed
        ]);
    }
    public function detailGamePage($slugname)
    {
        // Get Detail Game
        $game = Game::where('slug_url', $slugname)->first();
        $developer= Developer::find($game->developer_id);
        $feature=Feature::where('game_id',$game->id)->get();
        $allgame=Game::where('developer_id',$developer->id)
        ->whereNotIn('id', [$game->id])->limit(4)->get();
        $data=[$game,$developer,$feature,$allgame];
        return view('developer.dashboard_detail_game')->with('data',$data);
    }
    public function dashboardAddGame(Request $request)
    {
        $platform=Platform::all();
        $genre=Genre::all();
        $data=[$platform,$genre];
        return view('developer.dashboard_game_add')->with('data',$data);
    }
    public function dashboardPage(){
        for ($i=0; $i < 12; $i++) {
            $temp[$i]=0;
        }
        $user = Account::find(Auth::user()->id);
        $developer= Developer::where('account_id',$user->id)
                              ->first();
         $htransaction=Htransaction::all();
        $jan = DB::table('htransactions')
        ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
         ->join('games','dtransactions.game_id','=','games.id')
         ->join('developers','games.developer_id','=','developers.id')
         ->whereMonth('htransactions.created_at', '01')
         ->where('developers.id','=',$developer->id)
         ->distinct()
         ->get();

         $feb = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '02')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $march = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '03')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $apr = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '04')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $may = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '05')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $jun = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '06')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $jul = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '07')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $aug = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '08')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $sep = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '09')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $oct = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '10')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $nov = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '11')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         $dec = DB::table('htransactions')
         ->join('dtransactions','htransactions.id','=','dtransactions.htransaction_id')
          ->join('games','dtransactions.game_id','=','games.id')
          ->join('developers','games.developer_id','=','developers.id')
          ->whereMonth('htransactions.created_at', '12')
          ->where('developers.id','=',$developer->id)
          ->distinct()
          ->get();
         foreach ($jan as $key => $value) {
            $temp[0]+=$value->total;
        }
        foreach ($feb as $key => $value) {
           $temp[1]+=$value->total;
       }
       foreach ($march as $key => $value) {
           $temp[2]+=$value->total;
       }
       foreach ($apr as $key => $value) {
           $temp[3]+=$value->total;
       }
       foreach ($may as $key => $value) {
           $temp[4]+=$value->total;
       }
       foreach ($jun as $key => $value) {
           $temp[5]+=$value->total;
       }
       foreach ($jul as $key => $value) {
           $temp[6]+=$value->total;
       }
       foreach ($aug as $key => $value) {
           $temp[7]+=$value->total;
       }
       foreach ($sep as $key => $value) {
           $temp[8]+=$value->total;
       }
       foreach ($oct as $key => $value) {
           $temp[9]+=$value->total;
       }
       foreach ($nov as $key => $value) {
           $temp[10]+=$value->total;
       }
       foreach ($dec as $key => $value) {
           $temp[11]+=$value->total;
       }
        // $data=DB::select(`*`)
		// ->from(`htransaction as h`)
		// ->join(`gift as g`, function($join) {
		// 	$join->on(`g.htransaction_id`, `=`, `h.id`);
		// 	})
		// ->where(`type`, `=`, 0)
		// ->where(`g.status`, `=`, 1, `or`)
        // ->get();
        $chart = (new LarapexChartsLarapexChart)->setType('area')
        ->setTitle('Total Money Gained Monthly')
        ->setSubtitle('From January to December')
        ->setXAxis([
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
        ])
        ->setDataset([
            [
                'name'  =>  'Total Games Sold',
                'data'  =>  [ $temp[0],  $temp[1],  $temp[2], $temp[3] ,$temp[4] ,$temp[5],$temp[6],$temp[7],$temp[8],$temp[9],$temp[10],$temp[11]]
            ]
        ]);
        return view('developer.dahsboard', [ 'usersChart' => $chart ]);
    }
    public function dashboardInbox(){
        $sales=Sale::all();
        return view('developer.dashboard_inbox')->with('sales',$sales);
    }
    public function dashboardDetailInbox($id){
        $sale=Sale::find($id);
        return view('developer.dashboard_detail_inbox')->with('sale',$sale);
    }
    public function dashboardSales(){
        $sales=Sale::all();
        return view('developer.dashboard_sales')->with('data',$sales);
    }
    public function dashboardDetailSales($id){
        $sales=Sale::find($id);
        return view('developer.dashboard_detail_sales')->with('sales',$sales);

    }
    public function dashboardJoinSales($id){
        $sales=Sale::find($id);
        $games=Game::all();
        $data=[$sales,$games];
        return view('developer.dashboard_join_sale')->with('data',$data);
    }
    public function addsales(Request $request)
    {
        $sales=Sale::find($request->salesid)->first();
        $games=Game::find($request->gamesid)->first();
        $sales->games()->attach($games->id,['discount'=>$request->discount]);
        return redirect("dashboard");
    }
    public function dashboardAllGames()
    {
        $user = Account::find(Auth::user()->id);
        $developer= Developer::where('account_id',$user->id)
                              ->first();
        $games=Game::where('developer_id',$developer->id)->get();
        return view('developer.dashboard_game')->with('games',$games);
    }

    public function dashboardEarning()
    {
        $htransaction=Htransaction::all();

        return view('welcome');
    }
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        //
    }
    public function editgame(Request $request)
    {
        $game = Game::where("id", "=", $request->idgame) ->update(
            array(
                "name" => $request->name,
                "description" => $request->description,
                "price" => $request->price,
            )
        );
        return redirect('/dashboard');
    }
    public function edit($id)
    {
        $games=Game::find($id);
        $platform=Platform::all();
        $feature=Feature::all();
        $genre=Genre::all();
        $data=[$platform,$genre,$games,$feature];
        return view('developer.dashboard_edit_game')->with('data',$data);
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
