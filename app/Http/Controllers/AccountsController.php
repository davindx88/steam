<?php

namespace App\Http\Controllers;

use App\Account;
use App\Developer;
use App\Game;
use App\Notifications\AccountRegistered;
use App\Rules\GamesValidToBuy;
use App\Rules\UniqueEmail;
use App\Rules\UniqueUsername;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AccountsController extends Controller
{
    public function login(Request $request){
        $validate = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        // Ambil parameter
        // Username bisa jadi Email
        $username = $request->username;
        $password = $request->password;
        // data login
        $data_1 = ['username' => $username, 'password' => $password];
        $data_2 = ['email' => $username, 'password' => $password];
        // Coba login pake username / email
        if(Auth::attempt($data_1) || Auth::attempt($data_2)){
            $account = Auth::user();
            // Jika ketemu user
            if($account->verified_at === null){
                // Jika belum di verifikasi
                $request->flash();
                Auth::logout();
                return back()->withErrors([
                    'msg' => 'Account not verified yet'
                ]);
            }else {
                // Session disimpan di Active
                if($account->type === 0){
                    // User
                    return redirect('/');
                }else if($account->type === 1){
                    // Developer
                    return redirect('/dashboard');
                }else if($account->type === 2){
                    // Admin
                    return redirect('/admin');
                }
            }
        }else{
            // Jika login gagal
            $request->flash();
            return back()->withErrors([
                'msg' => 'Invalid username/email or password'
            ]);
        }
    }
    public function register(Request $request){
        // dd($request->all());
        $validate = $request->validate([
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'username' => ['required', 'alpha_num', 'min:8', new UniqueUsername],
            'email' => ['required',' email:rfc,dns', new UniqueEmail],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            // 'terms' => 'required'
        ]);
        // Insert Account
        $account = new Account;
        $account->username = $request->username;
        $account->email = $request->email;
        $account->password = Hash::make($request->password);
        $account->first_name = $request->first_name;
        $account->last_name = $request->last_name;
        $account->balance = 0;
        $source = '/template/profile/iconprofile.jpg';
        $destination = 'profile/iconprofile.jpg';
        Storage::copy($source, '/public/'.$destination);
        $account->filename = $destination;
        $account->save();
        $account->type = 0;
        $account->token = Str::random(100);
        $account->save();
        // Kirim Email Notification
        $account->notify(new AccountRegistered);
        $account->save();
        // Redirect
        return redirect('/login');
    }
    public function editprofile(Request $request)
    {
        if ($request->hasFile('picture')) {
            // dd($request->file("picture")->getClientOriginalExtension());
            $nama = uniqid().".".$request->file("picture")->getClientOriginalExtension();
            $account = Account::where("id", "=", $request->iduser) ->update(
                array(
                    "first_name" => $request->firstname,
                    "last_name" => $request->lastname,
                    "filename" => $nama,
                )
            );
            if($request->filename != ''){
                Storage::delete($request->filename);
            }
            $request->file("picture")->storeAs("/public/photos/", $nama);
            return redirect('/users');
        } else {
            // TODO Validation with Error
            return "tidak ada foto";
        }

    }
    public function logout(){
        // Hapus session (seperti cart)
        session()->flush();
        // Logout
        Auth::logout();
        // Redirect kembali ke index
        return redirect('/');
    }

    // Cart
    public function addGameCart(Request $request){
        $validate = $request->validate([
            'game_slug' => ['required', new GamesValidToBuy]
        ]);
        // Get Parameter
        $game_slug = $request->game_slug;
        // Tambah Game ke Cart
        $cart = session()->get('cart',[]);
        $cart[$game_slug] = 1;
        session()->put('cart', $cart);
        return redirect('/cart');
    }
    public function deleteGameCart(Request $request){
        // Delete game dari cart
        $validate = $request->validate([
            'game_slug' => 'required'
        ]);
        // Get Parameter
        $game_slug = $request->game_slug;
        // Tambah Game ke Cart
        $cart = session()->get('cart',[]);
        unset($cart[$game_slug]);
        session()->put('cart', $cart);
        return redirect('/cart');
    }
    // TODO update cart
    public function clearCart(Request $request){
        // Bersihkan Cart
        session()->forget('cart');
        return redirect('/cart');
    }
    // Download Game
    public function downloadGame(Request $request){
        // Delete game dari cart
        $validate = $request->validate([
            'game_slug' => 'required'
        ]);
        // Get Parameter
        $game_slug = $request->game_slug;
        $game = Game::where('slug_url', $game_slug)->first();
        return Storage::download('/public/'.$game->game_filename);
    }
    // Verif Emai;
    public function verifyAccount($code){
        $account = Account::where('token', $code)->first();
        if($account == null){
            // Jika tidak ketemu
            alert()->error('Verification code not valid!', '');
        }else{
            // Jika ketemu
            $account->verified_at = now();
            $account->save();
            alert()->success('Account succesfully verified!', 'success');
        }
        return redirect('/');
    }
}
