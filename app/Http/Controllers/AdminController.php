<?php

namespace App\Http\Controllers;

use App\Account;
use App\Developer;
use App\Feature;
use App\Game;
use App\Genre;
use App\Htransaction;
use App\Platform;
use App\Rules\UniqueEmail;
use App\Rules\UniqueUsername;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    // page
    // /
    public function index_page()
    {
        $games = Game::all();
        $accounts = Account::all();
        $developers = Developer::all();
        $genres = Genre::all();
        $platforms = Platform::all();
        $sales = Sale::all();
        $top_genre = Genre::withCount('games')->orderBy('games_count', 'desc')->take(3);
        $top_platform = Platform::withCount('games')->orderBy('games_count', 'desc')->take(3);
        $top_games = Game::withCount('gamers')->orderBy('gamers_count', 'desc')->take(5);
        $monthly_purchases_made = DB::table('htransactions')->whereBetween('created_at', [Carbon::now()->startOfMonth()->subMonth(6), Carbon::now()->lastOfMonth()])->join('dtransactions', 'id', '=', 'htransaction_id')->selectRaw('count(*) as "count", year(htransactions.created_at) as "year", month(htransactions.created_at) as "month", CONCAT(MONTHNAME(htransactions.created_at)," ",year(htransactions.created_at)) as "date"')->groupBy('year', 'month', 'date')->orderByDesc('year', 'month')->get();
        $ongoing_sale = Sale::where('started_at', "<=", Carbon::now())->where('ended_at', '>=', Carbon::now())->first();
        return view('admin.home', [
            'games' => $games,
            'accounts' => $accounts,
            'developers' => $developers,
            'genres' => $genres,
            'platforms' => $platforms,
            'sales' => $sales,
            'top_genre' => $top_genre,
            'top_platform' => $top_platform,
            'top_games' => $top_games,
            'monthly_purchases_made' => $monthly_purchases_made,
            'ongoing_sale' => $ongoing_sale,
        ]);
    }
    // /profile
    public function profile()
    {
        return view('admin.profile');
    }
    // /profile/update
    public function profile_update(Request $request)
    {
        $request->validate([
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);
        // Insert Account
        $account = Auth::user();
        $account->password = Hash::make($request->password);
        $account->first_name = $request->first_name;
        $account->last_name = $request->last_name;
        $account->save();
        alert()->success('Update profile success', 'success');
        return redirect('/admin');
    }
    // /accounts
    public function accounts_page()
    {
        // $accounts = Account::all();
        return view('admin.accounts.accounts');
    }
    public function get_accounts(Request $request)
    {
        if ($request->ajax()) {
            $data = Account::where('type', 0)->get();
            return DataTables::of($data)
                ->editColumn('name', function (Account $account) {
                    return $account->first_name . " " . $account->last_name;
                })
                ->addColumn('action', function (Account $account) {
                    return "<a href='/admin/accounts/$account->username' class='btn btn-secondary btn-sm'>Detail</a>";
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    // /accounts/{id}
    public function accounts_show_page($username)
    {
        $account = Account::where('username', $username)->first();
        return view('admin.accounts.accounts_detail', [
            "account" => $account
        ]);
    }
    //  /developers
    public function developers_page()
    {
        $developers = Developer::all();
        return view('admin.developers.developers');
    }
    public function get_developers(Request $request)
    {
        if ($request->ajax()) {
            $data = Developer::all();
            return DataTables::of($data)
                ->editColumn('id', function (Developer $developer) {
                    return $developer->id;
                })
                ->editColumn('username', function (Developer $developer) {
                    return $developer->account->username;
                })
                ->editColumn('email', function (Developer $developer) {
                    return $developer->account->email;
                })
                ->editColumn('name', function (Developer $developer) {
                    return $developer->name;
                })
                ->addColumn('action', function (Developer $developer) {
                    return "<a href='/admin/developers/$developer->slug_url' class='btn btn-secondary btn-sm'>Detail</a>";
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    // /developers/{id}
    public function developers_show_page($slug_url)
    {
        $developer = Developer::where('slug_url', $slug_url)->first();
        return view('admin.developers.developers_detail', [
            "developer" => $developer
        ]);
    }
    // /games
    public function games_released_page()
    {
        return view('admin.games.games_released');
    }
    public function games_unreleased_page()
    {
        return view('admin.games.games_unreleased');
    }
    public function get_unreleased_games(Request $request)
    {
        if ($request->ajax()) {
            $data = Game::where('status', '0')->get();
            return DataTables::of($data)
                ->editColumn('id', function (Game $game) {
                    return $game->id;
                })
                ->editColumn('image', function (Game $game) {
                    if ($game->thumbnail_filename  == "") {
                        return "<img src='" . url('/icongame.png') . "' height=70 width=50/>";
                    }
                    return "<img src='" . url('/storage/' . $game->thumbnail_filename) . "' height=80 width=60/>";
                })
                ->editColumn('name', function (Game $game) {
                    $platforms_img = '';
                    foreach ($game->platforms as $platform) {
                        if ($platform->filename != '') {
                            $platforms_img .= "<img src='" . url('/storage/photos/' . $platform->filename) . "' width=30 height=30 style='margin-right: 4px'/>";
                        } else {
                            $platforms_img .= "<img src='" . url('/icongame.png') . "' width=20 height=20 style='margin-right: 4px'/>";
                        }
                    }
                    return "<div>
                        <p>$game->name</p>
                        $platforms_img
                    </div>";
                })
                ->editColumn('price', function (Game $game) {
                    if ($game->price == 0) {
                        return "FREE";
                    }
                    return "IDR" . number_format($game->price, 2, '.', ',');
                })
                ->editColumn('description', function (Game $game) {
                    return $game->description;
                })
                ->editColumn('developer', function (Game $game) {
                    return $game->developer->developer->name;
                })
                ->editColumn('released_at', function (Game $game) {
                    return Carbon::parse($game->released_at)->format('d M, Y');
                })
                ->addColumn('action', function (Game $game) {
                    return "
                        <button onclick='accept_confirmation($game->id)' class='btn btn-success btn-sm w-100'>Accept</button>
                        <button onclick='reject_confirmation($game->id)' class='btn btn-danger btn-sm w-100'>Reject</button>
                        <a href='" . url('/admin/games/' . $game->slug_url) . "' class='btn btn-secondary btn-sm w-100'>Detail</a>
                    ";
                })
                ->rawColumns(['name', 'image', 'action'])
                ->make(true);
        }
    }
    public function get_released_games(Request $request)
    {
        if ($request->ajax()) {
            $data = Game::where('status', '1')->get();
            return DataTables::of($data)
                ->editColumn('id', function (Game $game) {
                    return $game->id;
                })
                ->editColumn('image', function (Game $game) {
                    if ($game->thumbnail_filename  == "") {
                        return "<img src='" . url('/icongame.png') . "' height=80 width=60/>";
                    }
                    return "<img src='" . url('/storage/' . $game->thumbnail_filename) . "' height=80 width=60/>";
                })
                ->editColumn('name', function (Game $game) {
                    $platforms_img = '';
                    foreach ($game->platforms as $platform) {
                        if ($platform->filename != '') {
                            $platforms_img .= "<img src='" . url('/storage/photos/' . $platform->filename) . "' width=30 height=30 style='margin-right: 4px'/>";
                        } else {
                            $platforms_img .= "<img src='" . url('/icongame.png') . "' width=20 height=20 style='margin-right: 4px'/>";
                        }
                    }
                    return "<div>
                        <p>$game->name</p>
                        $platforms_img
                    </div>";
                })
                ->editColumn('price', function (Game $game) {
                    if ($game->price == 0) {
                        return "FREE";
                    }

                    foreach ($game->sales as $sale_games) {
                        if ($sale_games->started_at < Carbon::now() && $sale_games->ended_at > Carbon::now()) {
                            $price = $game->price - ($game->price * $sale_games->pivot->discount / 100);
                            return
                                '<p><del>' . "IDR" . number_format($game->price, 2, '.', ',') . '</del></p>' .
                                '<p>' . "IDR" . number_format($price, 2, '.', ',') . '</p>';
                        }
                    }
                    return '<p>' . "IDR" . number_format($game->price, 2, '.', ',') . '</p>';
                })
                ->editColumn('description', function (Game $game) {
                    return $game->description;
                })
                ->editColumn('developer', function (Game $game) {
                    return $game->developer->developer->name;
                })
                ->editColumn('released_at', function (Game $game) {
                    return Carbon::parse($game->released_at)->format('d M, Y');
                })
                ->addColumn('action', function (Game $game) {
                    return "<a href='" . url('/admin/games/' . $game->slug_url) . "' class='btn btn-secondary btn-sm'>Detail</a>";
                })
                ->rawColumns(['name', 'price', 'image', 'action'])
                ->make(true);
        }
    }
    public function games_accept($id)
    {
        $game = Game::find($id);
        $game->released_at = Carbon::now();
        $game->status = 1;
        $game->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully accept the game'
        ]);
    }
    public function games_reject($id, Request $request)
    {
        $game = Game::find($id);
        $game->status = -1;
        $game->message_admin = $request->message_admin;
        $game->save();

        return response()->json([
            'success' => true,
            'message' => 'Successfully reject the game'
        ]);
    }
    //
    public function games_download(Request $request)
    {
        $game = Game::where('slug_url', $request->slug_url)->first();
        return Storage::download('/public/games/' . $game->game_filename);
    }
    // /games/{slug_url}
    public function games_show_page($slug_url)
    {
        $game = Game::where('slug_url', $slug_url)->first();
        return view('admin.games.games_detail', [
            "game" => $game
        ]);
    }
    // /gamegenres
    public function gamegenres_page()
    {
        return view('admin.gamegenres.gamegenres');
    }
    public function get_genres(Request $request)
    {
        if ($request->ajax()) {
            $data = Genre::all();
            return DataTables::of($data)
                ->editColumn('id', function (Genre $genre) {
                    return $genre->id;
                })
                ->editColumn('name', function (Genre $genre) {
                    return $genre->name;
                })
                ->addColumn('action', function (Genre $genre) {
                    return "
                        <a href='gamegenres/$genre->id/edit' class='btn btn-sm btn-primary'>Edit</a>
                        <button class='btn btn-sm btn-danger' onclick='delete_confirmation($genre->id)'>Delete</button>
                    ";
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    // /gamegenres/{id}
    public function gamegenres_show_page($id)
    {
        $genre = Genre::find($id);
        return view('admin.gamegenres.gamegenres_detail', [
            "genre" => $genre
        ]);
    }
    // /gamegenres/add
    public function gamegenres_add_page()
    {
        return view('admin.gamegenres.gamegenres_add');
    }
    // /gamegenres/add
    public function gamegenres_edit_page($id)
    {
        $genre = Genre::find($id);
        return view('admin.gamegenres.gamegenres_detail', [
            "genre" => $genre
        ]);
    }
    // /gameplatforms
    public function gameplatforms_page()
    {
        $platforms = Platform::all();
        return view('admin.gameplatforms.gameplatforms', [
            "platforms" => $platforms
        ]);
    }
    public function get_platforms(Request $request)
    {
        if ($request->ajax()) {
            $data = Platform::all();
            return DataTables::of($data)
                ->editColumn('name', function (Platform $platform) {
                    return $platform->name;
                })
                ->editColumn('filename', function (Platform $platform) {
                    if ($platform->filename == "") {
                        return "<img src='" . url('/icongame.png') . "' height=30 width=30 class='img-fluid'/>";
                    }
                    return "<img src='" . url('/storage/photos/' . $platform->filename) . "' alt='Image' width=30 height=30 class='img-fluid'>";
                })
                ->addColumn('action', function (Platform $platform) {
                    return "
                    <a href='gameplatforms/$platform->id/edit' class='btn btn-warning btn-sm'>Edit</a>
                    <button class='btn btn-sm btn-danger' onclick='delete_confirmation($platform->id)'>Delete</button>
                    ";
                })
                ->rawColumns(['action', 'filename'])
                ->make(true);
        }
    }
    // /gameplatforms/{id}
    public function gameplatforms_show_page($id)
    {
        $platform = Platform::find($id);
        return view('admin.gameplatforms.gameplatforms_detail', [
            "platform" => $platform
        ]);
    }
    // /gameplatforms/add
    public function gameplatforms_add_page()
    {
        return view('admin.gameplatforms.gameplatforms_add');
    }
    // /gameplatforms/{id}/edit
    public function gameplatforms_edit_page($id)
    {
        $platform = Platform::find($id);
        return view('admin.gameplatforms.gameplatforms_detail', [
            "platform" => $platform
        ]);
    }
    // /gamefeatures
    public function gamefeatures()
    {
        $features = Feature::all();
        return view('admin.gamefeatures.gamefeatures', [
            "features" => $features
        ]);
    }
    public function get_features(Request $request)
    {
        if ($request->ajax()) {
            $data = Feature::all();
            return DataTables::of($data)
                ->editColumn('name', function (Feature $feature) {
                    return $feature->name;
                })
                ->editColumn('description', function (Feature $feature) {
                    return $feature->description;
                })
                ->editColumn('image_filename', function (Feature $feature) {
                    return "<img src='" . url('/storage/photos/' . $feature->image_filename) . "' alt='Image' width=30 height=30 class='img-fluid'>";
                })
                ->addColumn('action', function (Feature $feature) {
                    return "
                    <a href='gamefeatures/$feature->id/edit' class='btn btn-warning btn-sm'>Edit</a>
                    <a href='gamefeatures/$feature->id/delete' class='btn btn-danger btn-sm'>Delete</a>";
                })
                ->rawColumns(['image_filename', 'action'])
                ->make(true);
        }
    }
    // /gamefeatures/{id}
    public function gamefeatures_show($id)
    {
        $feature = Feature::find($id);
        return view('admin.gamefeatures.gamefeatures_detail', [
            "feature" => $feature
        ]);
    }
    // /gamefeatures/add
    public function gamefeatures_add()
    {
        return view('admin.gamefeatures.gamefeatures_add');
    }
    // /gamefeatures/{id}/edit
    public function gamefeatures_edit($id)
    {
        $feature = Feature::find($id);
        return view('admin.gamefeatures.gamefeatures_edit', [
            "feature" => $feature
        ]);
    }
    // /sales
    public function sales_page()
    {
        return view('admin.sales.sales');
    }
    public function sales_add_page()
    {
        $games = Game::all();
        return view('admin.sales.sales_add', [
            "games" => $games,
        ]);
    }
    public function get_sales(Request $request)
    {
        if ($request->ajax()) {
            $data = Sale::all();
            return DataTables::of($data)
                ->editColumn('id', function (Sale $sale) {
                    return $sale->id;
                })
                ->editColumn('title', function (Sale $sale) {
                    return $sale->title;
                })
                ->editColumn('description', function (Sale $sale) {
                    return $sale->description;
                })
                ->editColumn('started_at', function (Sale $sale) {
                    return Carbon::parse($sale->started_at)->format("M d Y H:i");
                })
                ->editColumn('ended_at', function (Sale $sale) {
                    return Carbon::parse($sale->ended_at)->format("M d Y H:i");
                })
                ->addColumn('action', function (Sale $sales) {
                    return "
                        <a href='/admin/sales/$sales->id' class='btn btn-secondary btn-sm'>Detail</a>
                        <button class='btn btn-sm btn-danger' onclick='delete_confirmation($sales->id)'>Delete</button>
                    ";
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    // /sales/ud
    public function sales_show_page($id)
    {
        $sale = Sale::find($id);
        return view('admin.sales.sales_detail', [
            "sale" => $sale
        ]);
    }
    public function get_sales_games($id, Request $request)
    {
        if ($request->ajax()) {
            $sale = Sale::find($id);
            $data = Sale::find($id)->games;
            return DataTables::of($data)
                ->editColumn('id', function (Game $game) {
                    return $game->id;
                })
                ->editColumn('image', function (Game $game) {
                    if ($game->thumbnail_filename  == "") {
                        return "<img src='" . url('/icongame.png') . "' height=80 width=60/>";
                    }
                    return "<img src='" . url('/storage/' . $game->thumbnail_filename) . "' height=80 width=60/>";
                })
                ->editColumn('name', function (Game $game) {
                    $platforms_img = '';
                    foreach ($game->platforms as $platform) {
                        if ($platform->filename != '') {
                            $platforms_img .= "<img src='" . url('/storage/photos/' . $platform->filename) . "' width=30 height=30 style='margin-right: 4px'/>";
                        } else {
                            $platforms_img .= "<img src='" . url('/icongame.png') . "' width=20 height=20 style='margin-right: 4px'/>";
                        }
                    }
                    return "<div>
                        <p>$game->name</p>
                        $platforms_img
                    </div>";
                })
                ->editColumn('price', function (Game $game) use ($sale) {
                    if ($game->price == 0) {
                        return "FREE";
                    }
                    foreach ($game->sales as $sale_games) {
                        if ($sale_games->id == $sale->id) {
                            $price = $game->price - ($game->price * $sale_games->pivot->discount / 100);
                            return
                                '<p><del>' . "IDR" . number_format($game->price, 2, '.', ',') . '</del></p>' .
                                '<p>' . "IDR" . number_format($price, 2, '.', ',') . '</p>';
                        }
                    }
                    return "IDR" . number_format($game->price, 2, '.', ',');
                })
                ->editColumn('discount', function (Game $game) use ($sale) {
                    foreach ($game->sales as $sale_games) {
                        if ($sale_games->id == $sale->id) {
                            return $sale_games->pivot->discount . '%';
                        }
                    }
                })
                ->editColumn('description', function (Game $game) {
                    return $game->description;
                })
                ->editColumn('developer', function (Game $game) {
                    return $game->developer->developer->name;
                })
                ->editColumn('released_at', function (Game $game) {
                    return Carbon::parse($game->released_at)->format('d M, Y');
                })
                ->addColumn('action', function (Game $game) {
                    return "<a href='/admin/games/$game->slug_url' class='btn btn-secondary btn-sm'>Detail</a>";
                })
                ->rawColumns(['name', 'price', 'image', 'action'])
                ->make(true);
        }
    }
    // /sales/{id}/edit
    public function sales_edit_page($id)
    {
        $sale = Sale::find($id);
        return view('admin.sales.sales_edit', [
            "sale" => $sale
        ]);
    }

    public function admins_page()
    {
        return view('admin.admins.admins');
    }

    public function get_admins(Request $request)
    {
        if ($request->ajax()) {
            $data = Account::where('type', 2)->get();
            return DataTables::of($data)
                ->editColumn('username', function (Account $account) {
                    return $account->username;
                })
                ->make(true);
        }
    }
    public function admins_add_page()
    {
        return view('admin.admins.admins_add');
    }

    public function admins_store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'username' => ['required', 'alpha_num', 'min:4', new UniqueUsername],
            'email' => ['required', ' email:rfc,dns', new UniqueEmail],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);
        // Insert Account
        $account = new Account;
        $account->username = $request->username;
        $account->email = $request->email;
        $account->password = Hash::make($request->password);
        $account->first_name = $request->first_name;
        $account->last_name = $request->last_name;
        $account->balance = 0;
        $account->filename = '';
        $account->type = 2;
        $account->token = Str::random(100);
        $account->verified_at = Carbon::now();
        $account->save();
        // TODO verify account dgn kirim email

        alert()->success('Successfully register admin', 'Success');
        return redirect()->back();
    }
}
