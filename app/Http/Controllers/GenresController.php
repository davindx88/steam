<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use Alert;

class GenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "name" => "required"
        ]);
        $genre = new Genre();
        $genre->name = $request->name;
        $genre->save();

        alert()->success('Successfully create new genre', 'success');
        return redirect('/admin/gamegenres')->with(["status" => "Successfully create new genre"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            "name" => "required"
        ]);
        $genre = Genre::find($id);
        $genre->name = $request->name;
        $genre->save();

        alert()->success('Successfully update genre', 'success');
        return redirect('/admin/gamegenres');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Genre::find($id)->games->count() > 0) {
            $success = false;
            $message = "Can't delete genre because there are games using this genre!";
        } else {
            $delete = Genre::where('id', $id)->delete();
            if ($delete == 1) {
                $success = true;
                $message = "Genre deleted successfully";
            } else {
                $success = false;
                $message = "Genre not found";
            }
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
