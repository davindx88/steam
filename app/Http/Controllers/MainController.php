<?php

namespace App\Http\Controllers;

use App\Account;
use App\ChatRoom;
use App\FriendRequest;
use App\Game;
use App\Genre;
use App\Gift;
use App\Htransaction;
use App\Platform;
use App\Sale;
use App\Topup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    // Page
    // /
    public function indexPage(){
        $now = Carbon::now();
        $sale = Sale::whereDate(
            'started_at', '<=', $now
        )->whereDate(
            'ended_at', '>=', $now
        )->orderBy('id','DESC')
        ->first();
        return view('welcome', [
            'sale' => $sale
        ]);
    }
    // /seacrh
    public function searchPage(Request $request){
        $list_game = [];
        $olddata=[];
        if(session()->has('list_game')){ // Misal Dari POST
            $list_game = session('list_game');
        }else{ // Misal dari GET
            // Get Query String
            $name = $request->query('n', '');
            $list_game = Game::where('status',1)
            ->where('name','like',"%$name%")
            ->get();
        }
        if(session()->has('olddata')){ // Misal Dari POST
            $olddata = session('olddata');
            // dd($olddata);
        }

        $genre = Genre::all();
        $platform=Platform::all();
        return view('user.search',[
            'list_game' => $list_game,
            'platform'=> $platform,
            'genre' => $genre,
            'user' => Auth::user(),
            'olddata' => $olddata
        ]);
    }
    // /upcomingsales
    public function upcomingSalesPage(){
        $now = Carbon::now();
        $sale = Sale::whereDate(
            'started_at', '<=', $now
        )->whereDate(
            'ended_at', '>=', $now
        )->orderBy('id','DESC')
        ->first();
        dd($sale);
        return view('user.sales');
    }
    // /sales
    public function salesPage(){
        $now = Carbon::now();
        $sale = Sale::whereDate(
            'started_at', '<=', $now
        )->whereDate(
            'ended_at', '>=', $now
        )->orderBy('id','DESC')
        ->first();
        return view('user.sales', [
            'user' => Auth::user(),
            'sale' => $sale
        ]);
    }
    // /wishlist
    public function wishlistPage(){
        return view('welcome');
    }
    // /login
    public function loginPage(){
        return view('user.login');
    }
    // /register
    public function registerPage(){
        return view('user.register');
    }
     // /login
     public function loginDeveloper(){
        return view('developer.dahsboard_login');
    }
    // /register
    public function registerDeveloper(){
        return view('developer.dashboard_register');
    }
    // /editprofile
    public function editProfilePage(){
        $user = Auth::user();
        return view('user.editprofile',[
            'user' => $user
        ]);
    }

    // /thankyou
    public function thankyouPage(){
        return view('user.thankyou');
    }
    // /topup
    public function topupPage(){
        $account = Auth::user();
        $list_topup = [
            45000, 145000, 245000, 345000, 445000, 545000, 645000
        ];
        return view('user.topup', [
            'list_topup' => $list_topup,
            'user' => $account
        ]);
    }
    // /history
    public function historyPage(){
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Get All History
        $history_transaction = collect()
        ->concat(Topup::all()->map(function($topup){
            return [
                'account_id' => $topup->account_id,
                'nominal' => $topup->nominal,
                'time' => $topup->created_at,
                'type' => 0,
                'object' => $topup
            ];
        }))
        ->concat(Htransaction::where('type', 0)->get()->map(function($htrans){
            return [
                'account_id' => $htrans->account_id,
                'nominal' => $htrans->total,
                'time' => $htrans->created_at,
                'type' => 1,
                'object' => $htrans
            ];
        }))
        ->concat(Gift::where('status', -1)->get()->map(function($gift){
            return [
                'account_id' => $gift->transaction->account_id,
                'nominal' => $gift->transaction->total,
                'time' => $gift->updated_at,
                'type' => 2,
                'object' => $gift
            ];
        }))->filter(function ($assoc, $idx) use ($user){
            return $assoc['account_id'] == $user->id;
        })->sortByDesc(function($trans){
            return $trans['time'];
        });
        return view('user.history',[
            'history_transaction' => $history_transaction
        ]);
    }
    // /users
    public function usersPage(Request $request){
        // Get Query String
        $u = $request->query('u','');
        // Get List _user
        $list_user = $u == '' ? collect() : Account::where('type', 0)
        ->where('verified_at','<>','')
        ->where('username', 'like', '%'.$u.'%')
        ->get();
        return view('user.users', [
            'user' => Auth::user(),
            'list_user' => $list_user
        ]);
    }
    // /users/{username}
    public function detailUserPage($username){
        // Get User URL
        $user = Account::where('username', $username)->first();
        // Get Active User
        $account = Auth::user();
        // Boolean is LoggedUser
        $isLoggedUser = false;
        if($account !== null)
            $isLoggedUser = $account->id == $user->id;
        return view('user.profile',[
            'user' => $user,
            'isLoggedUser' => $isLoggedUser
        ]);
    }
    // /friends
    public function friendsPage(){
        $user = Auth::user();
        // Dapetin Friend, FriendRequest yg pending
        $list_chatroom = ChatRoom::where(function($q)use($user){
            $q->where('account_id', $user->id)
            ->orWhere('friend_id', $user->id);
        })->where('status',0)
        ->get();
        return view('user.friend', [
            'user' => $user,
            'chatroom' => $list_chatroom
        ]);
    }
    // /friends/{username} chat
    public function detailFriendPage($username){
        $user = Account::find(Auth::user()->id);
        $friend = Account::where('username', $username)->first();
        $room = $user->chatroomWithFriend($friend->id);
        return view('user.chat', [
            'user' => $user,
            'friend' => $friend,
            'room' => $room
        ]);
    }
    // /friends/add
    public function addFriendPage(Request $request){
        // Get Query String if exist
        $username = $request->query('u','');
        $user = Auth::user();
        return view('user.addfriend', [
            'user' => $user,
            'username' => $username
        ]);
    }
    // /friends/request
    public function friendRequestPage(){
        $user = Auth::user();
        // Dapetin yg pending maupun yg Request biasa
        $list_pending = FriendRequest::where('friend_id', $user->id)
            ->where('status', 0)
            ->get();
        $list_requested = FriendRequest::where('account_id', $user->id)
            ->where('status','<>',2)
            ->get();
        return view('user.friendrequest', [
            'user' => $user,
            'list_pending' => $list_pending,
            'list_requested' => $list_requested,
        ]);
    }
    // /gifts
    public function giftPage(Request $request){
        // Get Type
        $list_type = collect(['received','sent']);
        $type = $request->type ?? 'received';
        // JIka Gift tidak valid maka replace dengan received
        if(!$list_type->contains($type))
            $type = $list_type[0];
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Get Gift tergantung jenis
        $list_gift = collect([]);
        if($type == 'received'){
            $list_gift = $user->mygifts()->where('status', 0)->get();
        }else if($type == 'sent'){
            $list_gift = $user->sentgifts;
        }
        return view('user.gift', [
            'user' => $user,
            'list_gift' => $list_gift,
            'type' => $type == 'received' ? 0 : 1
        ]);
    }
    // /mygames
    public function myGamesPage(){
        return view('user.mygames', [
            'user' => Auth::user()
        ]);
    }
    // /mygames/{game_slug} NOPE
    public function detailMyGamesPage($game_slug){
        $user = Account::find(Auth::user()->id);
        $game = $user->games()->where('slug_url', $game_slug)->first();
        return view('user.detail_mygames', [
            'user' => $user,
            'game' => $game
        ]);
    }
    // /cart
    public function cartPage(){
        // Get Cart
        $cart = session()->get('cart',[]);
        $cart = collect($cart)->map(function($n_game, $game_slug){
            return [
                'game' => Game::where('slug_url',$game_slug)->first(),
                'qty' => $n_game
            ];
        });
        // Subtotal
        $cart_total = $cart->count() == 0 ? -1 : $cart->reduce(function($carry, $item){
            return $carry + $item['game']->price;
        },0);
        // Discount
        $discount = $cart->reduce(function($carry, $item){
            return $carry + $item['game']->price - $item['game']->discprice();
        },0);
        // Total
        $final_total = $cart_total - $discount;
        return view('user.cart', [
            'user' => Auth::user(),
            'cart' => $cart,
            'cart_total' => $cart_total,
            'discount' => $discount,
            'final_total' => $final_total,
        ]);
    }
    // /checkout
    public function checkoutPage(){
        // Get Cart
        $cart = session()->get('cart',[]);
        $cart = collect($cart)->map(function($n_game, $game_slug){
            return [
                'game' => Game::where('slug_url',$game_slug)->first(),
                'qty' => $n_game
            ];
        });
        // Subtotal
        $cart_total = $cart->count() == 0 ? -1 : $cart->reduce(function($carry, $item){
            return $carry + $item['game']->price;
        },0);
        // Discount
        $discount = $cart->reduce(function($carry, $item){
            return $carry + $item['game']->price - $item['game']->discprice();
        },0);
        // Total
        $final_total = $cart_total - $discount;
        // Get All Friend
        $user = Account::find(Auth::user()->id);
        $list_friend = $user->chatrooms()
        ->where('status',0)
        ->get()
        ->map(function($room)use($user){
            return $room->friend($user->id);
        });
        return view('user.checkout', [
            'user' => $user,
            'cart' => $cart,
            'cart_total' => $cart_total,
            'discount' => $discount,
            'final_total' => $final_total,
            // list Friend to gift
            'list_friend' => $list_friend
        ]);
    }
}
