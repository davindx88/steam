<?php

namespace App\Http\Controllers;

use App\Account;
use App\Gift;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GiftsController extends Controller
{
    public function responseGift(Request $request)
    {
        $validate = $request->validate([
            'gift_id' => 'required',
            'response' => ['required','in:0,1']
        ]); // 0 Terima, 1 Reject
        // Get Parameter
        $gift_id = $request->gift_id;
        $response = $request->response;
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Get gift
        $gift = Gift::find($gift_id);
        if($response == 0){
            // Jika diterima
            // Update Gift Status
            $gift->status = 1;
            $gift->save();
            // Iterate untuk tiap games pada transaksi
            $htrans = $gift->transaction;
            $htrans->games->each(function($game)use($user){
                // Tambah ke AccountGames
                $user->games()->attach($game->id,[
                    'owned_at' => Carbon::now()
                ]);
                // Tambah uang developer
                $developer = $game->developer;
                $developer->balance += $game->pivot->price;
                $developer->save();
            });
        }else{
            // Jika ditolak
            // Update Gift Status
            $gift->status = -1;
            $gift->save();
            // Iterate untuk tiap games pada transaksi
            $htrans = $gift->transaction;
            $buyer = $htrans->account;
            $htrans->games->each(function($game)use($buyer){
                // Kembalikan uang buyer
                $buyer->balance += $game->pivot->price;
            });
            $buyer->save();
        }
        // TODO kasih sweet alert gift succes direspon
        return redirect('/gifts');
    }
}
