<?php

namespace App\Http\Controllers;

use App\Developer;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "title" => "required",
            "description" => "required",
            "filename" => "required",
            "date" => "required"
        ]);
        if ($request->hasFile("filename")) {
            $date = explode(' until ', $request->date);
            $date_start = Carbon::createFromFormat('Y-m-d H:i:s A', trim($date[0]))->format('Y-m-d H:i:s');
            $date_end = Carbon::createFromFormat('Y-m-d H:i:s A', trim($date[1]))->format('Y-m-d H:i:s');

            // dd($date_start);
            // check if there is another sale
            $sale = Sale::whereBetween('started_at', [$date_start, $date_end])->orWhereBetween('ended_at', [$date_start, $date_end])->get();

            if ($sale->count()) {
                alert()->error("Can't create new sale because there is another sale", 'Failed');
                return redirect()->back();
            } else {

                $sale = new Sale();
                $sale->title = $request->title;
                $sale->description = $request->description;
                $sale->started_at = $date_start;
                $sale->ended_at = $date_end;
                $sale->filename = uniqid() . "." . $request->file('filename')->getClientOriginalExtension(); // string random
                $sale->save();
                // saving file
                $request->file('filename')->storeAs('/public/Photos', $sale->filename);

                foreach (Developer::all() as $developer) {
                    $sale->accounts()->attach($developer->account->id, ["message" => "$sale->title is now open for register, register your game now!", "status" => 0]);
                }
                alert()->success('Successfully create new sale', 'success');
                return redirect('/admin/sales');
            }
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Carbon::now() > Sale::find($id)->started_at) {
            $success = false;
            $message = "Can't delete sale because sale is ongoing!";
        } else {
            $delete = Sale::where('id', $id)->delete();
            if ($delete == 1) {
                $success = true;
                $message = "Genre deleted successfully";
            } else {
                $success = false;
                $message = "Genre not found";
            }
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
