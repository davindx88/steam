<?php

namespace App\Http\Controllers;

use App\ChatRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatRoomsController extends Controller
{
    public function unFriend(Request $request){
        $validate = $request->validate([
            'chatroom_id' => 'required'
        ]);
        // Get Parameter
        $chatroom_id = $request->chatroom_id;
        // Get Active User
        $user = Auth::user();
        // Get Active ChatRoom
        // SECURITY cek apakah chatroomnya ada active user
        $room = ChatRoom::find($chatroom_id);
        $room->status = 1;
        $room->save();
        // Redirect kembali
        // TODO kasih pesan
        return back();
    }
}
