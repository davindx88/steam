<?php

namespace App\Http\Controllers;

use App\Platform;
use Illuminate\Http\Request;

class PlatformsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "name" => "required",
            "filename" => "required"
        ]);
        if ($request->hasFile("filename")) {
            $platform = new Platform();
            $platform->name = $request->name;
            $fullname = $request->file('filename')->getClientOriginalName();
            $filename = pathinfo($fullname, PATHINFO_FILENAME);
            $extension = $request->file('filename')->getClientOriginalExtension();
            // string random
            $platform->filename = uniqid() . "." . $extension;
            $platform->save();
            // saving file
            $request->file('filename')->storeAs('/public/Photos', $platform->filename);


            alert()->success('Successfully create new platform', 'success');
            return redirect('/admin/gameplatforms')->with(["status" => "Successfully create new platform"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "name" => "required",
        ]);
        if ($request->hasFile("filename")) {

            $platform = Platform::find($id);
            $platform->name = $request->name;

            //code for remove old file
            if ($platform->file != ''  && $platform->file != null) {
                $file_old = public_path() . '/Photos/' . $platform->filename;
                unlink($file_old);
            }

            $platform->filename = uniqid() . "." . $request->file('filename')->getClientOriginalExtension();;
            $platform->save();

            $request->file('filename')->storeAs('/public/Photos', $platform->filename);

            alert()->success('Successfully update platform', 'success');

            return redirect('/admin/gameplatforms');
        } else {
            $platform = Platform::find($id);
            $platform->name = $request->name;
            alert()->success('Successfully update platform', 'success');

            return redirect('/admin/gameplatforms');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Platform::find($id)->games->count() > 0) {
            $success = false;
            $message = "Can't delete platform because there are games using this platform!";
        } else {
            $delete = Platform::where('id', $id)->delete();
            if ($delete == 1) {
                $success = true;
                $message = "Platform deleted successfully";
            } else {
                $success = false;
                $message = "Platform not found";
            }
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
