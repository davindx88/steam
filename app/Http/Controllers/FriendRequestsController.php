<?php

namespace App\Http\Controllers;

use App\Account;
use App\ChatRoom;
use App\FriendRequest;
use App\Rules\AccountIsGamer;
use App\Rules\AccountVerified;
use App\Rules\UsernameExist;
use App\Rules\UsernameNotActiveUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FriendRequestsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Make friend request
        $validate = $request->validate([
            'username' => 'required',
            'message' => 'required'
        ]);
        $validate = $request->validate([ // Check Apakah Username exist
            'username' => new UsernameExist
        ]);
        $validate = $request->validate([ // Check Apakah Account Active
            'username' => new UsernameNotActiveUser
        ]);
        $validate = $request->validate([ // Check Apakah Account Verified
            'username' => new AccountVerified
        ]);
        $validate = $request->validate([ // Check Apakah Account Gamer
            'username' => new AccountIsGamer
        ]);
        // Get Parameter
        $username = $request->username;
        $message = $request->message;
        // Get Active User
        $user = Auth::user();
        // Get other User
        $friend = Account::where('username', $username)->first();
        // Pengecekan apakah sudah friend dan masih friend
        $chatroom = ChatRoom::where(function($q) use ($user, $friend){
            $q->where('account_id', $user->id)
            ->where('friend_id', $friend->id)
            ->where('status', 0);
        })->orWhere(function($q) use ($user, $friend){
            $q->where('account_id', $friend->id)
            ->where('friend_id', $user->id)
            ->where('status', 0);
        })->get();
        if($chatroom->count() != 0){
            // Kalau sudah friend
            return back()->withErrors([
                'msg' => 'Friend sudah ada pada list friend'
            ]);
        }else{
            // Kalau belum friend
            // Cek apakah ada Request yang masih pending
            $friendreq = FriendRequest::where('account_id', $user->id)
                ->where('friend_id', $friend->id)
                ->where('status', 0)
                ->first();
            if($friendreq !== null){
                // Jika ada request yang pending
                return back()->withErrors([
                    'msg' => 'Masih ada request yang pending'
                ]);
            }else{
                // Jika tidak ada request yang pending
                $friendreq = new FriendRequest();
                $friendreq->account_id = $user->id;
                $friendreq->friend_id = $friend->id;
                $friendreq->message = $message;
                $friendreq->status = 0;
                $friendreq->save();
                // Redirect ke halaman friend
                session()->flash('msg', 'Request berhasil dibuat');
                return redirect('/friends/request');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validate = $request->validate([
            'id' => 'required',
            'resp' => ['required','in:1,-1']
        ]);
        // Active User
        $user = Auth::user();
        // Update Friend Request
        $freq = FriendRequest::find($request->id);
        $freq->status = $request->resp;
        $freq->save();
        // Jika diterima maka buat Chatroom
        if($freq->status == 1){
            // TODO auto Expired Freind Request yang ngawang
            // Code ...

            // Buat Chatroom
            $room = new ChatRoom();
            $room->account_id = $freq->sender->id;
            $room->friend_id = $freq->receiver->id;
            $room->status = 0;
            $room->save();
            // dd($freq);
        }
        return redirect('/friends/request');
    }
}
