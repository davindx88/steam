<?php

namespace App\Http\Controllers;

use App\Account;
use App\Game;
use App\Genre;
use App\Rules\GameNotReviewed;
use App\Rules\GameOwned;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GamesController extends Controller
{
    public function reviewGame(Request $request)
    {
        $validate = $request->validate([ // check form input
            'game_slug' => 'required',
            'rating' => 'required|numeric|between:1,5',
            'message' => 'required'
        ]);
        $validate = $request->validate([ // Check punya game & juga game exist
            'game_slug' => new GameOwned,
        ]);
        $validate = $request->validate([ // Check apakah game sudah direview
            'game_slug' => new GameNotReviewed,
        ]);
        // Get Parameter
        $game_slug = $request->game_slug;
        $rating = $request->rating;
        $message = $request->message;
        // Get Active User
        $user = Account::find(Auth::user()->id);
        // Get Game
        $game = Game::where('slug_url', $game_slug)->first();
        // Insert Review
        $game->reviews()->attach($user->id, [
            'rating' => $rating,
            'message' => $message,
        ]);
        // Redirect
        return redirect('/developers/'.$game->developer->developer->slug_url.'/'.$game->slug_url);
    }
    public function search(Request $request )
    {
        // Get Parameter
        $name = $request->key ?? '';
        $listGenre = $request->genre ?? [];
        $listGenre = collect($listGenre);
        $listPlatform = $request->platform ?? [];
        $listPlatform = collect($listPlatform);
        // dd($request->filterprice );
        // Query
        $query = Game::where('status',1);
        if($name !== ''){
            $query = $query->where('name', 'like', '%'.$name.'%');
        }
        $list_game = $query->get();
        // if($orderBy !== ''){
        //     $query = $query->orderBy('price', $orderBy);
        // }

        if ($request->allfilter == "asc") {
            $list_game =   $list_game->sortBy(function($game){
                return $game->discprice();
            });
        }

        if ($request->allfilter == "desc") {
            $list_game =   $list_game->sortByDesc(function($game){
                return $game->discprice();
            });
        }
        if ($request->allfilter == "rating") {
            $list_game =   $list_game->sortByDesc(function($game){
                return $game->rating();
            });
        }
        if ($request->allfilter == "views") {
            $query = Game::where('status',1)->orderBy('views', 'desc');
            $list_game = $query->get();
        }
        if ($request->allfilter == "release") {
            $query = Game::where('status',1)->orderBy('released_at', 'asc');
            $list_game = $query->get();
        }
        if ($request->allfilter == "recent") {
            $query = Game::where('status',1)->orderBy('released_at', 'desc');
            $list_game = $query->get();
        }
        if ($request->allfilter == "most") {
            $list_game =   $list_game->sortByDesc(function($game){
                return $game->gamers->count();
            });
        }






        // Pengecekan Genre

        if($listGenre->count() > 0){
            $list_game = $list_game->filter(function($game)use($listGenre){
                return $game->genres()->whereIn('id', $listGenre)->first() !== null;
            });
        }
        if($listPlatform->count() > 0){
            $list_game = $list_game->filter(function($game)use($listPlatform){
                return $game->platforms()->whereIn('id', $listPlatform)->first() !== null;
            });
        }


        if($request->filterprice =="free"){
            $list_game =  $list_game->filter(function($game){
                return $game->discprice() == 0;
            });
        }
        if($request->filterprice =="0"){
            $list_game =  $list_game->filter(function($game){
                return $game->discprice() >= 0 && $game->discprice() < 50000;
            });
        }
        if($request->filterprice =="50"){
            $list_game =  $list_game->filter(function($game){
                return $game->discprice() >= 50000 && $game->discprice() < 100000;
            });
        }
        if($request->filterprice =="100"){
            $list_game =  $list_game->filter(function($game){
                return $game->discprice() >= 100000 && $game->discprice() < 200000;
            });
        }
        if($request->filterprice =="200"){
            $list_game =   $list_game->filter(function($game){
                return $game->discprice() >= 200000 && $game->discprice() < 500000;
            });
        }
        if($request->filterprice =="500"){
            $list_game =   $list_game->filter(function($game){
                return $game->discprice() >= 500000;
            });
        }
        // Simpan Di Session Flash
        session()->flash('list_game', $list_game);
        session()->flash('olddata',[
            "key"=>$request->key,
            "allfilter"=>$request->allfilter,
            "genre"=>$request->genre,
            "platform"=>$request->platform,
            "filterprice"=>$request->filterprice,
        ]);
        return redirect('/search');
    }
}
