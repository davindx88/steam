<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class View extends Model
{
    //
    protected $table = 'views';
    use SoftDeletes;

    public function game()
    {
        return $this->belongsTo('App\Game', 'game_id', 'id');
    }

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id', 'id');
    }
}
