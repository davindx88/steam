<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topup extends Model
{
    protected $table = 'topups';
    use SoftDeletes;
    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }
}
