<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wishlist extends Model
{
    protected $table = 'wishlists';
    use SoftDeletes;

    public function accounts()
    {
        return $this->belongsTo('App\Account', 'whistlist_id', 'account_id');
    }
    public function games()
    {
        return $this->belongsToMany('App\Game', 'GamePlatforms', 'whistlist_id', 'game_id');
    }

}
