<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatRoom extends Model
{
    //
    protected $table = 'chatrooms';
    use SoftDeletes;

    public function chats(){
        return $this->hasMany(Chat::class, 'chatroom_id');
    }

    public function accounts(){
        return Account::whereIn('id', [
            $this->account_id, $this->friend_id
        ]);
    }

    public function sender(){
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function receiver(){
        return $this->belongsTo(Account::class, 'friend_id');
    }

    // Dapetin Profile Temen
    // parameter: active user
    // output: user lain yang berada di chatroom
    public function friend($user_id){
        $field = ($this->account_id == $user_id) ? 'friend_id' : 'account_id';
        return $this->belongsTo(Account::class, $field)->first();
    }
}
