<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Account extends Authenticatable
{
    // Notification
    use Notifiable;
    //
    protected $table = "accounts";
    use SoftDeletes;

    public function developedGames()
    {
        return $this->developer->hasMany(Game::class, 'developer_id');
    }

    public function games()
    {
        return $this->belongsToMany(Game::class, 'accountgames', 'account_id', 'game_id')->withPivot('owned_at');
    }

    public function transactions()
    {
        return $this->hasMany(Htransaction::class, 'account_id');
    }

    public function reviews()
    {
        return $this->belongsToMany(Game::class, 'reviews', 'account_id', 'game_id')->withPivot('rating', 'message')->withTimestamps();;
    }

    public function wishlists()
    {
        return $this->hasMany(Wishlist::class, 'account_id');
    }

    public function salemails()
    {
        return $this->belongsToMany(Sale::class, 'salemails', 'account_id', 'sale_id');
    }

    public function topups()
    {
        return $this->hasMany(Topup::class, 'account_id');
    }

    public function sentgifts()
    {
        return $this->hasMany(Gift::class, 'account_id');
    }

    public function mygifts()
    {
        return $this->hasMany(Gift::class, 'friend_id');
    }

    public function sentfriendrequest()
    {
        return $this->hasMany(FriendRequest::class, 'account_id');
    }

    public function myfriendrequest()
    {
        return $this->hasMany(FriendRequest::class, 'friend_id');
    }

    public function chatrooms()
    {
        return ChatRoom::where(function ($q) {
            $q->where('account_id', $this->id)
                ->orWhere('friend_id', $this->id);
        });
    }

    public function chatroomWithFriend($friend_id)
    {
        return ChatRoom::where(function ($q)use($friend_id){
            $q->where(function($q1)use($friend_id){
                $q1->where('account_id', $this->id)
                    ->where('friend_id', $friend_id);
            })->orWhere(function($q1)use($friend_id){
                $q1->where('account_id', $friend_id)
                    ->where('friend_id', $this->id);
            });
        })->where('status', 0)->first();
    }

    public function friends()
    {
        return $this->chatrooms()
            ->where('status', 0)
            ->get()
            ->map(function ($room) {
                return $room->friend($this->id);
            });
    }

    public function sentchats()
    {
        return $this->hasMany(Chat::class, 'sender_id');
    }

    public function developer()
    {
        return $this->hasOne(Developer::class, 'account_id');
    }

    public function views()
    {
        return $this->hasMany(View::class, 'account_id');
    }

    // Helper
    public function name()
    {
        return $this->first_name.' '.$this->last_name;
    }
}
