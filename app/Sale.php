<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    protected $table = 'sales';
    use SoftDeletes;
    public function games()
    {
        return $this->belongsToMany('App\Game', 'SaleGames', 'sale_id', 'game_id')->withPivot(["discount"])->withTimestamps();
    }
    public function accounts()
    {
        return $this->belongsToMany('App\Account', 'SaleMails', 'sale_id', 'account_id')->withPivot(["message", "status"])->withTimestamps();
    }
}
