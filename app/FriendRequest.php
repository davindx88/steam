<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FriendRequest extends Model
{
    //
    protected $table = 'friendrequests';
    use SoftDeletes;

    public function Account() {
        return $this->belongsToMany('App\Account', 'Friend', 'id', 'id');
      }

    public function sender(){
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function receiver(){
        return $this->belongsTo(Account::class, 'friend_id');
    }
}
