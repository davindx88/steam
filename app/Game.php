<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    //
    protected $table = "games";
    use SoftDeletes;
    // TimeStamp
    protected $dates = ['released_at'];

    public function developer(){
        return $this->accountDeveloper->belongsTo(Account::class, 'account_id');
    }

    public function accountDeveloper(){
        return $this->belongsTo(Developer::class, 'developer_id');
    }

    public function genres(){
        return $this->belongsToMany(Genre::class, 'gamegenres', 'game_id', 'genre_id');
    }

    public function platforms(){
        return $this->belongsToMany(Platform::class, 'gameplatforms', 'game_id', 'platform_id');
    }

    public function features(){
        return $this->hasMany(Feature::class, 'game_id');
    }

    public function transactions(){
        return $this->belongsToMany(Htransaction::class, 'dtransactions', 'game_id', 'htransaction_id')->withPivot('price');
    }

    public function reviews(){
        return $this->belongsToMany(Account::class, 'reviews', 'game_id', 'account_id')->withPivot('rating', 'message')->withTimestamps();
    }

    public function gamers(){
        return $this->belongsToMany(Account::class, 'accountgames', 'game_id', 'account_id');
    }

    public function wishlists(){
        return $this->hasMany(Wishlist::class, 'game_id');
    }

    public function sales(){
        return $this->belongsToMany(Sale::class, 'salegames', 'game_id', 'sale_id')->withPivot("discount")->withTimestamps();
    }

    public function gifts(){
        return $this->hasMany(Gift::class, 'game_id');
    }

    public function views(){
        return $this->hasMany(View::class, 'game_id');
    }

    // Helper
    public function rating(){
        $ctr = 0;
        $list_review = $this->reviews;
        foreach($list_review as $account){
            $ctr += $account->pivot->rating;
        }
        $rating = 0;
        if($ctr != 0){
            $rating = round($ctr/$this->reviews->count(),1);
        }
        return $rating;
    }

    public function discprice(){
        $now = Carbon::now();
        $sale = Sale::whereDate(
            'started_at', '<=', $now
        )->whereDate(
            'ended_at', '>=', $now
        )->orderBy('id','DESC')
        ->first();

        // Default Value
        $price = $this->price;
        if($sale !== null){
            $game = $sale->games()->where('id', $this->id)->first();
            if($game !== null){
                // Jika games masuk sales
                $price = (100-$game->pivot->discount) * $this->price / 100;
            }
        }
        return $price;
    }
}
