# FAF Gaming

FAF Gaming adalah website clone dari [Steam](https://store.steampowered.com/). Seperti Steam website ini merupakan website jual / beli game. Di website ini juga terdapat 3 user yaitu:  
1. __Gamer__ : Game buyer
1. __Developer__ : Game seller 
1. __Admin__ : Website maintainer

Project ini ada karena merupakan project untuk mata kuliah __Framework Pemrograman Web__, dimana dalam mata kuliah ini mahasiswa mempelajari secara dalam bagaimana cara kerja framework [Laravel](https://laravel.com/). Karena itu tentu bahasa pemrograman yang dipakai adalah [PHP](https://www.php.net/) dan framework yang digunakan adalah [Laravel](https://laravel.com/).

## Table Of Contents
[[_TOC_]]

## Developer

* 218116682 - [Davin Djayadi](https://www.linkedin.com/in/davin-djayadi-04a8a41ab/)
* 218116688 - [Ivan Budihardjo](https://www.linkedin.com/in/ivan-budihardjo-61a8a21ab/)
* 218116689 - [James Jeremy Foong](https://www.linkedin.com/in/james-f-308b6111b/)
* 218116707 - [Yulius](https://www.linkedin.com/in/yulius-setiawan-13708a1aa/)

## Pembagian Tugas

Untuk pembagian tugasnya kita bagi per **role** :
- __User__ &rarr; Davin & Yulius
- __Developer__ &rarr; Ivan
- __Admin__ &rarr; James

## Materi yang digunakan

Karena proyek ini dibuat untuk proyek mata kuliah __Framework Pemrograman Web__ tentu ada beberapa materi yang kita gunakan yaitu :
- __Routing__
- __Controlller__
- __Views & Blade__
- __Form Validation & Rules__
- __Model__
- __Eloquent__
- __Session__
- __Middleware__
- __Auth & Guard__
- __Notification Email__
- __Seeder & Factory__
- __Migration__
- __File Storage__


## Installation

1. Clone project **FPW Steam** di https://gitlab.com/davindx88/steam
    ```bash
    $ git clone https://gitlab.com/davindx88/steam.git
    ```
1.  Pergi ke directory project **FPW Steam**
    ```bash
    $ cd steam
    ```
1. Install Package Vendor
    ```bash
    $ composer install
    ```
1. Buat symbolic link untuk __File Storage__
    ```bash
    $ php artisan storage:link
    ```
1. Copy directory __public/Photos__ ke dalam directory __storage/app__ dan rename folder yang baru dicopy menjadi __template__
    ```
    steam
    ├───public
    │   ├───Photos <-- copy
    │   ...
    ...   
    └───storage
    │   ├───app <-- paste disini
    │   │   ├─ public
    │   │   └─ template <-- rename jadi template
    │   ├───framework
    │   └───logs
    ...
    ```
    ```bash
    $ cp -R public/Photos storage/app/template
    ```
1. Buat & konfigurasi file __.env__. Untuk referensinya bisa dilihat di __.env.example__
    ```bash
    $ cp .env.example .env
    $ vi .env
    ```
1. Buat database dengan nama sesuai yang ada pada __.env__

1. Jalankan migrasi database
    ```bash
    $ php artisan migrate:fresh --seed
    ```

## Run Project

Setelah tahap Installasi untuk tahap run cukup mudah bisa langsung dijalankan dengan
```bash
$ php artisan serve
```
