<?php

use App\Http\Middleware\CartGamesValidMiddleware;
use App\Http\Middleware\CartNotEmptyMiddleware;
use App\Http\Middleware\DeveloperExistMiddleware;
use App\Http\Middleware\DeveloperGameValidMiddleware;
use App\Http\Middleware\GameOwnedMiddleware;
use App\Http\Middleware\isAlreadyinSale;
use App\Http\Middleware\IsFriendValidMiddleware;
use App\Http\Middleware\IsGiftValidMiddleware;
use App\Http\Middleware\IsUnAuthorizedMiddleware;
use App\Http\Middleware\IsUserMiddleware;
use App\Http\Middleware\IsUsernameValidMiddleware;
use App\Http\Middleware\NotDeveloperAdminMiddleware;
use App\Http\Middleware\UserBalanceEnoughMiddleware;
use App\Http\Middleware\ViewGameMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Main
// Not Developer or Admin
Route::get('/', 'MainController@indexPage')->middleware(NotDeveloperAdminMiddleware::class);
Route::get('/search', 'MainController@searchPage')->middleware(NotDeveloperAdminMiddleware::class);
Route::get('/upcomingsales', 'MainController@upcomingSalesPage')->middleware(NotDeveloperAdminMiddleware::class);
Route::get('/sales', 'MainController@salesPage')->middleware(NotDeveloperAdminMiddleware::class);
Route::group(['prefix' => '/users', 'middleware' => [
    NotDeveloperAdminMiddleware::class
]], function () {
    Route::get('/', 'MainController@usersPage');
    Route::get('/{username}', 'MainController@detailUserPage')
        ->middleware(IsUsernameValidMiddleware::class);
});
Route::group(['prefix' => '/cart', 'middleware' => [
    NotDeveloperAdminMiddleware::class
]], function () {
    // Page
    Route::get('/', 'MainController@cartPage')
        ->middleware([
            'cartgamesvalid:true'
        ]);
    Route::get('/checkout', 'MainController@checkoutPage')
        ->middleware([
            IsUserMiddleware::class,
            CartNotEmptyMiddleware::class,
            'cartgamesvalid:true'
        ]);
    // Action
    Route::post('/add', 'AccountsController@addGameCart');
    Route::post('/delete', 'AccountsController@deleteGameCart');
    Route::post('/clear', 'AccountsController@clearCart');
    Route::post('/submit', 'HtransactionsController@makeTransaction')
        ->middleware([
            IsUserMiddleware::class,
            CartNotEmptyMiddleware::class,
            CartGamesValidMiddleware::class,
            UserBalanceEnoughMiddleware::class
        ]);
});
Route::prefix('/accounts')->group(function () {
    Route::post('/login', 'AccountsController@login')->middleware(IsUnAuthorizedMiddleware::class);
    Route::post('/register', 'AccountsController@register')->middleware(IsUnAuthorizedMiddleware::class);
    Route::post('/topup', 'TopupsController@store')->middleware(IsUserMiddleware::class);
    Route::post('/editprofile', 'AccountsController@editprofile')->middleware(IsUserMiddleware::class);
});
// Not Authrorized User
Route::get('/login', 'MainController@loginPage')->middleware(IsUnAuthorizedMiddleware::class);
Route::get('/register', 'MainController@registerPage')->middleware(IsUnAuthorizedMiddleware::class);
Route::get('/logindeveloper', 'MainController@loginDeveloper')->middleware(IsUnAuthorizedMiddleware::class);
Route::get('/registerdeveloper', 'MainController@registerDeveloper')->middleware(IsUnAuthorizedMiddleware::class);
// Logged User
Route::get('/wishlist', 'MainController@wishlistPage')->middleware(IsUserMiddleware::class);
Route::get('/editprofile', 'MainController@editProfilePage')->middleware(IsUserMiddleware::class);
Route::get('/thankyou', 'MainController@thankyouPage')->middleware(IsUserMiddleware::class);
Route::get('/topup', 'MainController@topupPage')->middleware(IsUserMiddleware::class);
Route::get('/history', 'MainController@historyPage')->middleware(IsUserMiddleware::class);
Route::group(['prefix' => '/friends', 'middleware' => [
    IsUserMiddleware::class
]], function () {
    Route::get('/', 'MainController@friendsPage');
    Route::get('/add', 'MainController@addFriendPage');
    Route::post('/addfriend', 'FriendRequestsController@store');
    Route::get('/request', 'MainController@friendRequestPage');
    Route::post('/response', 'FriendRequestsController@update');
    Route::post('/unfriend', 'ChatRoomsController@unFriend');
    Route::get('/{username}', 'MainController@detailFriendPage') // Chat
        ->middleware([
            IsUsernameValidMiddleware::class,
            IsFriendValidMiddleware::class
        ]);
});
Route::group(['prefix' => '/gifts', 'middleware' => [
    IsUserMiddleware::class
]], function () {
    Route::get('/', 'MainController@giftPage');
    Route::post('/response', 'GiftsController@responseGift')
        ->middleware([
            IsGiftValidMiddleware::class
        ]);
});
Route::group(['prefix' => '/mygames', 'middleware' => [
    IsUserMiddleware::class
]], function () {
    Route::get('/', 'MainController@myGamesPage');
    Route::get('/{game_slug}', 'MainController@detailMyGamesPage')
        ->middleware([
            GameOwnedMiddleware::class
        ]);
});
// Post
Route::post('/reviewgames', 'GamesController@reviewGame')->middleware(IsUserMiddleware::class);;
Route::group(['prefix' => '/chats', 'middleware' => [
    IsUserMiddleware::class
]], function () {
    Route::post('/insert', 'ChatsController@insert');
    Route::post('/get', 'ChatsController@get');
});

// Admin
Route::group(['prefix' => '/admin', 'middleware' => ['isAdmin']], function () {
    Route::get('/', 'AdminController@index_page');
    Route::get('/profile', 'AdminController@profile');
    Route::post('/profile/update', 'AdminController@profile_update');


    Route::get('/accounts', 'AdminController@accounts_page');
    Route::get('/accounts/get_accounts', 'AdminController@get_accounts');
    Route::get('/accounts/{username}', 'AdminController@accounts_show_page');

    Route::get('/developers', 'AdminController@developers_page');
    Route::get('/developers/get_developers', 'AdminController@get_developers');
    Route::get('/developers/{slug_url}', 'AdminController@developers_show_page');

    Route::get('/games/released', 'AdminController@games_released_page');
    Route::get('/games/unreleased', 'AdminController@games_unreleased_page');
    Route::get('/games/get_released_games', 'AdminController@get_released_games');
    Route::get('/games/get_unreleased_games', 'AdminController@get_unreleased_games');
    Route::post('/games/accept/{id}', 'AdminController@games_accept');
    Route::post('/games/reject/{id}', 'AdminController@games_reject');
    Route::post('/games/download', 'AdminController@games_download');
    Route::get('/games/{slug_url}', 'AdminController@games_show_page');


    Route::get('/gamegenres', 'AdminController@gamegenres_page');
    Route::get('/gamegenres/get_genres', 'AdminController@get_genres');
    Route::get('/gamegenres/add', 'AdminController@gamegenres_add_page');
    Route::post('/gamegenres/store', 'GenresController@store');
    Route::post('/gamegenres/{id}/update', 'GenresController@update');
    Route::post('/gamegenres/{id}/destroy', 'GenresController@destroy');
    Route::get('/gamegenres/{id}', 'AdminController@gamegenres_show_page');
    Route::get('/gamegenres/{id}/edit', 'AdminController@gamegenres_edit_page');

    Route::get('/gameplatforms', 'AdminController@gameplatforms_page');
    Route::get('/gameplatforms/get_platforms', 'AdminController@get_platforms');
    Route::get('/gameplatforms/add', 'AdminController@gameplatforms_add_page');
    Route::post('/gameplatforms/store', 'PlatformsController@store');
    Route::post('/gameplatforms/{id}/update', 'PlatformsController@update');
    Route::post('/gameplatforms/{id}/destroy', 'PlatformsController@destroy');
    Route::get('/gameplatforms/{id}', 'AdminController@gameplatforms_show_page');
    Route::get('/gameplatforms/{id}/edit', 'AdminController@gameplatforms_edit_page');

    // Route::get('/gamefeatures', 'AdminController@gamefeatures');
    // Route::get('/gamefeatures/get_features', 'AdminController@get_features');
    // Route::get('/gamefeatures/add', 'AdminController@gamefeatures_add');
    // Route::post('/gamefeatures/store', 'FeaturesController@store');
    // Route::post('/gamefeatures/{id}/update', 'FeaturesController@update');
    // Route::get('/gamefeatures/{id}/destroy', 'FeaturesController@destroy');
    // Route::get('/gamefeatures/{id}', 'AdminController@gamefeatures_show');
    // Route::get('/gamefeatures/{id}/edit', 'AdminController@gamefeatures_edit');

    Route::get('/sales', 'AdminController@sales_page');
    Route::get('/sales/get_sales', 'AdminController@get_sales');
    Route::get('/sales/add', 'AdminController@sales_add_page');
    Route::post('/sales/store', 'SalesController@store');
    Route::get('/sales/{id}', 'AdminController@sales_show_page');
    Route::get('/sales/{id}/edit', 'AdminController@sales_edit_page');
    Route::post('/sales/{id}/destroy', 'SalesController@destroy');
    Route::get('/sales/{id}/get_games', 'AdminController@get_sales_games');

    Route::get('/admins', 'AdminController@admins_page');
    Route::get('/admins/get_admins', 'AdminController@get_admins');
    Route::get('/admins/add', 'AdminController@admins_add_page');
    Route::post('/admins/store', 'AdminController@admins_store');
});

// Developer
Route::group(['prefix' => '/developers', 'middleware' => [
    NotDeveloperAdminMiddleware::class
]], function () {
    Route::get('/', 'DevelopersController@developerPage'); //halaman developer
    Route::post('/login', 'DevelopersController@login'); //login dev
    Route::post('/register', 'DevelopersController@register'); //login dev

    Route::get('/{developer_slug}', 'DevelopersController@developerDetailPage')
        ->middleware([ //halaman developer perseorangan
            NotDeveloperAdminMiddleware::class,
            DeveloperExistMiddleware::class
        ]);
    Route::get('/{developer_slug}/{game_slug}', 'DevelopersController@developerGamePage')
        ->middleware([ //games" yang dibuat developer
            NotDeveloperAdminMiddleware::class,
            DeveloperExistMiddleware::class,
            DeveloperGameValidMiddleware::class,
            ViewGameMiddleware::class
        ]);
});
Route::group(['prefix' => '/dashboard',  'middleware' => ['isDeveloper']], function () {
    Route::post('/addgame', 'DevelopersController@addGame');
    Route::get('/', 'DevelopersController@dashboardPage'); //halaman awal
    Route::get('/games', 'DevelopersController@dashboardAllGames'); //lihat semua games
    Route::get('/games/add', 'DevelopersController@dashboardAddGame'); //tambah games
    Route::get('/games/edit/{id}', 'DevelopersController@edit'); //tambah games
    Route::post('/games/preview', 'DevelopersController@previewGames'); //masih tampilan blm save
    Route::get('/games/{game_id}', 'DevelopersController@detailGamePage'); //detail game games
    Route::get('/earning', 'DevelopersController@dashboardEarning');
    Route::get('/inbox', 'DevelopersController@dashboardInbox');
    Route::get('/inbox/{id}', 'DevelopersController@dashboardDetailInbox');
    Route::get('/sales', 'DevelopersController@dashboardSales');
    Route::get('/sales/{id}', 'DevelopersController@dashboardDetailSales');
    Route::get('/sales/{id}/join', 'DevelopersController@dashboardJoinSales'); //join promo
    Route::get('/profile', 'DevelopersController@profile');
    Route::get('/editprofile', 'DevelopersController@editprofile');
    Route::post('/isEditProfile', 'DevelopersController@isEditProfile');
    Route::post('/addsales', 'DevelopersController@addsales');
    Route::post('/editgame', 'DevelopersController@editgame');
    // ->middleware([isAlreadyinSale::class]);
});

// Action User
Route::get('/logout', 'AccountsController@logout');
Route::post('/filtersearch', 'GamesController@search');
Route::post('/downloadgame', 'AccountsController@downloadGame')->middleware(IsUserMiddleware::class);
// Verify Account
Route::get('/verif/{code}', 'AccountsController@verifyAccount');
