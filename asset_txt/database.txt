DB: Accounts
	- id
	- username string
	- email string
	- password string (harusnya dihash)
	- first_name string
	- last_name string
	- balance number
    - filename
	- type number
	- verified_at timestamp null

DB: Games
	- id
	- developer_id
	- name
	- price
	- description
    - message_admin
    - status
	- thumbnail_filename
	- background_filename

DB: GameGenres
	- game_id
	- genre_id

DB: GamePlatforms
	- game_id
	- platform_id

DB: GameFeatures
	- game_id
	- feature_id

DB: Genres
	- id
	- name

DB: Platforms:
	- id
	- name
    - filename

DB: Features
	- id
	- name
	- description
	- image_filename

DB: HTransactions
	- id
	- account_id
	- total

DB: DTransactions
	- htransaction_id
	- game_id
	- price
	- qty
	- subtotal

DB: Reviews
	- game_id
	- account_id
	- rating
	- message

DB: AccountGames
	- account_id
	- game_id
	- qty

DB: Wishlists
	- id
	- account_id
	- game_id

DB: Sales
    - id
    - title
    - description
    - filename
    - started_at
    - ended_at

DB: SaleMails
    - sale_id
    - account_id
    - message
    - status

DB: SaleGames
    - sale_id
    - game_id
    - discount

DB: Topups
	- id
	- account_id
	- nominal

DB: Gifts
	- id
	- account_id
	- friend_id
	- game_id
	- message
	- status

DB: FriendRequests
    - id
    - account_id
    - friend_id
    - message
    - status

DB: Friend
	- account_id
	- friend_id

DB: Chatrooms
    - id
    - account_id
    - friend_id
    - status

DB: Chat
    - id
    - chatroom_id
    - sender_id
    - message

DB: Developer
    - account_id
    - description
    - background_filename

DB: Views
    - id
    - game_id
    - account_id


DB: Gamer
