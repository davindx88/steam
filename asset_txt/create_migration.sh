php artisan make:migration create_accounts_table
php artisan make:migration create_games_table
php artisan make:migration create_genres_table
php artisan make:migration create_platforms_table
php artisan make:migration create_features_table
php artisan make:migration create_gamegenres_table
php artisan make:migration create_gameplatforms_table
# php artisan make:migration create_gamefeatures_table
php artisan make:migration create_htransactions_table
php artisan make:migration create_dtransactions_table
php artisan make:migration create_reviews_table
php artisan make:migration create_accountgames_table
php artisan make:migration create_topups_table
php artisan make:migration create_wishlists_table
php artisan make:migration create_sales_table
php artisan make:migration create_gifts_table
php artisan make:migration create_friends_table
php artisan make:migration create_chats_table
php artisan make:migration create_developers_table
php artisan make:migration create_friendrequests_table
php artisan make:migration create_views_table
php artisan make:migration create_salemails_table
php artisan make:migration create_salegames_table
php artisan make:migration create_chatrooms_table
# Gamer
