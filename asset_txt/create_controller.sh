php artisan make:controller MainController
php artisan make:controller AdminController
php artisan make:controller AccountsController --resource
php artisan make:controller ChatsController --resource
php artisan make:controller ChatRoomsController --resource
php artisan make:controller DevelopersController --resource
php artisan make:controller FeaturesController --resource
php artisan make:controller FriendRequestsController --resource
php artisan make:controller GamesController --resource
php artisan make:controller GenresController --resource
php artisan make:controller GiftsController --resource
php artisan make:controller HtransactionsController --resource
php artisan make:controller PlatformsController --resource
php artisan make:controller SalesController --resource
php artisan make:controller TopupsController --resource
php artisan make:controller ViewsController --resource
php artisan make:controller WishlistsController --resource
